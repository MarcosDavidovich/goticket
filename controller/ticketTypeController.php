<?php
namespace controller;
use model\ticketType as ticketType;
use dao\ticketTypeDAOPDO as ticketTypeDAOPDO;

class ticketTypeController{

    private $ticketTypeDAO = array();

    public function __construct()
    {
        $this->ticketTypeDAO = new ticketTypeDAOPDO();
    }

    public function ticketTypeView($message = " ")
    {   
        $ticketType = new TicketType();
        $_SESSION['editTicketType'] = null;
        $message = $message;
        $ticketTypeList = $this->ticketTypeDAO->getAllActives();
        require_once VIEWS_PATH."ticketTypeView.php";
    }

    public function listTicketTypeView($message = " ")
    {
        require_once VIEWS_PATH."listTicketTypeView.php";
    }


    public function createTicketType($name)
    {
        if ($this->isTicketTypeCreated($name)) {
            $this->ticketTypeView(" Ya existe el tipo de ticket ".$name);
        } else {
        $ticketType= new ticketType();
        $ticketType->setName($name);
        $_SESSION['editTicketType'] = null;
        $this->ticketTypeDAO->add($ticketType);
        $this->ticketTypeView("EXITO!");
        }
    }
    public function modifyTicketType($idTicketType, $name)
    {
        if ($this->isTicketTypeCreated($name)) {
            $this->ticketTypeView(" Ya existe el tipo de ticket ".$name);
        } else {
        $ticketType = new ticketType();
        $_SESSION['editTicketType'] = null;
        $ticketType->setName($name);
        $ticketType->setId($idTicketType);
        $this->ticketTypeDAO->modify($idTicketType,$name);
        $this->TicketTypeView();
        }
    }
    public function isTicketTypeCreated($name)
    {
        $isTicketTypeCreated = false;
        $ticketTypeList = $this->ticketTypeDAO->getAllActives();
        foreach ($ticketTypeList as $ticketType) {
            $nameCapital = $ticketType->getName();
            $nameNoCapital = strtolower($nameCapital);
            $nameNoCapitalCompare =  strtolower($name);
            if ($nameNoCapital == $nameNoCapitalCompare) {
                $isTicketTypeCreated = true;
            }
        }
        return $isTicketTypeCreated;
    }

    public function editTicketType($idTicketType, $name)
    {
        $message = " ";
        $ticketType = $this->ticketTypeDAO->getById($idTicketType);
        $ticketType->setName($name);
        $ticketTypeList = $this->ticketTypeDAO->getAllActives();
        $_SESSION['editTicketType'] = true;
        require_once VIEWS_PATH.'ticketTypeView.php';
    }

    public function deleteTicketType($id)
    {
        $this->ticketTypeDAO->logicDelete($id);
        $this->ticketTypeView();
    }

}
?>