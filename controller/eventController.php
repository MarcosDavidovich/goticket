<?php
namespace controller;

use model\artist as artist;
use model\event as event;
use model\calendar as calendar;
use model\image as image;

use dao\eventDAOPDO as eventDAOPDO;
use dao\artistDAOPDO as artistDAOPDO;
use dao\calendarDAOPDO as calendarDAOPDO;
use dao\categoryDAOPDO as categoryDAOPDO;
use dao\ticketDAOPDO as ticketDAOPDO;
use dao\purchasedTicketDAOPDO as purchasedTicketDAOPDO;
use dao\imageDAOPDO as imageDAOPDO;

class EventController
{
    private $eventDAO;
    private $calendarDAO;
    private $categoryDAO;
    private $artistDAO;
    private $ticketDAO;
    private $purchasedTicketDAO;
    private $imageDAO;
    private $message;

    public function __construct()
    {
        $this->calendarDAO = new calendarDAOPDO();   
        $this->categoryDAO = new categoryDAOPDO();   
        $this->artistDAO = new artistDAOPDO();   
        $this->eventDAO = new eventDAOPDO();   
        $this->ticketDAO = new ticketDAOPDO();   
        $this->purchasedTicketDAO = new purchasedTicketDAOPDO();   
        $this->imageDAO = new imageDAOPDO();   
    }

    public function index()
    {
    }

    public function eventView($eventId)
    {
        $_SESSION['editEvent'] = null;
        $calendarList = $this->calendarDAO->getByEventId($eventId);
        $event= $this->eventDAO->getById($eventId);
        $ticketList= $this->ticketDAO->getAllActives();
        foreach ($calendarList as $calendar) {
            $calendar->setArtists($this->artistDAO->getArtists($calendar->getId()));
            foreach ($ticketList as $ticket) {
                $ticket->setRemain($ticket->getAvailable() - $this->purchasedTicketDAO->getUsedTicketsByIdCalendarAndIdTicket($calendar->getId(), $ticket->getId())) ;
             }  
        }
        
        require_once VIEWS_PATH."eventView.php";
    }


    public function createEventView($message = " ", $eventId ="")
    {
        $message = $this->message;
        $eventList = $this->eventDAO->getAllActives();
        $calendarList = $this->calendarDAO->getAllActives();
        $ticketList = $this->ticketDAO->getAllActives();
        $categoryList = $this->categoryDAO->getAllActives();
        require_once VIEWS_PATH."eventAdminView.php";
    }

    public function listEventView( $filtertype = 0, $filterParams = array(), $message = " ")
    {
        if($filtertype!=2){  // filtra por categorias
                $filterCategories = $filterParams;
                $eventList = $this->eventDAO->getAllActives();
        } else if($filtertype==2) //filtra por fecha
        {
            $eventList = $this->getEventsInDates($filterParams[0], $filterParams[1]);
        } else {  //default

            $eventList = $this->eventDAO->getAllActives();
        }
        
       
        $categoryList = $this->categoryDAO->getAllActives();
        require_once VIEWS_PATH."listEventView.php";
    }

    //devuelve array de eventos en un rango de fechas
    public function getEventsInDates($date1, $date2)
    {
        $calendarList = $this->calendarDAO->getInDates($date1, $date2);
        $eventList = array();
        foreach($calendarList as $calendar)
        {
            if(!in_array($calendar->getEvent(), $eventList))
            {
                array_push($eventList, $calendar->getEvent());
            }
        }
        return $eventList;
    }
    // muestra eventos que contengan artist id
    public function listEventByArtistView($artistId)
    {   
        $artist= $this->artistDAO->getById($artistId);
        $calendarList= $this->calendarDAO->getCalendarsByArtist($artistId);
        $eventList = array();
        foreach($calendarList as $calendar)
        {
            if(!in_array($calendar->getEvent(), $eventList))
            {
                array_push($eventList, $calendar->getEvent());
            }
        }
        require_once VIEWS_PATH."eventByArtistView.php";
        
    }

    public function uplodaImage($file, $location)
    {
        try
        {
            //var_dump($file);

            $fileName = $file["name"];
            $tempFileName = $file["tmp_name"];
            $type = $file["type"];            
            $filePath = $location.basename($fileName);            
            $fileType = strtolower(pathinfo($filePath, PATHINFO_EXTENSION));
            $imageSize = getimagesize($tempFileName);

            if($imageSize !== false)
            {
                $newId=null;
                if (move_uploaded_file($tempFileName, $filePath))
                {
                    $image = new image();
                    $image->setName($fileName);
                    $newId = $this->imageDAO->Add($image);
                    $message = "Imagen subida correctamente";
                } else {
                    //$message = "Ocurrió un error al intentar subir la imagen";
                }
             } else {   
                // $message = "El archivo no corresponde a una imágen";
             }
            return $newId;
        }
        catch(Exception $ex)
        {
            $message = $ex->getMessage();
        }

    }    


    public function createEvent($name, $description, $category, $coverImg, $sqImg )
    {
       
        if ($this->isEventCreated($name,$description)) {
            $eventList = $this->eventDAO->getAllActives();
            $calendarList = $this->calendarDAO->getAllActives();
            $ticketList = $this->ticketDAO->getAllActives();
            $categoryList = $this->categoryDAO->getAllActives();
            $message = ' Ya existe un evento con nombre '.$name.' y descripcion '.$description;
            require_once VIEWS_PATH.'eventAdminView.php';
        } else {  
        // fix imagenes vacias
        $coverId=null;
        $sqId=null;
        if($coverImg["size"]!=0)
        {
            $coverId = $this->uplodaImage($coverImg, VIEWS_PATH."img/events/");
        } 
        if($sqImg["size"]!=0)
        {
            $sqId = $this->uplodaImage($sqImg, VIEWS_PATH."img/events/");
        }
        //end fix

        $_SESSION['editEvent'] = null;
        $event = new event();
        
        $event->setName($name);
        $event->setDescription($description); 
        $event->setCategory( $this->categoryDAO->getById($category)); 
        $event->setCoverImg($coverId);
        $event->setSqImg($sqId);
         
 
        $eventId = $this->eventDAO->add($event);
        $this->setActiveEvent($eventId);
        header("location:".FRONT_ROOT."calendar/calendarView");
        }
    }
    
    public function isEventCreated($name, $description)
    {
        $isEventCreated = false;
        $eventList = $this->eventDAO->getAllActives();
        foreach ($eventList as $event) {
            $eventDescription = $event->getDescription();
            $eventName =  $event->getName();
            if($eventDescription == $description && $name == $eventName  )
            {
                $isEventCreated = true;  
            }
            
        }
        return $isEventCreated;
    }
    
    public function modifyEvent($idEvent, $name, $description, $idCategory, $coverImg, $sqImg)
    {
        
        // fix imagenes vacias
        $coverId=null;
        $sqId=null;
        if($coverImg["size"]!=0)
        {
            $coverId = $this->uplodaImage($coverImg, VIEWS_PATH."img/events/");
        } else{
            $coverImage = $this->imageDAO->getByName( $this->eventDAO->getById($idEvent)->getCoverImg() );
            $coverId = $coverImage->getId();
        }  
        if($sqImg["size"]!=0)
        {
            $sqId = $this->uplodaImage($sqImg, VIEWS_PATH."img/events/");
        } else {
            $sqImage = $this->imageDAO->getByName( $this->eventDAO->getById($idEvent)->getSqImg() );
            $sqId = $sqImage->getId();
        }
        //end fix
        $event = new event();
        $_SESSION['editEvent'] = null;
        $event->setName($name);
        $event->setId($idEvent);
        $event->setDescription($description);
        $event->setCategory($this->categoryDAO->getById($idCategory));
        $event->setCoverImg($coverId);
        $event->setSqImg($sqId);
        $this->eventDAO->modify($idEvent, $name, $description, $idCategory, $coverId, $sqId);
        $this->createEventView();
        
    }

    public function deleteEventImg($eventId, $opt)    
    {
       if($opt == "1")
       {
         $this->eventDAO->deleteCoverImage($eventId);
        }else{
            $this->eventDAO->deleteSqImage($eventId);
       }
       $this->editEvent($eventId);
    }

    public function editEvent($eventId)
    {
        $event = $this->eventDAO->getById($eventId);
        $categoryList = $this->categoryDAO->getAllActives();
        $_SESSION['editEvent'] = true;
        require_once VIEWS_PATH.'editEventView.php';
    }

    public function setActiveEvent($id)
    {
        $_SESSION["activeEvent"] = $id;    
    }

    public function calendarCount($id)
    {
        $calendarList = $this->calendarDAO->getAllActives();
        $i=0;
        foreach ($calendarList as $calendar) {
            if($calendar->getEvent()->getId()== $id){
                $i++ ;
            }
        }
        return $i;
    }
    public function ticketCount($id)
    {
        $ticketList = $this->ticketDAO->getAllActives();
        
        $i=0;
        foreach ($ticketList as $ticket) {
            if($ticket->getCalendar()->getEvent()->getId()== $id){
                $i++ ;
            }
        }
        return $i;
    }

    public function deleteEvent($id)
    {
        $hasPurchasedTickets = false;
        $event = $this->eventDAO->getById($id);
    
         $calendarList = $this->calendarDAO->getByEventId($id);
         $purchasedTickets = $this->purchasedTicketDAO->getAllActives();
         foreach ($calendarList as $calendar) {
         foreach ($purchasedTickets as $purchasedTicket) {
             $ticket = $this->ticketDAO->getById($purchasedTicket->getIdTicket());
             if ($ticket->getCalendar()->getId() == $calendar->getId()) {
                 $hasPurchasedTickets = true;
             }
         }
        }
       if ($hasPurchasedTickets) {
        $this->message = " El evento tiene calendarios con tickets vendidos ";
        $this->createEventView();
       } else {
        $this->eventDAO->logicDelete($id);
        $this->createEventView();
       }
       
    
    }

}
