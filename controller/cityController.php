<?php
namespace controller;

use model\city as city;
use dao\cityDAO as cityDAO;
use dao\cityDAOPDO as cityDAOPDO;
use dao\venueDAOPDO as venueDAOPDO;

class cityController {

    private $cityDAO;
    private $venueDAO;
    private $message;

    public function __construct()
    {

        $this->cityDAO = new cityDAOPDO();
        $this->venueDAO = new venueDAOPDO();
        
    }


    public function index()
    {
        // el action se forma /"nombre del proyecto"/"nombre del controler"/"metodo"
        include_once VIEWS_PATH."cityView.php";
    }

    public function cityView($message = " ")
    {   
        $city = new city();
        $_SESSION['editCity'] = null;
        $cityList = $this->cityDAO->getAllActives();
        require_once VIEWS_PATH."cityView.php";
    }


    public function createcity($name)
    {   
        if ($this->isCityCreated($name)) {
            $cityList = $this->cityDAO->getAllActives();
            $this->cityView("Ya existe la ciudad con nombre ".$name);    
        } else {
        $city = new city();
        $city->setName($name);
        $cityList = $this->cityDAO->getAllActives();
        $_SESSION['editCity'] = null;
        $this->cityDAO->add($city);
        $this->cityView("EXITO!");
        }
    }
    public function isCityCreated($name)
    {
        $isCityCreated = false;
        $cityList = $this->cityDAO->getAllActives();
        foreach ($cityList as $city) {
            $nameCapital = $city->getName();
            $nameNoCapital = strtolower($nameCapital);
            $nameNoCapitalCompare =  strtolower($name);
            if ($nameNoCapital == $nameNoCapitalCompare) {
                $isCityCreated = true;
            }
        }
        return $isCityCreated;
    }
    public function modifyCity($idCity, $name)
    {
        if ($this->isCityCreated($name)) {
            $cityList = $this->cityDAO->getAllActives();
            $this->cityView("Ya existe la ciudad con nombre ".$name);    
        } else {
        $message = "";
        $city = new city();
        $_SESSION['editCity'] = null;
        $city->setName($name);
        $city->setId($idCity);
        $this->cityDAO->modify($idCity,$name);
        $this->cityView();
        }    
    }

    public function editCity($idCity, $name)
    {        
        $message = " ";
        $city = $this->cityDAO->getById($idCity);
        $city->setName($name);
        $cityList = $this->cityDAO->getAllActives();
        $_SESSION['editCity'] = true;
        require_once VIEWS_PATH.'cityView.php';
    }

    public function deleteCity($id)
    {
        $hasVenues = false;
        $venueList = $this->venueDAO->getAllActives();
        foreach ($venueList as $venue) {
            if($id == $venue->getCity()->getId())
            {
                $hasVenues = true;
            }
        }
        if ($hasVenues) {
            $this->cityView(" La ciudad tiene lugares creados");
        } else {
            $this->cityDAO->logicDelete($id);
            $this->cityView("Exito");
        }
    }

}

?>