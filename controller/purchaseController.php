<?php
namespace controller;

use model\purchase as purchase;
use model\purchasedTicket as purchasedTicket;
use model\purchaseLine as purchaseLine;
use model\calendar as calendar;

use dao\purchaseDAOPDO as purchaseDAOPDO;
use dao\ticketDAOPDO as ticketDAOPDO;
use dao\ticketTypeDAOPDO as ticketTypeDAOPDO;
use dao\eventDAOPDO as eventDAOPDO;
use dao\userDAOPDO as userDAOPDO;
use dao\calendarDAOPDO as calendarDAOPDO;
use dao\purchaseLineDAOPDO as purchaseLineDAOPDO;
use dao\purchasedTicketDAOPDO as purchasedTicketDAOPDO; 
use dao\artistDAOPDO as artistDAOPDO; 

class purchaseController {

  private $purchaseDAO;
  private $ticketDAO;
  private $ticketTypeDAO;
  private $calendarDAO;
  private $eventDAO;
  private $purchaseLineDAO;
  private $purchasedTicketDAO;
  private $userDAO;
  private $ticketList;
  private $calendarList;
  private $eventList;
  private $activePurchase;
  private $artistDAO;
  
    public function __construct()
    {
      if(!isset($_SESSION["purchase"])){   // consulta si no esta creado en session
        $_SESSION["purchase"] = new purchase();   // crea array en session
      }
      $this->activePurchase = &$_SESSION["purchase"];    // vincula el local con el de session        
      $this->eventDAO = new eventDAOPDO();
      $this->calendarDAO = new calendarDAOPDO();
      $this->ticketDAO = new ticketDAOPDO();
      $this->ticketTypeDAO = new ticketTypeDAOPDO();
      $this->purchaseDAO = new purchaseDAOPDO();
      $this->userDAO = new userDAOPDO();
      $this->purchaseLineDAO = new purchaseLineDAOPDO();
      $this->purchasedTicketDAO =new purchasedTicketDAOPDO();
      $this->artistDAO = new artistDAOPDO();   

    }


    public function index()
    {
        // el action se forma /"nombre del proyecto"/"nombre del controler"/"metodo"
        include_once VIEWS_PATH."categoryView.php";
    }

    public function purchaseView()
    {   

        $ticketList = $this->ticketDAO->getAllActives();
        $activePurchase = $this->activePurchase;
        require_once VIEWS_PATH."purchaseView.php";
    }

    // agrega tickets al carrito
    public function addTicket($ticketId, $quantity)
    {
        $ticket = $this->ticketDAO->getById($ticketId);
        $calendar = $ticket->getCalendar();
        
        $ticketList = $this->ticketDAO->getAllActives();
        $activePurchase = $this->activePurchase;
        $calendar->setArtists($this->artistDAO->getArtists($calendar->getId()));
        $venue= $calendar->getVenue();
        $event = $calendar->getEvent();
        $calendarList = $this->calendarDAO->getByEventId($event->getId());
        if ($quantity > $ticket->getAvailable() - $this->purchasedTicketDAO->getUsedTicketsByIdCalendarAndIdTicket($calendar->getId(), $ticket->getId())) {
            $message = 'No hay esa cantidad de tickets';    
            require_once VIEWS_PATH."eventView.php";
        }else {
        
        // tiene que comprobar que no halla una linea del nuevo ticket ya creada, si hay suma, sino crea.
        $ticketAux= $this->ticketDAO->getById($ticketId);

        $exist=false;
        foreach($activePurchase->getLines() as $line)
        {
            if($line->getIdTicket() == $ticketId) 
            {   
                $exist=true;
                $line->setQuantity($line->getQuantity()+$quantity);
                break;
            }
        }

        if(!$exist)
        {
            $newLine = new purchaseLine();
            $newLine->setId( rand(0,99) );  // seteo un id random para poder borrar lineas, despues al momento de compra el pdo le sobreescribe con el id posta.
            $newLine->setIdTicket($ticketId);
            $newLine->setQuantity($quantity);
            $newLine->setPrice($quantity*$ticketAux->getPrice());
            
            $this->activePurchase->addLine($newLine);
        }
        require_once VIEWS_PATH."purchaseView.php";
        }
    }

    // recibe un calendario y lo llena de artistas
    public function fillCalendar(calendar $calendar)
    {
        $calendar->setArtists($this->artistDAO->getArtists($calendar->getId()));  
        return $calendar;
    }


    public function deleteLine($lineId)
    {
      $newPurchase = new purchase();
            foreach($this->activePurchase->getLines() as $line)
            {
                if($line->getId() != $lineId)
                {
                    $newPurchase->addLine($line);
                }
            }
      $this->activePurchase = $newPurchase;
      
      
      $this->purchaseView();
    
    }



    public function addPurchase()
    {
        
        $this->activePurchase->setUserId($_SESSION['activeUser']);
        $this->activePurchase->setDate(date("Y-m-d"));
        $purchaseId = $this->purchaseDAO->add($this->activePurchase);
        foreach ($this->activePurchase->getLines() as $line) {
          $line->setIdPurchase($purchaseId);
          $this->purchaseLineDAO->add($line);
        }
        $this->addPurchasedTicket($purchaseId);
        $this->activePurchase = null;
        $_SESSION['activePurchase'] = null;
        
        header("location:".FRONT_ROOT."user/userView");

    }

    public function addPurchasedTicket($idPurchase)
    {
      
      $purchase = $this->purchaseDAO->getById($idPurchase);
      $lines = $this->purchaseLineDAO->getLinesByIdPurchase($idPurchase);
      foreach ($lines as $line) {
        $lineQuantity = $line->getQuantity();
        
        for ($i=0; $i < $lineQuantity; $i++) { 
            $hashQr = hash('sha256', $i.$idPurchase.$line->getIdTicket());
            $purchasedTicket = new PurchasedTicket();
            $purchasedTicket->setIdPurchase($idPurchase);
            $purchasedTicket->setIdTicket($line->getIdTicket());
            $purchasedTicket->setPrice($line->getPrice()/$lineQuantity);
            $purchasedTicket->setQr($hashQr);
            $this->purchasedTicketDAO->add($purchasedTicket);
        }
        
      }
    }

}

?>