<?php
namespace controller;
use model\user as User;

use dao\userDAOPDO as userDAOPDO;
use dao\calendarDAOPDO as calendarDAOPDO;
use dao\artistDAOPDO as ArtistDAOPDO;
use dao\purchasedTicketDAOPDO as purchasedTicketDAOPDO;
use dao\purchaseDAOPDO as purchaseDAOPDO;
use dao\purchaseLineDAOPDO as purchaseLineDAOPDO;
use dao\ticketDAOPDO as ticketDAOPDO;

class UserController
{
    private $userDAO;
    private $calendarDAO;
    private $purchasedTicketDAO;
    private $purchaseDAO;
    private $purchaseLineDAO;
    private $artistDAO;
    private $message;
    

    public function __construct()
    {
        $this->userDAO= new userDAOPDO();
        $this->purchasedTicketDAO = new purchasedTicketDAOPDO();
        $this->purchaseDAO = new purchaseDAOPDO();
        $this->purchaseLineDAO = new purchaseLineDAOPDO();
        $this->ticketDAO = new ticketDAOPDO();
        $this->artistDAO = new artistDAOPDO();   

    }

    public function index()
    {
        require_once VIEWS_PATH."homeView.php";
    }
    
    public function loginView($message="")
    {

      require_once VIEWS_PATH."loginView.php";
    }
    
    public function userView()
    {
            $user= $this->userDAO->getById($_SESSION["activeUser"]);
            
            $purchasesArray = $this->purchaseDAO->getByIdUser($user->getId());
            $purchasedTicketArray= $this->purchasedTicketDAO->getAllActives();
            foreach ($purchasesArray as $purchase) {
                $purchase->setLines($this->purchaseLineDAO->getLinesByIdPurchase($purchase->getId()));
            }
            
            require_once VIEWS_PATH."userView.php";
    }

    public function adminView($message = ' ')
    {   
        $user= $this->userDAO->getById($_SESSION["activeUser"]);
        $message = $this->message;
        $userList = $this->userDAO->getAllActives();
        require_once VIEWS_PATH."adminView.php";
    }
    public function editUser($idUser, $email, $userName, $userTypeId)
    {
        //$this->modifyUser($idUser, $email, $userName, $userTypeId);
        $user= $this->userDAO->getById($idUser);
        require_once VIEWS_PATH."editUserView.php";
    }
    public function modifyUser($idUser, $email, $userName, $userTypeId)
    {
        $user= $this->userDAO->getById($idUser);
        $this->userDAO->modify($user->getId(), $email, $userName, $userTypeId);
        $message = 'Exito';
        $userList = $this->userDAO->getAllActives();
        require_once VIEWS_PATH."adminView.php";
    }
    public function changePasswordView($idUser)
    {
        $message = ' ';
        $user= $this->userDAO->getById($idUser);
        $userList = $this->userDAO->getAllActives();
        require_once VIEWS_PATH."changePasswordView.php";
    }
    public function changePassword($idUser,$newPassword, $newPassword2, $oldPassword)
    {
        $isValidUser = false;
        $user= $this->userDAO->getById($_SESSION["activeUser"]);
        $message = ' ';
        if ($newPassword == $newPassword2) {
           
            
            $userList = $this->userDAO->getAllActives();
            foreach ($userList as $user) {
                
                if ($user->getId() == $idUser && $user->getPassword() == $oldPassword) {
                    $isValidUser = true;
                    $this->userDAO->changePassword($idUser, $newPassword);
                    
                }
            }
            if ($isValidUser) {
                $message = 'Exito!';
                require_once VIEWS_PATH."adminView.php";
            } else {
                $message = 'Contraseña no valida';
                require_once VIEWS_PATH."changePasswordView.php";
            }

        } else {

            $message = ' Las contraseñas no coinciden'; 
            require_once VIEWS_PATH."changePasswordView.php";
        }
        


    }

    public function login($userName, $pass)
    {
        $userName = strtolower($userName);
        $user=$this->userDAO->getByUserName($userName);
        if ($user==null){
            $user=$this->userDAO->getByEmail($userName);
        }        
        
        if($user)
        {
            if($user->getPassword() == $pass)
            {
                if($user->getUserType()==1)
                {
                    $_SESSION["userType"] = "user";
                } else if($user->getUserType()==0){
                    $_SESSION["userType"] = "admin";
                }
                
                $_SESSION["activeUser"] = $user->getId();
                
                $this->loginView();
                echo '<script type="text/javascript">',
                'successMod();',
                '</script>';
            }
            
        }
        $message="Usuario y/o Contraseña incorrectos";
        $this->loginView($message);
              
    }

    public function signUp($email, $userName, $password)
    {   
        $userName = strtolower($userName);
        $email = strtolower($email);
        $user=$this->userDAO->getByUserName($userName);
        if ($user==null){
            $user=$this->userDAO->getByEmail($email);
            if($user==null)
            {
                $user = new User();
                $user->setEmail($email);
                $user->setUserName($userName);
                $user->setPassword($password);
                //  0 para Admin y 1 para User
                $user->setUserType(1);
                
                $newId= $this->userDAO->add($user);
                //TODO hacer login automatico
                $_SESSION["userType"] = "user";
                $_SESSION["activeUser"] = $newId;
                $this->loginView();
                
                echo '<script type="text/javascript">',
                'successMod();',
                '</script>'; 
            } else {
                $message="Ya existe una cuenta con esa dirección de email";
                $this->loginView($message);
            }

        } else{
            $message="El nombre de Usuario es incorrecto o ya esta ocupado.";
            $this->loginView($message);
        }
    }



    public function logout()
    {
        session_destroy();
        // el action se forma /"nombre del proyecto"/"nombre del controler"/"metodo"
        $this->index();
    }

    public function deleteUser($id)
    {
        $hasPurchasedTickets = false;
        $user = $this->userDAO->getById($id);
    
         $purchasedTickets = $this->purchasedTicketDAO->getAllActives();
         foreach ($purchasedTickets as $purchasedTicket) {
             $purchase = $this->purchaseDAO->getById($purchasedTicket->getIdPurchase());
             if ($purchase->getUserId() == $user->getId()) {
                 $hasPurchasedTickets = true;
             }
        }
       if ($hasPurchasedTickets) {
        $this->message = " El usuario tiene tickets comprados ";
        $this->adminView();
       } else {
        $this->userDAO->logicDelete($id);
        $this->message = " Exito!";
        $this->adminView();
       }
    }
   
}
?>