<?php
namespace controller;

use model\calendar as calendar;
use model\venue as venue;
use model\artist as artist;
use model\event as event;
use model\artistxCalendar as artistxCalendar;

use dao\calendarDAOPDO as calendarDAOPDO;
use dao\venueDAOPDO as venueDAOPDO;
use dao\artistDAOPDO as artistDAOPDO;
use dao\eventDAOPDO as eventDAOPDO;
use dao\ticketDAOPDO as ticketDAOPDO;
use dao\purchasedTicketDAOPDO as purchasedTicketDAOPDO;
use dao\artistxCalendarDAOPDO as artistxCalendarDAOPDO; 

class CalendarController
{
    private $calendarDAO;
    private $artistDAO;
    private $venueDAO;
    private $eventDAO;
    private $ticketDAO;
    private $artistxCalendarDAO;
    private $purchasedTicketDAO;
    private $message;

    public function __construct()
    {
        $this->calendarDAO = new calendarDAOPDO();   
        $this->artistDAO = new artistDAOPDO();   
        $this->venueDAO = new venueDAOPDO();   
        $this->eventDAO = new eventDAOPDO();   
        $this->ticketDAO = new ticketDAOPDO();
        $this->purchasedTicketDAO = new purchasedTicketDAOPDO(); 
        $this->artistxCalendarDAO = new artistxCalendarDAOPDO();   
    }

    public function index()
    {
      
    }

    public function calendarView()
    {
        $message = $this->message;
        $eventId = $_SESSION['activeEvent'];
        $localEvent = $this->eventDAO->getById($eventId);
        $filteredCalendars= $this->filterCalendarsByEvent($eventId);
        $filteredTickets= $this->filterTicketsByEvent($eventId);
        $artistsList = $this->artistDAO->getAllActives();
        $eventList = $this->eventDAO->getAllActives();
        $venuesList = $this->venueDAO->getAllActives();
        $calendarList = $this->calendarDAO->getAllActives();
        $purchasedTicketsList = $this->purchasedTicketDAO->getAllActives();
        foreach ($calendarList as $calendar) {
            $calendar->setArtists($this->artistDAO->getArtists($calendar->getId()));
            foreach ($filteredTickets as $ticket) {
               $ticket->setRemain($ticket->getAvailable() - $this->purchasedTicketDAO->getUsedTicketsByIdCalendarAndIdTicket($calendar->getId(), $ticket->getId())) ;
            }            
        }
        
        require_once VIEWS_PATH."calendarView.php";
    }
    
    public function listCalendarView( )
    {
        $calendarList = $this->calendarDAO->getAllActives();
        foreach ($Ft as $calendar) {
            $calendar->setArtists($this->artistDAO->getArtists($calendar->getId()));
        }
        require_once VIEWS_PATH."listCalendarView.php";
    }

    public function editCalendar($eventId)
    {
        $_SESSION['activeEvent'] = $eventId;
        $this->calendarView();
    }
    
    public function updateCalendar($calendarId, $eventId)
    {
        $calendar = $this->calendarDAO->getById($calendarId);
        $_SESSION['activeEvent'] = $eventId;        
        $artistsList = $this->artistDAO->getAllActives();
        $eventList = $this->eventDAO->getAllActives();
        $venuesList = $this->venueDAO->getAllActives();
        $calendarList = $this->calendarDAO->getAllActives();
        $localCalendar = $this->calendarDAO->getById($calendarId);
        $localCalendar->setArtists($this->artistDAO->getArtists($calendarId));
        $localEvent = $this->eventDAO->getById($eventId);
        require_once VIEWS_PATH."editCalendarView.php";
    }

    public function modifyCalendar($eventId, $calendarId, $date, $venue, $artistArray)
    {
        
        $calendar = $this->calendarDAO->getById($calendarId);   
        $calendar->setEvent($this->eventDAO->getById($eventId));  
        $calendar->setDate($date);
        $calendar->setVenue($this->venueDAO->getById($venue));   
        
        foreach ($artistArray as $artist) {
            $calendar->addArtist( $this->artistDAO->getById($artist) );
        }

        $idCalendar = $this->calendarDAO->update($calendar);

        $this->updateArtistXCalendar($idCalendar,$artistArray);
        $this->message = "EXITO!";
        $this->calendarView();
        
    }


    public function createCalendar($eventId, $date, $venue, $artistArray)
    {
        if ($this->isCalendarCreated($eventId, $date, $venue)) {
            $venueAux = $this->venueDAO->getById($venue);
            $this->message = ' Ya existe un calendario en '.$venueAux->getName().' el dia '.$date.' para el evento '.$eventId;
            $this->calendarView();
        } else {   
        $calendar = new calendar;
        $calendar->setEvent($this->eventDAO->getById($eventId));  
        $calendar->setDate($date);
        $calendar->setVenue($this->venueDAO->getById($venue));   
        foreach ($artistArray as $artist) {
            $calendar->addArtist( $this->artistDAO->getById($artist) );
        }

        $idCalendar = $this->calendarDAO->add($calendar);
        $this->createArtistXCalendar($idCalendar,$artistArray);
        $this->message = "EXITO!";
        $this->calendarView();
        }
    }
    public function isCalendarCreated($eventId, $date, $venue)
    {
        $isCalendarCreated = false;
        $calendarList = $this->calendarDAO->getAllActives();
        foreach ($calendarList as $calendar) {
            $eventIdCompare = $calendar->getEvent()->getId();
            $dateCompare =  $calendar->getDate();
            $venueCompare = $calendar->getVenue()->getId();
            if($eventIdCompare == $eventId && $dateCompare == $date && $venueCompare == $venue )
            {
                $isCalendarCreated = true;  
            }
            
        }
        return $isCalendarCreated;
    }

    // ver de borrarlo a la mierda
    public function filterCalendarsByEvent($id)
    {
        $calendarList = $this->calendarDAO->getAllActives();
        $filteredList = array();
        foreach ($calendarList as $calendar) {
            $calendar->setArtists($this->artistDAO->getArtists($calendar->getId()));
            if($calendar->getEvent()->getId() == $id)
            {
                array_push($filteredList, $calendar);
            }
        }
        return $filteredList;
    }
    
    public function filterTicketsByEvent($id)
    {
        $ticketList = $this->ticketDAO->getAllActives();
        $filteredList = array();
        foreach ($ticketList as $ticket) {
            if($ticket->getCalendar()->getEvent()->getId() == $id)
            {
                array_push($filteredList, $ticket);
            }
        }
        return $filteredList;
    }

    public function deleteCalendar($id)
    {
        $hasPurchasedTickets = false;
        $calendar = $this->calendarDAO->getById($id);
    
         $purchasedTickets = $this->purchasedTicketDAO->getAllActives();
         foreach ($purchasedTickets as $purchasedTicket) {
             $ticket = $this->ticketDAO->getById($purchasedTicket->getIdTicket());
             if ($ticket->getCalendar()->getId() == $calendar->getId()) {
                 $hasPurchasedTickets = true;
             }
        }
       if ($hasPurchasedTickets) {
        $this->message = " El calendario tiene tickets vendidos ";
        $this->calendarView();
       } else {
        $this->calendarDAO->logicDelete($id);
        $this->calendarView();
       }
    }
    
    public function createArtistXCalendar($idCalendar,$artistArray)
    {
        $artistxCalendar = new artistxCalendar();
        
        
        foreach ($artistArray as $artist) {
            $artistxCalendar->setIdArtist($artist);
            $artistxCalendar->setIdCalendar($idCalendar);
            $this->artistxCalendarDAO->add($artistxCalendar);
        }
    }
    public function updateArtistXCalendar($idCalendar,$artistArray)
    {
        $this->artistxCalendarDAO->deleteByIdCalendar($idCalendar);
        $artistxCalendar = new artistxCalendar();
        foreach ($artistArray as $artist) {
            $artistxCalendar->setIdArtist($artist);
            $artistxCalendar->setIdCalendar($idCalendar);
            $this->artistxCalendarDAO->add($artistxCalendar);
        }
    }

}
