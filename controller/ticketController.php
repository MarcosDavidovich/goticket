<?php
namespace controller;
use model\ticket as ticket;
use model\calendar as calendar;

use dao\ticketTypeDAOPDO as ticketTypeDAOPDO;
use dao\purchasedTicketDAOPDO as purchasedTicketDAOPDO;
use dao\ticketDAOPDO as ticketDAOPDO;
use dao\calendarDAOPDO as calendarDAOPDO;
use dao\eventDAOPDO as eventDAOPDO;
use dao\venueDAOPDO as venueDAOPDO;
use dao\artistDAOPDO as artistDAOPDO;

class ticketController
{
    private $ticketDAO;
    private $purchasedTicketDAO;
    private $ticketTypeDAO;
    private $calendarDAO;
    private $eventDAO;
    private $venueDAO;
    private $artistDAO;

    private $message;

    public function __construct()
    {
        $this->ticketDAO = new ticketDAOPDO();
        $this->purchasedTicketDAO =new purchasedTicketDAOPDO();
        $this->ticketTypeDAO = new ticketTypeDAOPDO();
        $this->calendarDAO = new calendarDAOPDO();
        $this->eventDAO = new eventDAOPDO();
        $this->venueDAO = new venueDAOPDO();
        $this->artistDAO = new artistDAOPDO();
    }


    public function index()
    {
        
    }

    public function createTicketView()
    { 
        $ticket = new ticket();
        $_SESSION['editTicket'] = null;
        $message= $this->message;
        $eventId = $_SESSION["activeEvent"];
        $localEvent = $this->eventDAO->getById($eventId);
        $ticketTypeList = $this->ticketTypeDAO->getAllActives();
        $ticketList = $this->ticketDAO->getAllActives();
        $calendarList = $this->calendarDAO->getAllActives();
        foreach ($calendarList as $calendar) {
            $calendar->setArtists($this->artistDAO->getArtists($calendar->getId()));
           
        }
        
        require_once VIEWS_PATH."ticketView.php";
    }
    
    public function listTicketView($message = " ")
    {
        
        $ticketTypeList = $this->ticketTypeDAO->getAllActives();
        $ticketList = $this->ticketDAO->getAllActives();
        foreach ($ticketList as $ticket) {
            $ticket->setRemain($ticket->getAvailable() - $this->purchasedTicketDAO->getUsedTicketsByIdCalendarAndIdTicket($calendar->getId(), $ticket->getId()));
        }
        require_once VIEWS_PATH."listTicketView.php";
    }

    //
    public function createTickets($calendar = array(), $ticketType, $price, $available)
    {
        foreach ($calendar as $calendarId) {
            
        
        $_SESSION['editTicket'] = null;
        $cal = $this->calendarDAO->getById($calendarId);
        if ($this->isTicketCreated($calendarId,$ticketType)) 
            {
                $ticketTypeAux = $this->ticketTypeDAO->getById($ticketType);
                $calendarAux = $this->calendarDAO->getById($calendarId);
                $this->message = 'Ya existe un ticket de tipo '.$ticketTypeAux->getName().' para el calendario '.$calendarAux->getEvent()->getName().' - '.$calendarAux->getDate().' @ '.$calendarAux->getVenue()->getName();
            } else {
            $ticket = new ticket();
            $ticket->setCalendar($cal);
            $ticket->setTicketType( $this->ticketTypeDAO->getById($ticketType));
            $ticket->setPrice($price);
            $ticket->setAvailable($available);
            $ticket->setRemain($available);
            $this->ticketDAO->add($ticket);
            }
        }
        // lo saque afuera de los if para que muestre el resultado total
        $this->createTicketView();

    }
    public function isTicketCreated($idCalendar,$idTicketType)
    {
        $isTicketCreated = false;
        $ticketList = $this->ticketDAO->getAllActives();
        foreach ($ticketList as $ticket) {
            $idCalendarCompare = $ticket->getCalendar()->getId();
            $idTicketTypeCompare = $ticket->getTicketType()->getId();
            if ( $idCalendarCompare == $idCalendar && $idTicketTypeCompare == $idTicketType) {
                $isTicketCreated = true;       
            }
        }
        return $isTicketCreated;
    }
    public function modifyTicket($idTicket, $idCalendar, $idTicketType, $price, $available)
    {
        $eventId = $_SESSION['activeEvent'];
        $localEvent = $this->eventDAO->getById($eventId);
        $filteredCalendars= $this->calendarDAO->getByid($idCalendar);
        $filteredTickets= $this->ticketDAO->getByCalendarId($idCalendar);
        $ticket = new ticket();
        $_SESSION['editTicket'] = null;
        $cal = $this->calendarDAO->getById($idCalendar);
        $ticket->setCalendar($cal);
        $ticket->setTicketType( $this->ticketTypeDAO->getById($idTicketType));
        $ticket->setPrice($price);
        $ticket->setAvailable($available);
        $this->ticketDAO->modify($idTicket, $idTicketType, $price, $available);
        $this->createTicketView();
    }

    public function editTicket($idTicket, $idCalendar, $idTicketType, $price, $available)
    {
        $ticketTypeList = $this->ticketTypeDAO->getAllActives();
        $ticket = $this->ticketDAO->getById($idTicket);
        $cal = $this->calendarDAO->getById($idCalendar);
        
        $_SESSION['editTicket'] = true;
        require_once VIEWS_PATH.'editTicketView.php';
    }
    public function deleteTicket($id)
    {
        $hasPurchasedTickets = false;
        $ticketCompare = $this->ticketDAO->getById($id);
        $purchasedTickets = $this->purchasedTicketDAO->getAllActives();
        foreach ($purchasedTickets as $purchasedTicket) {
            $ticket = $this->ticketDAO->getById($purchasedTicket->getIdTicket());
            if ($ticket->getId() == $ticketCompare->getId()) {
                $hasPurchasedTickets = true;
            }
        }
        if ($hasPurchasedTickets) {
            $this->message = " El ticket ya fue comprado ";
            $this->createTicketView();
    
        } else {
            $this->ticketDAO->logicDelete($id);
            $this->createTicketView();    
        }
        
        }


}
