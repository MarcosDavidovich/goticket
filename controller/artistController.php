<?php
namespace controller;

//use dao\artistDAOPDO as ArtistDAOPDO;
use model\artist as Artist;
use dao\artistDAOPDO as ArtistDAOPDO;
use dao\calendarDAOPDO as CalendarDAOPDO;

class ArtistController
{
    private $artistDAO;
    private $calendarDAO;
    private $message;
    public function __construct()
    {

        //$artistDAOPDO = new ArtistDAOPDO();
        $this->artistDAO = new ArtistDAOPDO();
        $this->calendarDAO = new CalendarDAOPDO();
        
    }

    public function index()
    {
    }
    
    public function artistView($message = "")
    {
        $message = $this->message;
        $artistList = $this->artistDAO->getAllActives();
        require_once VIEWS_PATH."artistView.php";
    }
    public function editArtistView($idArtist, $message = " ")
    {
        $artist = $this->artistDAO->getById($idArtist);
        require_once VIEWS_PATH."editArtistView.php";
    }
    
    public function listArtistView($keyword="")
    {
        if($keyword!="")
        {
            $artistList = $this->artistDAO->getByName(strtolower($keyword));  //filtra segun busqueda
        } else {
            $artistList = $this->artistDAO->getAllActives();
        }
        // var_dump($artistList);
        require_once VIEWS_PATH."listArtistView.php";
    }
    
    public function createArtist($name, $description)
    {
        if ($this->isArtistCreated($name)) {
            $this->artistView("Ya existe el artista ".$name."");    
        } else {
        $artist = new artist();
        $artist->setName($name);
        $artist->setDescription($description);
        $this->artistDAO->add($artist);
        $this->message = 'Exito!';
        $this->artistView();
        }
    }
    public function isArtistCreated($name)
    {
        $isArtistCreated = false;
        $artistList = $this->artistDAO->getAllActives();
        foreach ($artistList as $artist) {
            $nameCapital = $artist->getName($name);
            $artist->setName(strtolower($nameCapital));
            $nameCompare = $artist->getName();
            $nameAux = strtolower($name);
            if ($nameCompare == $nameAux) {
                $isArtistCreated = true;
            }
        }
        return $isArtistCreated;
    }
    public function editArtist($idArtist,$name, $description)
    {
        
        $this->artistDAO->modify($idArtist, $name, $description);
        $this->artistView("EXITO!");
        
    }

    public function deleteArtist($id)
    {
        $hasCalendars = false;
        $artist = $this->artistDAO->getById($id);
        $calendars = $this->calendarDAO->getCalendarsByArtist($id);
        if (!empty($calendars)) {
            $hasCalendars = true;
        }
       if ($hasCalendars) {
        $this->message = " El artista  tiene calendarios relacionados ";
        $this->artistView();
       } else {
        $this->artistDAO->logicDelete($id);
        $this->message = " Exito!";
        $this->artistView();
       }
    }
    

}
?>
