<?php
namespace controller;

use model\venue as venue;
use dao\venueDAO as venueDAO;
use dao\venueDAOPDO as VenueDAOPDO;
use dao\cityDAOPDO as CityDAOPDO;
use dao\calendarDAOPDO as calendarDAOPDO;
use model\city as City;

class VenueController {

    private $venueDAO;
    private $cityDAO;
    private $calendarDAO;
    private $message;

    public function __construct()
    {

        //$artistDAOPDO = new ArtistDAOPDO();
        $this->venueDAO = new VenueDAOPDO();
        $this->cityDAO = new CityDAOPDO();
        $this->calendarDAO = new calendarDAOPDO();

        
    }


    public function index()
    {
        // el action se forma /"nombre del proyecto"/"nombre del controler"/"metodo"
        include_once VIEWS_PATH."venueView.php";
    }

    public function venueView($message = " ")
    {   
        $venue = new venue();
        $_SESSION['editVenue'] = null;
        $cityList = $this->cityDAO->getAllActives();
        $venueList = $this->venueDAO->getAllActives();
        require_once VIEWS_PATH."venueView.php";
    }
    


    public function createVenue($name, $description, $address, $city)
    {   
        if ($this->isVenueCreated($name)) {
            $this->venueView(" Ya existe el lugar con nombre ".$name);
        } else {
        $venue = new venue();
        $cityList = $this->cityDAO->getAllActives();
        $venue->setName($name);
        $venue->setDescription($description);
        $venue->setAddress($address);
        $venue->setCity( $this->cityDAO->getById($city));
        $_SESSION['editVenue'] = null;
        $this->venueDAO->add($venue);
        $this->venueView("EXITO!");
        }
    }
    public function isVenueCreated($name)
    {
        $isVenueCreated = false;
        $venueList = $this->venueDAO->getAllActives();
        foreach ($venueList as $venue) {
            $nameCapital = $venue->getName();
            $nameNoCapital = strtolower($nameCapital);
            $nameNoCapitalCompare =  strtolower($name);
            if ($nameNoCapital == $nameNoCapitalCompare) {
                $isVenueCreated = true;
            }
        }
        return $isVenueCreated;
    }
    public function modifyVenue($idVenue, $name, $description, $address, $idCity)
    {
        
        $venue = new venue();
        $_SESSION['editVenue'] = null;
        $venue->setName($name);
        $venue->setDescription($description);
        $venue->setAddress($address);
        $venue->setCity($this->cityDAO->getById($idCity));
        $venue->setId($idVenue);
        $this->venueDAO->modify($idVenue, $name, $description, $address, $idCity);
        $this->venueView();
        
    }
    
    public function editVenue($idVenue, $name, $description, $address, $idCity)
    {
        $message = " ";
        $cityList = $this->cityDAO->getAllActives();
        $venue = $this->venueDAO->getById($idVenue);
        $venue->setName($name);
        $venue->setDescription($description);
        $venue->setAddress($address);
        $venue->setCity($this->cityDAO->getById($idCity));
        $venueList = $this->venueDAO->getAllActives();
        $_SESSION['editVenue'] = true;
        require_once VIEWS_PATH.'venueView.php';
    }
    public function deleteVenue($id)
    {
        $hasCalendars= false;
        $calendarList = $this->calendarDAO->getAllActives();
        foreach ($calendarList as $calendar) {
            if ($calendar->getVenue()->getId() == $id) {
                $hasCalendars = true;
            }
        }
        if ($hasCalendars) {
            $this->venueView(" El lugar ya pertenece a un calendario");
        } else {
        
            $this->venueDAO->logicDelete($id);
            $this->venueView(" Exito!");
        }
        
    }

}

?>