<?php
namespace controller;

use model\category as category;
use dao\categoryDAO as categoryDAO;
use dao\categoryDAOPDO as categoryDAOPDO;
use dao\eventDAOPDO as eventDAOPDO;

class CategoryController {

    private $eventDAO;
    private $categoryDAO;
    private $message;

    public function __construct()
    {

        $this->eventDAO = new eventDAOPDO();
        $this->categoryDAO = new categoryDAOPDO();
        
    }


    public function index()
    {
        // el action se forma /"nombre del proyecto"/"nombre del controler"/"metodo"
        include_once VIEWS_PATH."categoryView.php";
    }

    public function categoryView($message = "")
    {   
        $category = new category();
        $_SESSION['editCategory'] = null;
        $categoryList = $this->categoryDAO->getAllActives();
        require_once VIEWS_PATH."categoryView.php";
    }


    public function createCategory($name)
    {   
        if ($this->isCategoryCreated($name)) {
            $this->categoryView(" Categoria con nombre ".$name." ya creada ");    
        } else {
        $category = new category();
        $categoryList = $this->categoryDAO->getAllActives();
        $_SESSION['editCategory'] = null;
        $category->setName($name);
        $this->categoryDAO->add($category);
        $this->categoryView("EXITO!");
        }
    }
    public function isCategoryCreated($name)
    {
        $isCategoryCreated = false;
        $categoryList = $this->categoryDAO->getAllActives();
        foreach ($categoryList as $category) {
            $nameCapital = $category->getName();
            $nameNoCapital = strtolower($nameCapital);
            $nameNoCapitalCompare =  strtolower($name);
            if ($nameNoCapital == $nameNoCapitalCompare) {
                $isCategoryCreated = true;
            }
        }
        return $isCategoryCreated;
    }
    public function modifyCategory($idCategory, $name)
    {
        if ($this->isCategoryCreated($name)) {
            $this->categoryView(" Categoria con nombre ".$name." ya creada ");    
        } else {
        $category = new category();
        $_SESSION['editCategory'] = null;
        $category->setName($name);
        $category->setId($idCategory);
        $this->categoryDAO->modify($idCategory,$name);
        $this->categoryView(" Exito!");
        }
    }
    public function editCategory($idCategory, $name)
    {
        $message = "";
        $category = $this->categoryDAO->getById($idCategory);
        $category->setName($name);
        $categoryList = $this->categoryDAO->getAllActives();
        $_SESSION['editCategory'] = true;
        
        
        require_once VIEWS_PATH.'categoryView.php';
        
        
    }
    public function deleteCategory($id)
    {
        $hasEvents = false;
        $eventList = $this->eventDAO->getAllActives();
        foreach ($eventList as $event) {
            if ($event->getCategory()->getId() == $id) {
                $hasEvents = true;
            }
        }
        if ($hasEvents) {
            $this->categoryView(" La categoria ya tiene eventos creados ");
        } else {
            $this->categoryDAO->logicDelete($id);
            $this->categoryView(" Exito! ");
        }
        
    }



}

?>