<?php
namespace controller;

// use model\artist as artist;
use dao\eventDAOPDO as eventDAOPDO;
use dao\purchasedTicketDAOPDO as purchasedTicketDAOPDO;

class HomeController
{
    private $eventDAO;
    private $purchasedTicketDAO;

    public function __construct()
    {
        $this->eventDAO = new eventDAOPDO();
        $this->purchasedTicketDAO = new purchasedTicketDAOPDO();
    }

    public function index()
    {
        // el action se forma /"nombre del proyecto"/"nombre del controler"/"metodo"
        $eventList = $this->eventDAO->getAllActives();
        require_once VIEWS_PATH."homeView.php";
    }
    
    public function search($keyword)
    {
        //reemplazar por resultados
        $eventList = $this->eventDAO->getByName($keyword);
        if ($keyword == "") {
            require_once VIEWS_PATH."homeView.php";
        } else{
        $keyword = strtolower($keyword);
        require_once VIEWS_PATH."searchView.php";
    }
    }

    public function dashboardView($date1 = "", $date2 = "")
    {
  
        $hoy = date("Y-m-d");
        $semant  = date("Y-m-d",strtotime("-7 days"));
        $mesant  = date("Y-m-d",strtotime("-1 month"));
        $range=false;

        if($date1!="" && $date2!=""){
            $range=true;
            $rta= $this->calculateSales($date1, $date2);
        }

        $daily = $this->calculateSales($hoy, $hoy);
        $weekly = $this->calculateSales($semant, $hoy);
        $monthly = $this->calculateSales($mesant, $hoy);
        
        require_once VIEWS_PATH."dashboardView.php";
    }

    // calcula el total de tickets vendidos y $$ de un rango de fechas
    public function calculateSales($date1, $date2)
    {
        $purchasedTickets = $this->purchasedTicketDAO->getInDates($date1, $date2);
        $cant=0;
        $total=0;
        $rta = array();
        foreach($purchasedTickets as $pt)
        {  
            $cant++;
            $total += $pt->getPrice();
        }
        $rta["cantidad"]= $cant;
        $rta["total"]= $total;
        return $rta;
    }

    public function logout()
    {
        session_destroy();
       
        //borra los qr temp files
        $files = glob('lib/tmp/*'); //obtenemos todos los nombres de los archivos
        foreach($files as $file){
            if(is_file($file))
            unlink($file); //elimino el archivo
        }

        $this->index();
        echo '<script type="text/javascript">',
        'myAlert();',
        '</script>';
    }
    
   
}
?>
