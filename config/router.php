<?php 
namespace config;
    class Router {
        /**
         * Se encarga de direccionar a la pagina solicitada
         * @param Request
         */
        public function __construct()
        {
            
        }
        public static function direccionar(Request $request) 
        {
            
            $controlador = $request->getControladora()."Controller";
            $metodo = $request->getMetodo();
            $parametros = $request->getParametros();
          
            $objeto = "controller\\". $controlador;
            $controlador = new $objeto();


            if(!isset($parametros)) 
                      
            {
                call_user_func(array($controlador, $metodo));
            } else 
            {
                // era call user func array pero enviava el arreglo como string. 
                call_user_func_array(array($controlador, $metodo),$parametros);
            }
        }
    }
 ?>