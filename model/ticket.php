<?php
namespace model;
use model\ticketType as ticketType;
use model\calendar as calendar;
class ticket
{
    private $id;
    private $calendar;
    private $ticketType;
    private $price;
    private $available;
    private $remain;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function getCalendar()
    {
        return $this->calendar;
    }
    public function setCalendar(calendar $calendar)
    {
        $this->calendar = $calendar;
    }

    public function setTicketType(ticketType $ticketType)
    {
        $this->ticketType = $ticketType;
    }

    public function getTicketType()
    {
        return $this->ticketType;
    }

    public function getAvailable()
    {
        return $this->available;
    }
    public function setAvailable($available)
    {
        $this->available = $available;
    }
    public function getPrice()
    {
        return $this->price;
    }
    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getRemain()
    {
        return $this->remain;
    }
    public function setRemain($remain)
    {
        $this->remain = $remain;
    }

}
