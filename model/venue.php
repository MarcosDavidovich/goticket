<?php
namespace model;
class venue {
	private $id;
	private $name;
	private $address;
	private $city;
	private $description;
	// Segun el DOM un venue tendria que tener ticket types por defecto?
	
	//private $ticketType = array();
	// public function addTicketTypes(TicketType $ticketType)
    // {
    //     array_push($this->ticketTypes, $ticketType);
    // }
    // public function getTicketTypes()
    // {
    //     return $this->ticketTypes;
    // }

	public function getId()
	{
		return $this->id;
	}
	public function setId($id)
	{
		$this->id = $id;
	}
	public function getAddress()
	{
		return $this->address;
	}
	public function setAddress($address)
	{
		$this->address = $address;
	}

	public function getCity()
	{
		return $this->city;
	}
	public function setCity($city)
	{
		$this->city = $city;
	}

	public function getDescription() {
		return $this->description;
	}
	public function setDescription($description) {
		$this->description = $description;
	}
	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}




}
?>