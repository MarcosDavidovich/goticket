<?php
namespace model;
class Customer
{
    private $id;
	private $name;
    private $purchaces = array();

	public function getId()
	{
		return $this->id;
	}
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function getname() {
		return $this->name;
	} 
	public function setname($name) {
		$this->name = $name;
    }
    
    public function addPurchace(Purchase $purchace)
    {
        array_push($this->purchaces, $purchace);
    }
    public function getPurchaces()
    {
        return $this->purchaces;
    }


}
?>