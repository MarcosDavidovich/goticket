<?php
namespace model;

class PurchasedTicket {
    private $idTicket;
    private $idPurchase;
    private $price;
    private $id;
    private $qr;

    public function getQr()
    {
        return $this->qr;
    }

    public function setQr($qr)
    {
        $this->qr = $qr;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getIdTicket()
    {
        return $this->idTicket;
    }

    public function setIdTicket($idTicket)
    {
        $this->idTicket = $idTicket;

        return $this;
    }

    public function getIdPurchase()
    {
        return $this->idPurchase;
    }

    public function setIdPurchase($idPurchase)
    {
        $this->idPurchase = $idPurchase;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }
}




?>