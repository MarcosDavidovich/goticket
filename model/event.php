<?php
namespace model;

use model\artist as artist;
use model\category as category;

class event
{
    private $id;
    private $name;
    private $description;
    private $category;
    private $coverImg;
    private $sqImg;

    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
    }

    // TODO TIPARLO?
    //Para mi que si.
    public function setCategory(category $category)
    {
        $this->category = $category;
    }
    public function getCategory()
    {
        return $this->category;
    }

    public function getDescription()
    {
        return $this->description;
    }
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    public function getCoverImg()
    {
        return $this->coverImg;
    }
    public function setCoverImg($coverImg)
    {
        $this->coverImg = $coverImg;
    }
    public function getSqImg()
    {
        return $this->sqImg;
    }
    public function setSqImg($sqImg)
    {
        $this->sqImg = $sqImg;
    }

}
