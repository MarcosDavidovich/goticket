<?php
namespace model;
use model\purchaseLine  as purchaseLine;

class purchase {
    private $id;
    private $userId;
    private $date;
    private $lines = array();
    private $total;
    // por las dudas si llegare a haber descuentos de algun tipo
    private $discount = 0;

    public function setId($id)  
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
    public function setDate($date)  
    {
        $this->date = $date;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function addLine(PurchaseLine $line)
    {
        array_push($this->lines, $line);
    }
    public function getLines()
    {
        return $this->lines;
    }

    public function calculateTotal()
    {
        $subtotal = 0;
        foreach ($this->lines as $line) {  
            $subtotal += $line->getPrice(); // NOT TESTED
        }
        // DESCUENTOS
        $subtotal = $subtotal - $this->discount;
        $this->total = $subtotal;
    }
    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }
 
    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    public function setLines($lines)
    {
        $this->lines = $lines;

        return $this;
    }

    public function getTicketAmount()
    {
        $total=0;
        foreach($this->lines as $line)
        {
            $total += $line->getQuantity();
        }
        return $total;
    }



}

?>