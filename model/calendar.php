<?php
namespace model;

use model\ticket as ticket;
use model\venue as venue;
use model\artist as artist;
use model\event as event;

class calendar
{
    private $id;
    private $date;
    private $venue;
    private $event;
    private $artists = array();

    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }
    public function getDate()
    {
        return $this->date;
    }
    public function setDate($date)
    {
        $this->date = $date;
    }
    public function setVenue(venue $venue)
    {
        $this->venue = $venue;
    }
    public function getVenue()
    {
        return $this->venue;
    }
    public function setEvent(event $event)
    {
        $this->event = $event;
    }
    public function getEvent()
    {
        return $this->event;
    }


    public function addArtist(artist $artist)
    {
        array_push($this->artists, $artist);
    }
    public function getArtists()
    {
        return $this->artists;
    }
    public function setArtists($artistArray)
    {
        $this->artists = $artistArray;
    }
}
