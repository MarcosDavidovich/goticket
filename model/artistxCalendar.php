<?php
namespace model;

class artistxCalendar 
{
    private $idArtist;
    private $idCalendar;
    
    public function getIdCalendar()
    {
        return $this->idCalendar;
    }

    public function setIdCalendar($idCalendar)
    {
        $this->idCalendar = $idCalendar;

        return $this;
    }
 
    public function getIdArtist()
    {
        return $this->idArtist;
    }

    public function setIdArtist($idArtist)
    {
        $this->idArtist = $idArtist;

        return $this;
    }
    
}
?>