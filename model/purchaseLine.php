<?php 
namespace model;
class purchaseLine{

    private $id;
    private $idTicket;
    private $idPurchase;
    private $quantity;
    private $price;

    public function setIdTicket($idTicket)
    {
        $this->idTicket = $idTicket;
    }
    public function getIdTicket()
    {
        return $this->idTicket;
    }
    public function setIdPurchase($idPurchase)
    {
        $this->idPurchase = $idPurchase;
    }
    public function getIdPurchase()
    {
        return $this->idPurchase;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function setPrice ($price){
        $this->price = $price;
    }
    public function getPrice()
    {
        return $this->price;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}


?>
