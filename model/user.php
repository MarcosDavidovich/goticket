<?php
namespace model;

class User
{
    private $id;
    private $email;
    private $userName;
    private $password;
    private $userType;
    
    public function getUserType()
    {
        return $this->userType;
    }
 
    public function setUserType($userType)
    {
        $this->userType = $userType;

        return $this;
    }
 
    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }
 
    public function getUserName()
    {
        return $this->userName;
    }

    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }
 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }
     
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
?>