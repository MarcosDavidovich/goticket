<?php
namespace dao;
use model\city as city;

class CityDAO implements iCityDAO
{
    private $cityArray = array();

    public function __construct(){
        if(!isset($_SESSION["cities"])){   // consulta si no esta creado en session
            $_SESSION["cities"] = array();   // crea array en session
        }
        $this->cityArray = &$_SESSION["cities"];    // vincula el local con el de session
    }

    public function getAll()
    {
        return $this->cityArray;
    }
    public function add(city $city)
    {
        $city->setId( \rand(0,999));
        array_push($this->cityArray, $city);
    }
    public function getById($id)
    {
        $city = null;
        foreach ($this->cityArray as $cityCompare) {
            if ($cityCompare->getId() == $id) {
                $city = $cityCompare;
            }
        }
    return $city;
    }

}
?>