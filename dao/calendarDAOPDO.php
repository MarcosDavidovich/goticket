<?php
namespace dao;
use model\calendar as Calendar;
use model\event as Event;
use model\venue as Venue;
use model\category as Category;
use model\city as City;
use model\artist as Artist;
class calendarDAOPDO implements iCalendarDAO
{
    private $connection = null;
    private $tableName = 'calendars';

    public function getAll()
    {
        try
        {
        $calendarsArray = array();
        $query = 'SELECT c.id as calendarId, c.date as calendarDate,';
        $query .= ' v.id as venueId, v.name as venueName, v.address as venueAddress, v.description as venueDescription,';
        $query .= ' ci.id as cityId, ci.name as cityName,';
        $query .= ' e.id as eventId, e.name as eventName, e.description as eventDescription,';
        $query .= ' ca.id as categoryId, ca.name as categoryName, ca.description as categoryDescription ';
        $query .= ' FROM '.$this->tableName.' as c ';
        $query .= ' INNER JOIN venues as v ON c.idVenue = v.id ';
        $query .= ' INNER JOIN events as e ON c.idEvent = e.id ';
        $query .= ' INNER JOIN cities as ci ON v.idCity = ci.id ';
        $query .= ' INNER JOIN categories as ca ON e.idCategory = ca.id; ';
        $this->connection = connection::getInstance();
        $resultSet = $this->connection->execute($query);
        foreach ($resultSet as $row) {
            $calendar = new Calendar();
            $event = new Event();
            $venue = new Venue();
            $city = new City();
            $category = new Category();
            $city->setId($row['cityId']);
            $city->setName($row['cityName']);
            $category->setId($row['categoryId']);
            $category->setName($row['categoryName']);
            $category->setDescription($row['categoryDescription']);
            $venue->setId($row['venueId']);
            $venue->setName($row['venueName']);
            $venue->setDescription($row['venueDescription']);
            $venue->setAddress($row['venueAddress']);
            $venue->setCity($city);
            $event->setId($row['eventId']);
            $event->setName($row['eventName']);
            $event->setDescription($row['eventDescription']);
            $event->setCategory($category);
            $calendar->setId($row['calendarId']);
            $calendar->setDate($row['calendarDate']);
            $calendar->setEvent($event);
            $calendar->setVenue($venue);
            array_push($calendarsArray, $calendar);
        }
        return $calendarsArray;    
        } catch (Exception $e)
        {
            throw $e;
        }
    }
    public function getAllActives()
    {
        try
        {
        $calendarsArray = array();
        $query = 'SELECT c.id as calendarId, c.date as calendarDate,';
        $query .= ' v.id as venueId, v.name as venueName, v.address as venueAddress, v.description as venueDescription,';
        $query .= ' ci.id as cityId, ci.name as cityName,';
        $query .= ' e.id as eventId, e.name as eventName, e.description as eventDescription,';
        $query .= ' ca.id as categoryId, ca.name as categoryName, ca.description as categoryDescription ';
        $query .= ' FROM '.$this->tableName.' as c ';
        $query .= ' INNER JOIN venues as v ON c.idVenue = v.id ';
        $query .= ' INNER JOIN events as e ON c.idEvent = e.id ';
        $query .= ' INNER JOIN cities as ci ON v.idCity = ci.id ';
        $query .= ' INNER JOIN categories as ca ON e.idCategory = ca.id ';
        $query .= ' WHERE c.erased = 0; ';
        $this->connection = connection::getInstance();
        $resultSet = $this->connection->execute($query);
        foreach ($resultSet as $row) {
            $calendar = new Calendar();
            $event = new Event();
            $venue = new Venue();
            $city = new City();
            $category = new Category();
            $city->setId($row['cityId']);
            $city->setName($row['cityName']);
            $category->setId($row['categoryId']);
            $category->setName($row['categoryName']);
            $category->setDescription($row['categoryDescription']);
            $venue->setId($row['venueId']);
            $venue->setName($row['venueName']);
            $venue->setDescription($row['venueDescription']);
            $venue->setAddress($row['venueAddress']);
            $venue->setCity($city);
            $event->setId($row['eventId']);
            $event->setName($row['eventName']);
            $event->setDescription($row['eventDescription']);
            $event->setCategory($category);
            $calendar->setId($row['calendarId']);
            $calendar->setDate($row['calendarDate']);
            $calendar->setEvent($event);
            $calendar->setVenue($venue);
            array_push($calendarsArray, $calendar);
        }
        return $calendarsArray;    
        } catch (Exception $e)
        {
            throw $e;
        }
    }


    public function add(Calendar $calendar)
    {
        try 
        {
            $query = 'INSERT INTO '.$this->tableName.' (date,idVenue,idEvent) VALUES (:date, :idVenue, :idEvent);';
            $parameters['date'] = $calendar->getDate();
            $parameters['idVenue'] = $calendar->getVenue()->getId();
            $parameters['idEvent'] = $calendar->getEvent()->getId();
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();
        } catch (Exception $e)
        {
            throw $e;
        }
    }

    public function getById($id)
    {
        try
        {
        $query = 'SELECT c.id as calendarId, c.date as calendarDate,';
        $query .= ' v.id as venueId, v.name as venueName, v.address as venueAddress, v.description as venueDescription,';
        $query .= ' ci.id as cityId, ci.name as cityName,';
        $query .= ' e.id as eventId, e.name as eventName, e.description as eventDescription,';
        $query .= ' ca.id as categoryId, ca.name as categoryName, ca.description as categoryDescription ';
        $query .= ' FROM '.$this->tableName.' as c ';
        $query .= ' INNER JOIN venues as v ON c.idVenue = v.id ';
        $query .= ' INNER JOIN events as e ON c.idEvent = e.id ';
        $query .= ' INNER JOIN cities as ci ON v.idCity = ci.id ';
        $query .= ' INNER JOIN categories as ca ON e.idCategory = ca.id ';
        $query .= ' WHERE c.id = :id;';
        $parameters['id'] = $id;
        $this->connection = connection::getInstance();
        $resultSet = $this->connection->execute($query,$parameters);
        // estaba adentro del foreach, lo saque porque tiraba bronca
        $calendar = new Calendar(); 
        foreach ($resultSet as $row) {
            $event = new Event();
            $venue = new Venue();
            $city = new City();
            $category = new Category();
            $city->setId($row['cityId']);
            $city->setName($row['cityName']);
            $category->setId($row['categoryId']);
            $category->setName($row['categoryName']);
            $category->setDescription($row['categoryDescription']);
            $venue->setId($row['venueId']);
            $venue->setName($row['venueName']);
            $venue->setDescription($row['venueDescription']);
            $venue->setAddress($row['venueAddress']);
            $venue->setCity($city);
            $event->setId($row['eventId']);
            $event->setName($row['eventName']);
            $event->setDescription($row['eventDescription']);
            $event->setCategory($category);
            $calendar->setId($row['calendarId']);
            $calendar->setDate($row['calendarDate']);
            $calendar->setEvent($event);
            $calendar->setVenue($venue);
        }
        return $calendar;    
        } catch (Exception $e)
        {
            throw $e;
        }        
    }

    public function getCalendarsByArtist($artistId)
    {
        try
        {
        $calendarsArray = array();
        $query = 'SELECT c.id as calendarId, c.date as calendarDate,';
        $query .= ' v.id as venueId, v.name as venueName, v.address as venueAddress, v.description as venueDescription,';
        $query .= ' ci.id as cityId, ci.name as cityName,';
        $query .= ' e.id as eventId, e.name as eventName, e.description as eventDescription,';
        $query .= ' ifnull(ic.name, "ticketgocover.jpg") as coverImg, ifnull(isq.name, "ticketgosq.jpg") as sqImg,';
        $query .= ' ca.id as categoryId, ca.name as categoryName, ca.description as categoryDescription ';
        $query .= ' FROM '.$this->tableName.' as c ';
        $query .= ' INNER JOIN venues as v ON c.idVenue = v.id ';
        $query .= ' INNER JOIN events as e ON c.idEvent = e.id ';
        $query .= ' LEFT JOIN images as ic ON e.idCoverImg = ic.id';
        $query .= ' LEFT JOIN images as isq ON e.idSqImg = isq.id ';
        $query .= ' INNER JOIN cities as ci ON v.idCity = ci.id ';
        $query .= ' INNER JOIN categories as ca ON e.idCategory = ca.id ';
        $query .= ' INNER JOIN artistxCalendar as axc on axc.idCalendar = c.id ';
        $query .= ' WHERE axc.idArtist = :id and e.erased = 0';
        $query .= ' ORDER BY c.id; ';
        $parameters['id'] = $artistId;
        $this->connection = connection::getInstance();
        $resultSet = $this->connection->execute($query, $parameters);
        foreach ($resultSet as $row) {
            $calendar = new Calendar();
            $event = new Event();
            $venue = new Venue();
            $city = new City();
            $category = new Category();
            $city->setId($row['cityId']);
            $city->setName($row['cityName']);
            $category->setId($row['categoryId']);
            $category->setName($row['categoryName']);
            $category->setDescription($row['categoryDescription']);
            $venue->setId($row['venueId']);
            $venue->setName($row['venueName']);
            $venue->setDescription($row['venueDescription']);
            $venue->setAddress($row['venueAddress']);
            $venue->setCity($city);
            $event->setId($row['eventId']);
            $event->setName($row['eventName']);
            $event->setDescription($row['eventDescription']);
            $event->setCategory($category);
            $event->setCoverImg($row['coverImg']);
            $event->setSqImg($row['sqImg']);

            $calendar->setId($row['calendarId']);
            $calendar->setDate($row['calendarDate']);
            $calendar->setEvent($event);
            $calendar->setVenue($venue);
            array_push($calendarsArray, $calendar);
        }
        return $calendarsArray;    
        } catch (Exception $e)
        {
            throw $e;
        }    
    }

    public function getByEventId($id)
    {
        try
        {
        $query = 'SELECT c.id as calendarId, c.date as calendarDate,';
        $query .= ' v.id as venueId, v.name as venueName, v.address as venueAddress, v.description as venueDescription,';
        $query .= ' ci.id as cityId, ci.name as cityName,';
        $query .= ' e.id as eventId, e.name as eventName, e.description as eventDescription,';
        $query .= ' ca.id as categoryId, ca.name as categoryName, ca.description as categoryDescription ';
        $query .= ' FROM '.$this->tableName.' as c ';
        $query .= ' INNER JOIN venues as v ON c.idVenue = v.id ';
        $query .= ' INNER JOIN events as e ON c.idEvent = e.id ';
        $query .= ' INNER JOIN cities as ci ON v.idCity = ci.id ';
        $query .= ' INNER JOIN categories as ca ON e.idCategory = ca.id ';
        $query .= ' WHERE c.idEvent = :id AND c.erased = 0;';
        $parameters['id'] = $id;
        $this->connection = connection::getInstance();
        $resultSet = $this->connection->execute($query,$parameters);
        $calendarsArray= array();
        foreach ($resultSet as $row) {
            $calendar = new Calendar(); 
            $event = new Event();
            $venue = new Venue();
            $city = new City();
            $category = new Category();
            $city->setId($row['cityId']);
            $city->setName($row['cityName']);
            $category->setId($row['categoryId']);
            $category->setName($row['categoryName']);
            $category->setDescription($row['categoryDescription']);
            $venue->setId($row['venueId']);
            $venue->setName($row['venueName']);
            $venue->setDescription($row['venueDescription']);
            $venue->setAddress($row['venueAddress']);
            $venue->setCity($city);
            $event->setId($row['eventId']);
            $event->setName($row['eventName']);
            $event->setDescription($row['eventDescription']);
            $event->setCategory($category);
            $calendar->setId($row['calendarId']);
            $calendar->setDate($row['calendarDate']);
            $calendar->setEvent($event);
            $calendar->setVenue($venue);
            array_push($calendarsArray, $calendar);
        }
        return $calendarsArray;    
        } catch (Exception $e)
        {
            throw $e;
        }        
    }

    public function getInDates($date1, $date2)
    {
        try
        {
        $calendarsArray = array();
        $query = 'SELECT c.id as calendarId, c.date as calendarDate,';
        $query .= ' v.id as venueId, v.name as venueName, v.address as venueAddress, v.description as venueDescription,';
        $query .= ' ci.id as cityId, ci.name as cityName,';
        $query .= ' e.id as eventId, e.name as eventName, e.description as eventDescription,';
        $query .= ' ifnull(ic.name, "ticketgocover.jpg") as coverImg, ifnull(isq.name, "ticketgosq.jpg") as sqImg,';
        $query .= ' ca.id as categoryId, ca.name as categoryName, ca.description as categoryDescription ';
        $query .= ' FROM '.$this->tableName.' as c ';
        $query .= ' INNER JOIN venues as v ON c.idVenue = v.id ';
        $query .= ' INNER JOIN events as e ON c.idEvent = e.id ';
        $query .= ' LEFT JOIN images as ic ON e.idCoverImg = ic.id';
        $query .= ' LEFT JOIN images as isq ON e.idSqImg = isq.id ';
        $query .= ' INNER JOIN cities as ci ON v.idCity = ci.id ';
        $query .= ' INNER JOIN categories as ca ON e.idCategory = ca.id ';
        $query .= ' INNER JOIN artistxCalendar as axc on axc.idCalendar = c.id ';
        $query .= ' WHERE c.date >= :date1 AND c.date <= :date2 AND e.erased=0; ';
        $parameters['date1'] = $date1;
        $parameters['date2'] = $date2;
        $this->connection = connection::getInstance();
        $resultSet = $this->connection->execute($query, $parameters);
        foreach ($resultSet as $row) {
            $calendar = new Calendar();
            $event = new Event();
            $venue = new Venue();
            $city = new City();
            $category = new Category();
            $city->setId($row['cityId']);
            $city->setName($row['cityName']);
            $category->setId($row['categoryId']);
            $category->setName($row['categoryName']);
            $category->setDescription($row['categoryDescription']);
            $venue->setId($row['venueId']);
            $venue->setName($row['venueName']);
            $venue->setDescription($row['venueDescription']);
            $venue->setAddress($row['venueAddress']);
            $venue->setCity($city);
            $event->setId($row['eventId']);
            $event->setName($row['eventName']);
            $event->setDescription($row['eventDescription']);
            $event->setCategory($category);
            $event->setCoverImg($row['coverImg']);
            $event->setSqImg($row['sqImg']);
            $calendar->setId($row['calendarId']);
            $calendar->setDate($row['calendarDate']);
            $calendar->setEvent($event);
            $calendar->setVenue($venue);
            array_push($calendarsArray, $calendar);
        }
        return $calendarsArray;    
        } catch (Exception $e)
        {
            throw $e;
        }    

    }

    public function delete($id)
    {
        try
        {
            $query = "DELETE FROM ".$this->tableName." WHERE id = :id ;";
            $parameters["id"] = $id;
            $this->connection = connection::GetInstance();
            $this->connection->ExecuteNonQuery($query, $parameters);   
        }
        catch(Exception $ex)
        {
            throw $ex;
        }
    }
    public function logicDelete($id)
    {
        try
        {
            $parameters['id'] = $id;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET erased=1 ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }

    public function update(Calendar $calendar)
    {
        try
        {
            $query = 'UPDATE '.$this->tableName;
            $query .= ' SET date = :date, idVenue = :idVenue, idEvent = :idEvent';
            $query .= ' WHERE id = :idCalendar;';
            $parameters['idCalendar'] = $calendar->getId();
            $parameters['date'] = $calendar->getDate();
            $parameters['idVenue'] = $calendar->getVenue()->getId();
            $parameters['idEvent'] = $calendar->getEvent()->getId();
            $this->connection = Connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $calendar->getId();
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
}
?>