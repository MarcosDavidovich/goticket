<?php
namespace dao;
use model\artist as Artist;

class ArtistDAOPDO implements iArtistDAO
{
    private $connection = null;
    private $tableName = 'artists';
    
    public function getAll(){
        try
        {
            $artistList = array();
            $query = 'SELECT * FROM '.$this->tableName.';';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $artist = new Artist();
                $artist->setId($row['id']);
                $artist->setName($row['name']);
                $artist->setDescription($row['description']);
                array_push($artistList, $artist);
            }
            return $artistList;
        }catch(Exception $e) {
            throw $e;
        }
    }
    public function getAllActives(){
        try
        {
            $artistList = array();
            $query = 'SELECT * FROM '.$this->tableName.' as a';
            $query .= ' WHERE a.erased = 0;';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $artist = new Artist();
                $artist->setId($row['id']);
                $artist->setName($row['name']);
                $artist->setDescription($row['description']);
                array_push($artistList, $artist);
            }
            return $artistList;
        }catch(Exception $e) {
            throw $e;
        }
    }
    public function add(Artist $artist){
        try
        {
            $query = 'INSERT INTO '.$this->tableName.' (name,description) VALUES (:name,:description);';
            $parameters['name'] = $artist->getName();
            $parameters['description'] = $artist->getDescription();
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();// return the last id inserted.
        }catch(Exception $e) 
        {
            throw $e;
        }
    }
    public function getById($id){
        try{
            $query = 'SELECT * FROM '.$this->tableName.' WHERE id = :id';
            $parameters['id'] = $id; 
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            foreach ($resultSet as $row) {
                $artist = new Artist(); 
                $artist->setId($row['id']);
                $artist->setName($row['name']);
                $artist->setDescription($row['description']);
            }
            return $artist;
        }catch (Exception $e)
        {
            throw $e;
        }
    }

    public function getByName($keyword)
    {
        try{
            $artistList = array();
            $query = 'SELECT id, name, description FROM '.$this->tableName.' WHERE lower(name) like "%'.$keyword.'%" ;';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $artist = new Artist(); 
                $artist->setId($row['id']);
                $artist->setName($row['name']);
                $artist->setDescription($row['description']);
                array_push($artistList, $artist);
            }
            return $artistList;
        }catch (Exception $e)
        {
            throw $e;
        }


    }

    public function getArtists($idCalendar)
    {   
        try
        {
        $artistsArray = array();
        $query = 'SELECT a.id as artistId, a.name as artistName, a.description as artistDescription';
        $query .=' FROM artistxCalendar as ac';
        $query .=' INNER JOIN artists as a ON ac.idArtist = a.id';
        $query .=' WHERE idCalendar = :idCalendar AND a.erased = 0;';
        $parameters['idCalendar'] = $idCalendar;
        $this->connection = connection::getInstance();
        $resultSet = $this->connection->execute($query, $parameters);
        foreach ($resultSet as $row) {
            $artist = new Artist();
            $artist->setId($row['artistId']);
            $artist->setName($row['artistName']);
            $artist->setDescription($row['artistDescription']);
            array_push($artistsArray, $artist);
        }
        return $artistsArray;
        } catch (Exception $e)
        {
            throw $e;
        }
    }
    public function modify($id, $name, $description)
    {
        try
        {
            $parameters['id'] = $id;
            $parameters['name'] = $name;
            $parameters['description'] = $description;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET name=:name, description=:description ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    public function delete($id)
    {
            try
            {
                $query = "DELETE FROM ".$this->tableName." WHERE id = :id";
            
                $parameters["id"] = $id;

                $this->connection = connection::GetInstance();

                $this->connection->ExecuteNonQuery($query, $parameters);   
            }
            catch(Exception $ex)
            {
                throw $ex;
            }            
    }
    public function logicDelete($id)
    {
        try
        {
            $parameters['id'] = $id;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET erased=1 ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    
}
?>