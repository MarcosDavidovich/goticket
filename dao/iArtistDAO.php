<?php
namespace dao;
use model\Artist as Artist;
interface IArtistDAO
{
    public function getAll();
    public function add(Artist $artist);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);
    
}
?>