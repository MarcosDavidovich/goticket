<?php
namespace dao;
use model\PurchaseLine as PurchaseLine;
class PurchaseLineDAO implements IPurchaseLineDAO
{
    private $purchaseLinesArray = array();

    public function getAll()
    {
        return $this->purchaseLinesArray;
    }
    public function add(PurchaseLine $purchaseLine)
    {
        array_push($this->purchaseLinesArray, $purchaseLine);
    }
    public function getById($id)
    {
        $purchaseLine = null;
        foreach ($this->purchaseLinesArray as $purchaseLineCompare) {
            if ($purchaseLineCompare->getId() == $id) {
                $purchaseLine = $purchaseLineCompare;
            }
        }
    return $purchaseLine;
    }
}
?>