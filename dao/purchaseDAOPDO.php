<?php 
namespace dao;
use model\purchase as Purchase;
class purchaseDAOPDO implements iPurchaseDAO
{
    private $connection = null;
    private $tableName = 'purchases';

    public function getAll()
    {
        try
        {
            $purchasesArray = array();
            $query = 'SELECT p.id as purchaseId, p.date as purchaseDate, p.total as purchaseTotal, p.userId as userId';
            $query .= ' FROM '.$this->tableName.' as p;';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $purchase = new Purchase();
                $purchase->setId($row['purchaseId']);
                $purchase->setUserId($row['userId']);
                $purchase->setDate($row['purchaseDate']);
                $purchase->setTotal($row['purchaseTotal']);
                array_push($purchasesArray,$purchase);
            }
            return $purchasesArray;
        } catch (Exception $e)
        {
            throw $e;
        }
    }
    public function getAllActives()
    {
        try
        {
            $purchasesArray = array();
            $query = 'SELECT p.id as purchaseId, p.date as purchaseDate, p.total as purchaseTotal, p.userId as userId';
            $query .= ' FROM '.$this->tableName.' as p';
            $query .= ' WHERE p.erased = 0;';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $purchase = new Purchase();
                $purchase->setId($row['purchaseId']);
                $purchase->setUserId($row['userId']);
                $purchase->setDate($row['purchaseDate']);
                $purchase->setTotal($row['purchaseTotal']);
                array_push($purchasesArray,$purchase);
            }
            return $purchasesArray;
        } catch (Exception $e)
        {
            throw $e;
        }
    }
    public function add(Purchase $purchase)
    {
        try
        {
            $query = 'INSERT INTO '.$this->tableName;
            $query .= ' (userId,date, total, discount) VALUES (:userId, :date, :total, :discount) ; ';
            $parameters['date'] = $purchase->getDate();
            $parameters['total'] = $purchase->getTotal();
            $parameters['discount'] = $purchase->getDiscount();
            $parameters['userId'] = $purchase->getUserId();
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();
        } catch (Exception $e)
        {
            throw $e;
        }
    }
    public function getById($id)
    {
        try
        {
            $query = 'SELECT p.id as purchaseId, p.date as purchaseDate, p.total as purchaseTotal, p.userId as userId';
            $query .= ' FROM '.$this->tableName.' as p';
            $query .= ' WHERE p.id = :id';
            $parameters['id'] = $id;
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            foreach ($resultSet as $row) {
                $purchase = new Purchase();
                $purchase->setId($row['purchaseId']);
                $purchase->setDate($row['purchaseDate']);
                $purchase->setTotal($row['purchaseTotal']);
                $purchase->setUserId($row['userId']);
            }
            return $purchase;
        } catch (Exception $e) 
        {
        throw $e;
        }
    }
    public function getByIdUser($idUser)
    {
        try
        {
            $purchasesArray = array();
            $query = 'SELECT p.id as purchaseId, p.date as purchaseDate, p.total as purchaseTotal, p.userId as userId';
            $query .= ' FROM '.$this->tableName.' as p';
            $query .= ' WHERE userId = :idUser;';
            $parameters['idUser'] = $idUser;
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query,$parameters);
            foreach ($resultSet as $row) {
                $purchase = new Purchase();
                $purchase->setId($row['purchaseId']);
                $purchase->setUserId($row['userId']);
                $purchase->setDate($row['purchaseDate']);
                $purchase->setTotal($row['purchaseTotal']);
                array_push($purchasesArray,$purchase);
            }
            return $purchasesArray;
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    public function delete($id)
    {
        try
        {
            $query = "DELETE FROM ".$this->tableName." WHERE id = :id"; 
            $parameters["id"] = $id;
            $this->connection = connection::GetInstance();
            $this->connection->ExecuteNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
        throw $e;
        }
    }
    public function logicDelete($id)
    {
        try
        {
            $parameters['id'] = $id;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET erased=1 ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    
}
?>