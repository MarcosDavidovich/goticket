<?php
namespace dao;
use model\Venue as Venue;
interface IVenueDAO
{
    public function getAll();
    public function add(Venue $venue);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);

}
?>