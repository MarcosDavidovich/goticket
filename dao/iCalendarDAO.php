<?php
namespace dao;
use model\Calendar as Calendar;
interface ICalendarDAO
{
    public function getAll();
    public function add(Calendar $calendar);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);

}
?>