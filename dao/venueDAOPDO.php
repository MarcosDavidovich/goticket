<?php
namespace dao;
use model\venue as Venue;
use model\city as City;
class VenueDAOPDO implements iVenueDAO
{
    private $connection = null;
    private $tableName = 'venues';
    public function getAll()
    {
        try
        {
            $venuesArray = array();
            $query = 'SELECT c.id as cityId, c.name as cityName,';
            $query .=' v.id as venueId, v.description as venueDescription, v.address as venueAddress, v.name as venueName';
            $query .=' FROM '.$this->tableName.' as v ';
            $query .=' INNER JOIN cities as c ON c.id = v.idCity;';
            $this->connection = Connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $venue = new Venue();
                $city = new City();
                $venue->setId($row['venueId']);
                $venue->setName($row['venueName']);
                $venue->setAddress($row['venueAddress']);
                $city->setId($row['cityId']);
                $city->setName($row['cityName']);
                $venue->setCity($city);
                $venue->setDescription($row['venueDescription']);
                array_push($venuesArray, $venue);
            }
            return $venuesArray;
            
        }catch (Exception $e) 
        {
            throw $e;
        }

    }
    public function getAllActives()
    {
        try
        {
            $venuesArray = array();
            $query = 'SELECT c.id as cityId, c.name as cityName,';
            $query .=' v.id as venueId, v.description as venueDescription, v.address as venueAddress, v.name as venueName';
            $query .=' FROM '.$this->tableName.' as v ';
            $query .=' INNER JOIN cities as c ON c.id = v.idCity';
            $query .=' WHERE v.erased = 0;';
            $this->connection = Connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $venue = new Venue();
                $city = new City();
                $venue->setId($row['venueId']);
                $venue->setName($row['venueName']);
                $venue->setAddress($row['venueAddress']);
                $city->setId($row['cityId']);
                $city->setName($row['cityName']);
                $venue->setCity($city);
                $venue->setDescription($row['venueDescription']);
                array_push($venuesArray, $venue);
            }
            return $venuesArray;
            
        }catch (Exception $e) 
        {
            throw $e;
        }

    }
    public function add(Venue $venue)
    {
        try 
        {
            $query = 'INSERT INTO '.$this->tableName.' (name,address,idCity,description) VALUES (:name, :address, :idCity, :description);';
            $parameters['name'] = $venue->getName();
            $parameters['address'] = $venue->getAddress();
            $parameters['idCity'] = $venue->getCity()->getId();
            $parameters['description'] = $venue->getDescription();
            $this->connection = Connection::getInstance();
            $this->connection->executeNonQuery($query,$parameters);
            return $this->connection->getLastInsertId();
        } catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function getById($id)
    {
        try{
            $query = 'SELECT c.id as cityId, c.name as cityName, ';
            $query .=' v.id as venueId, v.description as venueDescription, v.address as venueAddress, v.name as venueName';
            $query .=' FROM '.$this->tableName.' as v ';
            $query .=' INNER JOIN cities as c ON c.id = v.idCity';
            $query .=' WHERE v.id = :id';
            $parameters['id'] = $id; 
            $this->connection = Connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            foreach ($resultSet as $row) {
                $venue = new Venue(); 
                $city = new City();
                $venue->setId($row['venueId']);
                $venue->setName($row['venueName']);
                $venue->setAddress($row['venueAddress']);
                $city->setId($row['cityId']);
                $city->setName($row['cityName']);
                $venue->setCity($city);
                $venue->setDescription($row['venueDescription']);
            }
            return $venue;
        }catch (Exception $e)
        {
            throw $e;
        }
    }
    public function modify($idVenue, $name, $description, $address, $idCity)
    {
        try
        {
            $parameters['id'] = $idVenue;
            $parameters['name'] = $name;
            $parameters['description'] = $description;
            $parameters['address'] = $address;
            $parameters['idCity'] = $idCity;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET name=:name, description = :description, address = :address, idCity = :idCity ";
            $query .= " WHERE id = :id;";
            $this->connection = Connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }

    public function delete($id)
    {
        try
        {
            $query = "DELETE FROM ".$this->tableName." WHERE id = :id";
            $parameters["id"] = $id;
            $this->connection = connection::GetInstance();
            $this->connection->ExecuteNonQuery($query, $parameters);   
        }
        catch(Exception $ex)
        {
            throw $ex;
        }            
    }
    public function logicDelete($id)
    {
        try
        {
            $parameters['id'] = $id;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET erased=1 ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }

}
?>