<?php
namespace dao;

use model\purchasedTicket as purchasedTicket;
class purchasedTicketDAOPDO implements iPurchasedTicketDAO
{
        private $connection = null;
        private $tableName = 'purchasedTickets';
        public function getAll()
        {
            try
            {
                $purchasedTicketsArray = array();
                $query = ' SELECT pt.id as purchasedTicketId, pt.idTicket as ticketId, pt.idPurchase as purchaseId, pt.qr as qr';
                $query .= ' FROM '.$this->tableName.' as pt; ';
                $this->connection = Connection::getInstance();
                $resultSet = $this->connection->execute($query);
                foreach ($resultSet as $row) {
                    $purchasedTicket = new PurchasedTicket();
                    $purchasedTicket->setId($row['purchasedTicketId']);
                    $purchasedTicket->setIdTicket($row['ticketId']);
                    $purchasedTicket->setIdPurchase($row['purchaseId']);
                    $purchasedTicket->setQr($row['qr']);
                    array_push($purchasedTicketsArray, $purchasedTicket);
                }
                return $purchasedTicketsArray;
            } catch (Exception $e) 
            {
                throw $e; 
            }
        }
        public function getAllActives()
        {
            try
            {
                $purchasedTicketsArray = array();
                $query = ' SELECT pt.id as purchasedTicketId, pt.idTicket as ticketId, pt.idPurchase as purchaseId, pt.qr as qr';
                $query .= ' FROM '.$this->tableName.' as pt ';
                $query .= ' WHERE pt.erased = 0; ';
                $this->connection = Connection::getInstance();
                $resultSet = $this->connection->execute($query);
                foreach ($resultSet as $row) {
                    $purchasedTicket = new PurchasedTicket();
                    $purchasedTicket->setId($row['purchasedTicketId']);
                    $purchasedTicket->setIdTicket($row['ticketId']);
                    $purchasedTicket->setIdPurchase($row['purchaseId']);
                    $purchasedTicket->setQr($row['qr']);
                    array_push($purchasedTicketsArray, $purchasedTicket);
                }
                return $purchasedTicketsArray;
            } catch (Exception $e) 
            {
                throw $e; 
            }
        }
        public function add(purchasedTicket $purchasedTicket)
        {
            try
            {
            $query = 'INSERT INTO '.$this->tableName.' (id,idTicket,IdPurchase,qr,price) ';
            $query .= ' VALUES (:id, :idTicket, :idPurchase, :qr, :price); ';
            $parameters['id'] = $purchasedTicket->getId();
            $parameters['idTicket'] = $purchasedTicket->getIdTicket();
            $parameters['idPurchase'] = $purchasedTicket->getIdPurchase();
            $parameters['qr'] = $purchasedTicket->getQr();
            $parameters['price'] = $purchasedTicket->getPrice();
            $this->connection = Connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();
            } catch (Exception $e) 
            {
                throw $e; 
            }
        }
        public function getInDates($date1, $date2)
        {
            try
            {
            $purchasedTicketsArray = array();
            $query = ' SELECT pt.id as purchasedTicketId, pt.idTicket as ticketId, pt.idPurchase as purchaseId, pt.qr as qr, pt.price as price';
            $query .= ' FROM '.$this->tableName.' as pt ';
            $query .= ' INNER JOIN purchases as p ON pt.idPurchase = p.id ';
            $query .= ' WHERE p.date >= :date1 AND p.date <= :date2 AND p.erased=0; ';
            $parameters['date1'] = $date1;
            $parameters['date2'] = $date2;
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            foreach ($resultSet as $row) {
                    $purchasedTicket = new PurchasedTicket();
                    $purchasedTicket->setId($row['purchasedTicketId']);
                    $purchasedTicket->setIdTicket($row['ticketId']);
                    $purchasedTicket->setIdPurchase($row['purchaseId']);
                    $purchasedTicket->setPrice($row['price']);
                    $purchasedTicket->setQr($row['qr']);
                    array_push($purchasedTicketsArray, $purchasedTicket);
            }
            return $purchasedTicketsArray;    
            } catch (Exception $e)
            {
                throw $e;
            }    

        }
        public function getByCategory($idCategory)
        {
            try
            {
                $purchasedTicketsArray = array();
                $query = ' SELECT pt.id as purchasedTicketId, pt.idTicket as ticketId, pt.idPurchase as purchaseId, pt.qr as qr, pt.price as price';
                $query .= ' FROM '.$this->tableName.' as pt; ';
                $query .= ' INNER JOIN tickets as t ON pt.idTicket = t.id ';
                $query .= ' INNER JOIN calendars as c ON t.idCalendar = c.id ';
                $query .= ' INNER JOIN events as e ON e.id = c.idEvent ';
                $query .= ' WHERE e.idCategory = :idCategory AND pt.erased = 0; ';
                $parameters['idCategory'] = $idCategory;
                $this->connection = Connection::getInstance();
                $resultSet = $this->connection->execute($query, $parameters);
                foreach ($resultSet as $row) {
                    $purchasedTicket = new PurchasedTicket();
                    $purchasedTicket->setId($row['purchasedTicketId']);
                    $purchasedTicket->setIdTicket($row['ticketId']);
                    $purchasedTicket->setIdPurchase($row['purchaseId']);
                    $purchasedTicket->setPrice($row['price']);
                    $purchasedTicket->setQr($row['qr']);
                    array_push($purchasedTicketsArray, $purchasedTicket);
                }
                return $purchasedTicketsArray;    
            } catch (Exception $e) 
            {
                throw $e; 
            }
        }
        public function getUsedTicketsByIdCalendarAndIdTicket($idCalendar, $idTicket)
        {
            try
            {
                $query = 'SELECT count(pt.id) as quantity ';
                $query .= ' FROM '.$this->tableName.' as pt ';
                $query .= ' INNER JOIN tickets as t ON pt.idTicket = t.id';
                $query .= ' INNER JOIN calendars as c ON t.idCalendar = c.id';
                $query .= ' WHERE c.id = :idCalendar AND pt.idTicket = :idTicket;';
                $parameters['idCalendar'] = $idCalendar;
                $parameters['idTicket'] = $idTicket;
                $this->connection = connection::getInstance();
                $resultSet = $this->connection->execute($query, $parameters);
                $ticketsQuantity = null;
                foreach ($resultSet as $row) {
                $ticketsQuantity = $row['quantity'];                   
                }
                return $ticketsQuantity;
            } catch (Exception $e)
            {
                throw $e;
            }
        }
        public function getById($id)
        {
            try
            {
                $purchasedTicketsArray = array();
                $query .= ' SELECT pt.id as purchasedTicketId, pt.idTicket as ticketId, pt.idPurchase as purchaseId, pt.qr as qr';
                $query .= ' FROM '.$this->tableName.' as pt ';
                $query .= ' WHERE pt.id = :id';
                $parameters['id'] = $id;
                $this->connection = Connection::getInstance();
                $resultSet = $this->connection->execute($query, $parameters);
                foreach ($resultSet as $row) {
                    $purchasedTicket = new PurchasedTicket();
                    $purchasedTicket->setId($row['purchasedTicketId']);
                    $purchasedTicket->setIdTicket($row['ticketId']);
                    $purchasedTicket->setIdPurchase($row['purchasedTicketId']);
                    $purchasedTicket->setQr($row['qr']);
                    array_push($purchasedTicketsArray, $purchasedTicket);
                }
            } catch (Exception $e) 
            {
                throw $e; 
            }
        }
        public function delete($id)
        {
            try
            {
                $query = "DELETE FROM ".$this->tableName." WHERE id = :id; ";
                $parameters["id"] = $id;
                $this->connection = connection::GetInstance();
                $this->connection->ExecuteNonQuery($query, $parameters);   
            } catch (Exception $e) 
            {
                throw $e; 
            }
        }
        public function logicDelete($id)
        {
            try
            {
                $parameters['id'] = $id;
                $query = " UPDATE ".$this->tableName;
                $query .= " SET erased=1 ";
                $query .= " WHERE id = :id;";
                $this->connection = connection::getInstance();
                $this->connection->executeNonQuery($query, $parameters);
            } catch (Exception $e) 
            {
                throw $e; 
            }
        }

}
?>