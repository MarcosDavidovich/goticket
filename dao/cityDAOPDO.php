<?php
namespace dao;
use model\city as City; 
use \PDO as PDO;
class cityDAOPDO implements iCityDAO
{
    private $connection = null;
    private $tableName = 'cities';
    public function getAll()
    {
        try
        {
            $citiesArray = array();
            $query = 'SELECT * FROM '.$this->tableName.';';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $city = new City();
                $city->setID($row['id']);//the value of the row must be equal to the column name
                $city->setName($row['name']);
                array_push($citiesArray,$city);
            }
            return $citiesArray;
        }catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function getAllActives()
    {
        try
        {
            $citiesArray = array();
            $query = 'SELECT * FROM '.$this->tableName.' as c';
            $query .= ' WHERE c.erased = 0;';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $city = new City();
                $city->setID($row['id']);//the value of the row must be equal to the column name
                $city->setName($row['name']);
                array_push($citiesArray,$city);
            }
            return $citiesArray;
        }catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function add(City $city)
    {
        try
        {
            $query = 'INSERT INTO '.$this->tableName.' (name) VALUES (:name);';
            $parameters['name'] = $city->getName();
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();// return the last id inserted
        }catch (Exception $e)
        {
            throw $e;
        }
    }
    public function getById($id)
    {
        try{
            $query = 'SELECT * FROM '.$this->tableName.' WHERE id = :id';
            $parameters['id'] = $id; 
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            foreach ($resultSet as $row) {
                $city = new City(); 
                $city->setId($row['id']);
                $city->setName($row['name']);
            }
            return $city;
        }catch (Exception $e)
        {
            throw $e;
        }
    }
    public function modify($idCity,$name)
    {
        try
        {
            $parameters['id'] = $idCity;
            $parameters['name'] = $name;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET name=:name ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    public function delete($id)
    {
        try
        {
            $query = "DELETE FROM ".$this->tableName." WHERE id = :id";
            
            $parameters["id"] = $id;

            $this->connection = connection::GetInstance();

            $this->connection->ExecuteNonQuery($query, $parameters);   
        }
        catch(Exception $ex)
        {
            throw $ex;
        }            
    }
    public function logicDelete($id)
    {
        try
        {
            $parameters['id'] = $id;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET erased=1 ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    
}


?>