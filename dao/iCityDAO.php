<?php
namespace dao;
use model\City as City;
interface ICityDAO
{
    public function getAll();
    public function add(City $city);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);

}
?>