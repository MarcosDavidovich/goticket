<?php
namespace dao;
use model\user as User;
class userDAOPDO implements iUserDAO
{
    private $connection = null;
    private $tableName = 'users';
    
    public function getAll()
    {
        try
        {
            $usersArray = array();
            $query = 'SELECT u.id as userId, u.email as userEmail, u.userName as userName, u.password as userPassword, u.userType as userType';
            $query .= ' FROM '.$this->tableName.' as u ;';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $user = new User();
                $user->setId($row['userId']);
                $user->setEmail($row['userEmail']);
                $user->setUserName($row['userName']);
                $user->setPassword($row['userPassword']);
                $user->setUserType($row['userType']);
                array_push($usersArray, $user);
            }
            return $usersArray;
        } catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function getAllActives()
    {
        try
        {
            $usersArray = array();
            $query = 'SELECT u.id as userId, u.email as userEmail, u.userName as userName, u.password as userPassword, u.userType as userType';
            $query .= ' FROM '.$this->tableName.' as u ';
            $query .= ' WHERE u.erased = 0;';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $user = new User();
                $user->setId($row['userId']);
                $user->setEmail($row['userEmail']);
                $user->setUserName($row['userName']);
                $user->setPassword($row['userPassword']);
                $user->setUserType($row['userType']);
                array_push($usersArray, $user);
            }
            return $usersArray;
        } catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function add(User $user)
    {
        try
        {
            $query = 'INSERT INTO '.$this->tableName.' (email,userName,password,userType) VALUES (:email,:userName,:password,:userType);';
            // $parameters['id'] = $user->getId();
            //el id lo pone la base de datos
            $parameters['email'] = $user->getEmail();
            $parameters['userName'] = $user->getUserName();
            $parameters['password'] = $user->getPassword();
            $parameters['userType'] = $user->getUserType();
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();
        } catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function getById($id)
    {
        try
        {
            $query = 'SELECT u.id as userId, u.email as userEmail, u.userName as userName, u.password as userPassword, u.userType as userType';
            $query .= ' FROM '.$this->tableName.' as u ';
            $query .= ' WHERE u.id = :id;';
            $parameters['id'] = $id;
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            foreach ($resultSet as $row) {
                $user = new User();
                $user->setId($row['userId']);
                $user->setEmail($row['userEmail']);
                $user->setUserName($row['userName']);
                $user->setPassword($row['userPassword']);
                $user->setUserType($row['userType']);
            }
            return $user;
        } catch (Exception $e) 
        {
            throw $e;
        }
    }

    public function getByUserName($userName)
    {
        try
        {
            $query = 'SELECT u.id as userId, u.email as userEmail, u.userName as userName, u.password as userPassword, u.userType as userType';
            $query .= ' FROM '.$this->tableName.' as u ';
            $query .= ' WHERE lower(u.userName) = :userName;';
            $parameters['userName'] = $userName;
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            $user=null;
            foreach ($resultSet as $row) {
                $user = new User();
                $user->setId($row['userId']);
                $user->setEmail($row['userEmail']);
                $user->setUserName($row['userName']);
                $user->setPassword($row['userPassword']);
                $user->setUserType($row['userType']);
            }
            return $user;
        } catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function getByEmail($email)
    {
        try
        {
            $query = 'SELECT u.id as userId, u.email as userEmail, u.userName as userName, u.password as userPassword, u.userType as userType';
            $query .= ' FROM '.$this->tableName.' as u ';
            $query .= ' WHERE lower(u.email) = :email;';
            $parameters['email'] = $email;
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            $user=null;
            foreach ($resultSet as $row) {
                $user = new User();
                $user->setId($row['userId']);
                $user->setEmail($row['userEmail']);
                $user->setUserName($row['userName']);
                $user->setPassword($row['userPassword']);
                $user->setUserType($row['userType']);
            }
            return $user;
        } catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function modify($id, $email, $userName, $userTypeId)
    {
        try
        {
            $parameters['id'] = $id;
            $parameters['email'] = $email;
            $parameters['userName'] = $userName;
            $parameters['userTypeId'] = $userTypeId;
            $query = ' UPDATE '.$this->tableName.' SET' ;
            $query .= ' userName = :userName, email = :email, userType = :userTypeId' ;
            $query .= ' WHERE id = :id;' ;
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
        
    }
    public function changePassword($idUser, $newPassword)
    {
        try
        {
            $parameters['id'] = $idUser;
            $parameters['newPassword'] = $newPassword;
            $query = ' UPDATE '.$this->tableName.' SET' ;
            $query .= ' password = :newPassword' ;
            $query .= ' WHERE id = :id;' ;
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }   
    }
    public function logicDelete($id)
        {
            try
            {
                $parameters['id'] = $id;
                $query = " UPDATE ".$this->tableName;
                $query .= " SET erased=1 ";
                $query .= " WHERE id = :id;";
                $this->connection = connection::getInstance();
                $this->connection->executeNonQuery($query, $parameters);
            } catch (Exception $e) 
            {
                throw $e; 
            }
        }
    public function delete($id)
    {
        try
        {
            $query = "DELETE FROM ".$this->tableName." WHERE id = :id"; 
            $parameters["id"] = $id;
            $this->connection = connection::GetInstance();
            $this->connection->ExecuteNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e;
        }
    }

}
?>