<?php
namespace dao;
use model\calendar as calendar;
class calendarDAO implements iCalendarDAO
{
    private $calendarArray = array();

    public function __construct(){
        if(!isset($_SESSION["Calendars"])){   // consulta si no esta creado en session
            $_SESSION["Calendars"] = array();   // crea array en session
        }
        $this->calendarArray = &$_SESSION["Calendars"];    // vincula el local con el de session
    }


    
    public function getAll()
    {
        return $this->calendarArray;
    }
    public function add(calendar $calendar)
    {
        $calendar->setId( rand(0,999) );
        array_push($this->calendarArray, $calendar);
    }
    public function getById($id)
    {
        $calendar = null;
        foreach ($this->calendarArray as $calendarCompare) {
            if ($calendarCompare->getId() == $id) {
                $calendar = $calendarCompare;
            }
        }
    return $calendar;
    }
    public function delete($id)
    {
        # code...
    }
}
?>