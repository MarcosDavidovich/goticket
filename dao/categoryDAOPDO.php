<?php
namespace dao;
use model\category as Category; 
class CategoryDAOPDO implements iCategoryDAO
{
    private $connection = null;
    private $tableName= "categories";
    public function getAll()
    {
        try
        {
            $categoriesArray = array();
            $query = 'SELECT * FROM '.$this->tableName.';';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $category = new Category();
                $category->setId($row['id']);
                $category->setName($row['name']);
                $category->setDescription($row['description']);
                array_push($categoriesArray, $category);
            }
            return $categoriesArray;
        } catch (Exception $e) {
            throw $e;
        }
    }
    public function getAllActives()
    {
        try
        {
            $categoriesArray = array();
            $query = 'SELECT * FROM '.$this->tableName.' as c';
            $query .= ' WHERE c.erased = 0;';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $category = new Category();
                $category->setId($row['id']);
                $category->setName($row['name']);
                $category->setDescription($row['description']);
                array_push($categoriesArray, $category);
            }
            return $categoriesArray;
        } catch (Exception $e) {
            throw $e;
        }
    }
    public function add(Category $category)
    {
        try
        {
            $query = 'INSERT INTO '.$this->tableName.' (name,description) VALUES (:name,:description);';
            $parameters['name'] = $category->getName();
            $parameters['description'] = $category->getDescription();
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();// return the last id inserted
        }catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function getById($id)
    {
        try{
            $query = 'SELECT * FROM '.$this->tableName.' WHERE id = :id';
            $parameters['id'] = $id; 
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            foreach ($resultSet as $row) {
                $category = new Category(); 
                $category->setId($row['id']);
                $category->setName($row['name']);
                $category->setDescription($row['description']);
            }
            return $category;
        }catch (Exception $e)
        {
            throw $e;
        }
    }

    public function modify($idCategory,$name)
    {
        try
        {
            $parameters['id'] = $idCategory;
            $parameters['name'] = $name;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET name=:name ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    public function delete($id)
    {
        try
        {
            $query = "DELETE FROM ".$this->tableName." WHERE id = :id";
            
            $parameters["id"] = $id;

            $this->connection = connection::GetInstance();

            $this->connection->ExecuteNonQuery($query, $parameters);   
        }
        catch(Exception $ex)
        {
            throw $ex;
        }            
    }
    public function logicDelete($id)
    {
        try
        {
            $parameters['id'] = $id;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET erased=1 ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }

} 
?>