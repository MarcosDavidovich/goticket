<?php
namespace dao;
use model\Customer as Customer;
class CustomerDAO implements ICustomerDAO
{
    private $customersArray = array();

    public function getAll()
    {
        return $this->customersArray;
    }
    public function add(Customer $customer)
    {
        array_push($this->customersArray, $customer);
    }
    public function getById($id)
    {
        $customer = null;
        foreach ($this->customersArray as $customerCompare) {
            if ($customerCompare->getId() == $id) {
                $customer = $customerCompare;
            }
        }
    return $customer;
    }
}
?>