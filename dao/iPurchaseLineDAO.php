<?php
namespace dao;
use model\purchaseLine as purchaseLine;
interface IPurchaseLineDAO
{
    public function getAll();
    public function add(purchaseLine $purchaseLine);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);

}
?>