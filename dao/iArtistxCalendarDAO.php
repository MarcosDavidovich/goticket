<?php
namespace dao;
use model\artistxCalendar as artistxCalendar;
interface iArtistxCalendarDAO
{
    public function getAll();
    public function add(ArtistxCalendar $artistxCalendar);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);
}
?>