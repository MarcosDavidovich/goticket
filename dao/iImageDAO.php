<?php
namespace dao;
use model\image as image;
interface iImageDAO 
{
    public function getAll();
    public function add(image $image);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);
}
?>