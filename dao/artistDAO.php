<?php
namespace dao;
use model\artist as artist;

class ArtistDAO implements iArtistDAO
{
    private $artistArray = array();

    public function __construct(){
        if(!isset($_SESSION["Artists"])){   // consulta si no esta creado en session
            $_SESSION["Artists"] = array();   // crea array en session
        }
        $this->artistArray = &$_SESSION["Artists"];    // vincula el local con el de session
    }

    public function getAll()
    {
        return $this->artistArray;
    }
    public function add(artist $artist)
    {   $artist->setId(rand(0,999));
        array_push($this->artistArray, $artist);
    }
    public function getById($id)
    {
        $artist = null;
        foreach ($this->artistArray as $artistCompare) {
            if ($artistCompare->getId() == $id) {
                $artist = $artistCompare;
            }
        }
    return $artist;
    }
    public function delete($id)
    {
        # code...
    }

}
?>