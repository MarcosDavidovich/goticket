<?php
namespace dao;
use model\User as User; 
interface IUserDAO
{
    public function getAll();
    public function add(User $user);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);
}
?>