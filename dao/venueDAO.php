<?php
namespace dao;
use model\venue as venue;

class venueDAO implements iVenueDAO
{
    private $venueArray = array();

    public function __construct(){
        if(!isset($_SESSION["Venues"])){   // consulta si no esta creado en session
            $_SESSION["Venues"] = array();   // crea array en session
        }
        $this->venueArray = &$_SESSION["Venues"];    // vincula el local con el de session
    }

    public function getAll()
    {
        return $this->venueArray;
    }
    public function add(venue $venue)
    {
        $venue->setId( rand(0,999));
        array_push($this->venueArray, $venue);
    }
    public function getById($id)
    {
        $venue = null;
        foreach ($this->venueArray as $venueCompare) {
            if ($venueCompare->getId() == $id) {
                $venue = $venueCompare;
            }
        }
    return $venue;
    }
    public function delete($id)
    {
        # code...
    }
}
?>