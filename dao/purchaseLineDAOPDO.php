<?php
namespace dao;
use model\purchaseLine as purchaseLine;
class purchaseLineDAOPDO implements iPurchaseLineDAO
{
    private $connection = null;
    private $tableName = 'purchaseLines';
    
    public function getAll()
    {
        try
        {
        $purchaseLinesArray = array();
        $query = 'SELECT pl.id as purchaseLineId, pl.quantity as purchaseLineQuantity, pl.price as purchaseLinePrice, ';
        $query .= ' pl.idPurchase as purchaseId, pl.idTicket as ticketId';
        $query .= ' FROM '.$this->tableName.' as pl; ';
        $this->connection = Connection::getInstance();
        $resultSet = $this->connection->execute($query);
        foreach ($resultSet as $row) {
            $purchaseLine = new PurchaseLine();
            $purchaseLine->setId($row['purchaseLineId']);
            $purchaseLine->setQuantity($$row['purchaseLineQuantity']);
            $purchaseLine->setPrice($row['purchaseLinePrice']);
            $purchaseLine->setIdTicket($row['ticketId']);
            $purchaseLine->setIdPurchase($row['purchaseId']);
            array_push($purchaseLinesArray, $purchaseLine);
        }
        return $purchaseLinesArray;
        } catch (Exception $e)  
        {
            throw $e;
        }
    }
    public function getAllActives()
    {
        try
        {
        $purchaseLinesArray = array();
        $query = 'SELECT pl.id as purchaseLineId, pl.quantity as purchaseLineQuantity, pl.price as purchaseLinePrice, ';
        $query .= ' pl.idPurchase as purchaseId, pl.idTicket as ticketId';
        $query .= ' FROM '.$this->tableName.' as pl ';
        $query .= ' WHERE pl.erased = 0; ';
        $this->connection = Connection::getInstance();
        $resultSet = $this->connection->execute($query);
        foreach ($resultSet as $row) {
            $purchaseLine = new PurchaseLine();
            $purchaseLine->setId($row['purchaseLineId']);
            $purchaseLine->setQuantity($$row['purchaseLineQuantity']);
            $purchaseLine->setPrice($row['purchaseLinePrice']);
            $purchaseLine->setIdTicket($row['ticketId']);
            $purchaseLine->setIdPurchase($row['purchaseId']);
            array_push($purchaseLinesArray, $purchaseLine);
        }
        return $purchaseLinesArray;
        } catch (Exception $e)  
        {
            throw $e;
        }
    }

    public function add(purchaseLine $purchaseLine)
    {
        try
        {
            $query = 'INSERT INTO '.$this->tableName.' (idTicket,idPurchase,price,quantity) VALUES (:idTicket, :idPurchase, :price, :quantity);';
            $parameters['idTicket'] = $purchaseLine->getIdTicket();
            $parameters['idPurchase'] = $purchaseLine->getIdPurchase();
            $parameters['price'] = $purchaseLine->getPrice();
            $parameters['quantity'] = $purchaseLine->getQuantity();
            $this->connection = Connection::getInstance();
            $this->connection->executeNonQuery($query,$parameters);
            return $this->connection->getLastInsertId();
        } catch (Exception $e) 
        {
            throw $e;
        }
    
    }
    public function getById($id)
    {
        try
        {
            $query = 'SELECT pl.id as purchaseLineId, pl.quantity as purchaseLineQuantity, pl.price as purchaseLinePrice, ';
            $query .= ' pl.idPurchase as purchaseId, pl.idTicket as ticketId';
            $query .= ' FROM '.$this->tableName.' as pl ';
            $query .= ' WHERE pl.id = :id; ';
            $parameters['id'] = $id;
            $this->connection = Connection::getInstance();
            $resultSet = $this->connection->execute($query,$parameters);
            foreach ($resultSet as $row) {
                $purchaseLine = new PurchaseLine();
                $purchaseLine->setId($row['purchaseLineId']);
                $purchaseLine->setQuantity($$row['purchaseLineQuantity']);
                $purchaseLine->setPrice($row['purchaseLinePrice']);
                $purchaseLine->setIdTicket($row['ticketId']);
                $purchaseLine->setIdPurchase($row['purchaseId']);
            }
            return $purchaseLine;
        } catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function delete($id)
    {
        try
        {
            $query = "DELETE FROM ".$this->tableName." WHERE id = :id"; 
            $parameters["id"] = $id;
            $this->connection = connection::GetInstance();
            $this->connection->ExecuteNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
        throw $e;
        }
    }
    public function logicDelete($id)
    {
        try
        {
            $parameters['id'] = $id;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET erased=1 ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }

    public function getLinesByIdPurchase($idPurchase)
    {
        try
        {
            $purchaseLinesArray = array();
            $query = 'SELECT pl.id as purchaseLineId, pl.quantity as purchaseLineQuantity, pl.price as purchaseLinePrice, ';
            $query .= ' pl.idPurchase as purchaseId, pl.idTicket as ticketId';
            $query .= ' FROM '.$this->tableName.' as pl ';
            $query .= ' WHERE pl.idPurchase = :idPurchase ; ';
            $parameters['idPurchase'] = $idPurchase;
            $this->connection = Connection::getInstance();
            $resultSet = $this->connection->execute($query,$parameters);
            foreach ($resultSet as $row) {
                $purchaseLine = new PurchaseLine();
                $purchaseLine->setId($row['purchaseLineId']);
                $purchaseLine->setQuantity($row['purchaseLineQuantity']);
                $purchaseLine->setPrice($row['purchaseLinePrice']);
                $purchaseLine->setIdTicket($row['ticketId']);
                $purchaseLine->setIdPurchase($row['purchaseId']);
                array_push($purchaseLinesArray, $purchaseLine);
            }
            return $purchaseLinesArray;
        } catch (Exception $e) 
        {
            throw $e;
        }
    }

}
?>