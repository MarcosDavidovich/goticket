<?php
namespace dao;
use model\Customer as Customer;
interface ICustomerDAO
{
    public function getAll();
    public function add(Customer $customer);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);

}
?>