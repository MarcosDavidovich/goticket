<?php
namespace dao;
use model\ticketType as ticketType;

class ticketTypeDAO implements iTicketTypeDAO
{
    private $ticketTypeArray = array();

    public function __construct(){
        if(!isset($_SESSION["TicketTypes"])){   // consulta si no esta creado en session
            $_SESSION["TicketTypes"] = array();   // crea array en session
        }
        $this->ticketTypeArray = &$_SESSION["TicketTypes"];    // vincula el local con el de session
    }

    public function getAll()
    {
        return $this->ticketTypeArray;
    }
    public function add(ticketType $ticketType)
    {
        $ticketType->setId(rand(0,999));
        array_push($this->ticketTypeArray, $ticketType);
    }
    public function getById($id)
    {
        $ticketType = null;
        foreach ($this->ticketTypeArray as $ticketTypeCompare) {
            if ($ticketTypeCompare->getId() == $id) {
                $ticketType = $ticketTypeCompare;
            }
        }
    return $ticketType;
    }
}
?>