<?php
namespace dao;
use model\ticket as Ticket;
use model\ticketType as TicketType;
use model\calendar as Calendar;
use model\event as Event;
use model\venue as Venue;
use model\category as Category;
use model\city as City;
use model\artist as Artist;
class ticketDAOPDO implements iTicketDAO
{
    private $connection = null;
    private $tableName = 'tickets';
    
    public function getAll()
    {
        try
        {
            $ticketsArray = array();
            $query = 'SELECT t.id as ticketId, t.price as ticketPrice, t.available as ticketAvailable, t.remain as ticketRemain, ';
            $query .= ' tt.id as ticketTypeId, tt.name as ticketTypeName, tt.description as ticketTypeDescription, ';
            $query .= ' c.id as calendarId, c.date as calendarDate,';
            $query .= ' v.id as venueId, v.name as venueName, v.address as venueAddress, v.description as venueDescription,';
            $query .= ' ci.id as cityId, ci.name as cityName,';
            $query .= ' e.id as eventId, e.name as eventName, e.description as eventDescription, ';
            $query .= ' ifnull(ic.name, "ticketgocover.jpg") as coverImg, ifnull(isq.name, "ticketgosq.jpg") as sqImg,';
            $query .= ' ca.id as categoryId, ca.name as categoryName, ca.description as categoryDescription ';
            $query .= ' FROM '.$this->tableName.' as t ';
            $query .= ' INNER JOIN ticketTypes as tt ON t.idTicketType = tt.id ';
            $query .= ' INNER JOIN calendars as c ON t.idCalendar = c.id';
            $query .= ' INNER JOIN venues as v ON c.idVenue = v.id ';
            $query .= ' INNER JOIN events as e ON c.idEvent = e.id ';
            $query .= ' INNER JOIN cities as ci ON v.idCity = ci.id ';
            $query .= ' INNER JOIN categories as ca ON e.idCategory = ca.id ';
            $query .= ' LEFT JOIN images as ic ON e.idCoverImg = ic.id';
            $query .= ' LEFT JOIN images as isq ON e.idSqImg = isq.id ORDER BY e.id;';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $ticket = new Ticket();
                $ticketType = new TicketType();
                $calendar = new Calendar();
                $event = new Event();
                $venue = new Venue();
                $city = new City();
                $category = new Category();
                $ticketType->setId($row['ticketTypeId']);
                $ticketType->setName($row['ticketTypeName']);
                $ticketType->setDescription($row['ticketTypeDescription']);
                $city->setId($row['cityId']);
                $city->setName($row['cityName']);
                $category->setId($row['categoryId']);
                $category->setName($row['categoryName']);
                $category->setDescription($row['categoryDescription']);
                $venue->setId($row['venueId']);
                $venue->setName($row['venueName']);
                $venue->setDescription($row['venueDescription']);
                $venue->setAddress($row['venueAddress']);
                $venue->setCity($city);
                $event->setId($row['eventId']);
                $event->setName($row['eventName']);
                $event->setDescription($row['eventDescription']);
                $event->setCategory($category);
                $event->setCoverImg($row['coverImg']);
                $event->setSqImg($row['sqImg']);
                $calendar->setId($row['calendarId']);
                $calendar->setDate($row['calendarDate']);
                $calendar->setEvent($event);
                $calendar->setVenue($venue);
                $ticket->setId($row['ticketId']);
                $ticket->setTicketType($ticketType);
                $ticket->setCalendar($calendar);
                $ticket->setPrice($row['ticketPrice']);
                $ticket->setAvailable($row['ticketAvailable']);
                $ticket->setRemain($row['ticketRemain']);
                array_push($ticketsArray, $ticket);
            }
            return $ticketsArray;
        } catch (Exception $e)
        {
            throw $e;
        }
    }
    public function getAllActives()
    {
        try
        {
            $ticketsArray = array();
            $query = 'SELECT t.id as ticketId, t.price as ticketPrice, t.available as ticketAvailable, t.remain as ticketRemain, ';
            $query .= ' tt.id as ticketTypeId, tt.name as ticketTypeName, tt.description as ticketTypeDescription, ';
            $query .= ' c.id as calendarId, c.date as calendarDate,';
            $query .= ' v.id as venueId, v.name as venueName, v.address as venueAddress, v.description as venueDescription,';
            $query .= ' ci.id as cityId, ci.name as cityName,';
            $query .= ' e.id as eventId, e.name as eventName, e.description as eventDescription, ';
            $query .= ' ifnull(ic.name, "ticketgocover.jpg") as coverImg, ifnull(isq.name, "ticketgosq.jpg") as sqImg,';
            $query .= ' ca.id as categoryId, ca.name as categoryName, ca.description as categoryDescription ';
            $query .= ' FROM '.$this->tableName.' as t ';
            $query .= ' INNER JOIN ticketTypes as tt ON t.idTicketType = tt.id ';
            $query .= ' INNER JOIN calendars as c ON t.idCalendar = c.id';
            $query .= ' INNER JOIN venues as v ON c.idVenue = v.id ';
            $query .= ' INNER JOIN events as e ON c.idEvent = e.id ';
            $query .= ' INNER JOIN cities as ci ON v.idCity = ci.id ';
            $query .= ' INNER JOIN categories as ca ON e.idCategory = ca.id ';
            $query .= ' LEFT JOIN images as ic ON e.idCoverImg = ic.id';
            $query .= ' LEFT JOIN images as isq ON e.idSqImg = isq.id';
            $query .= ' WHERE t.erased = 0;';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $ticket = new Ticket();
                $ticketType = new TicketType();
                $calendar = new Calendar();
                $event = new Event();
                $venue = new Venue();
                $city = new City();
                $category = new Category();
                $ticketType->setId($row['ticketTypeId']);
                $ticketType->setName($row['ticketTypeName']);
                $ticketType->setDescription($row['ticketTypeDescription']);
                $city->setId($row['cityId']);
                $city->setName($row['cityName']);
                $category->setId($row['categoryId']);
                $category->setName($row['categoryName']);
                $category->setDescription($row['categoryDescription']);
                $venue->setId($row['venueId']);
                $venue->setName($row['venueName']);
                $venue->setDescription($row['venueDescription']);
                $venue->setAddress($row['venueAddress']);
                $venue->setCity($city);
                $event->setId($row['eventId']);
                $event->setName($row['eventName']);
                $event->setDescription($row['eventDescription']);
                $event->setCategory($category);
                $event->setCoverImg($row['coverImg']);
                $event->setSqImg($row['sqImg']);
                $calendar->setId($row['calendarId']);
                $calendar->setDate($row['calendarDate']);
                $calendar->setEvent($event);
                $calendar->setVenue($venue);
                $ticket->setId($row['ticketId']);
                $ticket->setTicketType($ticketType);
                $ticket->setCalendar($calendar);
                $ticket->setPrice($row['ticketPrice']);
                $ticket->setAvailable($row['ticketAvailable']);
                $ticket->setRemain($row['ticketRemain']);
                array_push($ticketsArray, $ticket);
            }
            return $ticketsArray;
        } catch (Exception $e)
        {
            throw $e;
        }
    }

    public function add(Ticket $ticket)
    {
        try
        {
            $query = 'INSERT INTO '.$this->tableName.' (id,idTicketType,idCalendar,price,available,remain) ';
            $query .= ' VALUES (:id, :idTicketType, :idCalendar, :price, :available, :remain); ';
            $parameters['id'] = $ticket->getId();
            $parameters['idTicketType'] = $ticket->getTicketType()->getId(); 
            $parameters['idCalendar'] = $ticket->getCalendar()->getId(); 
            $parameters['price'] = $ticket->getPrice(); 
            $parameters['available'] = $ticket->getAvailable(); 
            $parameters['remain'] = $ticket->getRemain(); 
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();
        } catch (Exception $e)
        {
            throw $e;
        }
    }

    public function getById($id)
    {
        try
        {
            $ticketsArray = array();
            $query = 'SELECT t.id as ticketId, t.price as ticketPrice, t.available as ticketAvailable, t.remain as ticketRemain, ';
            $query .= ' tt.id as ticketTypeId, tt.name as ticketTypeName, tt.description as ticketTypeDescription, ';
            $query .= ' c.id as calendarId, c.date as calendarDate,';
            $query .= ' v.id as venueId, v.name as venueName, v.address as venueAddress, v.description as venueDescription,';
            $query .= ' ci.id as cityId, ci.name as cityName,';
            $query .= ' e.id as eventId, e.name as eventName, e.description as eventDescription,';
            $query .= ' ifnull(ic.name, "ticketgocover.jpg") as coverImg, ifnull(isq.name, "ticketgosq.jpg") as sqImg,';
            $query .= ' ca.id as categoryId, ca.name as categoryName, ca.description as categoryDescription ';
            $query .= ' FROM '.$this->tableName.' as t ';
            $query .= ' INNER JOIN ticketTypes as tt ON t.idTicketType = tt.id ';
            $query .= ' INNER JOIN calendars as c ON t.idCalendar = c.id';
            $query .= ' INNER JOIN venues as v ON c.idVenue = v.id ';
            $query .= ' INNER JOIN events as e ON c.idEvent = e.id ';
            $query .= ' INNER JOIN cities as ci ON v.idCity = ci.id ';
            $query .= ' INNER JOIN categories as ca ON e.idCategory = ca.id ';
            $query .= ' LEFT JOIN images as ic ON e.idCoverImg = ic.id ';
            $query .= ' LEFT JOIN images as isq ON e.idSqImg = isq.id ';
            $query .= ' WHERE t.id = :id AND t.erased = 0; ';
            $parameters['id'] = $id;
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            $ticket = new Ticket();
            foreach ($resultSet as $row) {
                    
                $ticketType = new TicketType();
                $calendar = new Calendar();
                $event = new Event();
                $venue = new Venue();
                $city = new City();
                $category = new Category();
                $ticketType->setId($row['ticketTypeId']);
                $ticketType->setName($row['ticketTypeName']);
                $ticketType->setDescription($row['ticketTypeDescription']);
                $city->setId($row['cityId']);
                $city->setName($row['cityName']);
                $category->setId($row['categoryId']);
                $category->setName($row['categoryName']);
                $category->setDescription($row['categoryDescription']);
                $venue->setId($row['venueId']);
                $venue->setName($row['venueName']);
                $venue->setDescription($row['venueDescription']);
                $venue->setAddress($row['venueAddress']);
                $venue->setCity($city);
                $event->setId($row['eventId']);
                $event->setName($row['eventName']);
                $event->setDescription($row['eventDescription']);
                $event->setCategory($category);
                $event->setCoverImg($row['coverImg']);
                $event->setSqImg($row['sqImg']);
                $calendar->setId($row['calendarId']);
                $calendar->setDate($row['calendarDate']);
                $calendar->setEvent($event);
                $calendar->setVenue($venue);
                $ticket->setId($row['ticketId']);
                $ticket->setTicketType($ticketType);
                $ticket->setCalendar($calendar);
                $ticket->setPrice($row['ticketPrice']);
                $ticket->setAvailable($row['ticketAvailable']);
                $ticket->setRemain($row['ticketRemain']);
            }
            return $ticket;
        } catch (Exception $e)
        {
            throw $e;
        }
    }
    public function getByCalendarId($idCalendar)
    {
        try
        {
            $ticketsArray = array();
            $query = 'SELECT t.id as ticketId, t.price as ticketPrice, t.available as ticketAvailable, t.remain as ticketRemain, ';
            $query .= ' tt.id as ticketTypeId, tt.name as ticketTypeName, tt.description as ticketTypeDescription, ';
            $query .= ' c.id as calendarId, c.date as calendarDate,';
            $query .= ' v.id as venueId, v.name as venueName, v.address as venueAddress, v.description as venueDescription,';
            $query .= ' ci.id as cityId, ci.name as cityName,';
            $query .= ' e.id as eventId, e.name as eventName, e.description as eventDescription,';
            $query .= ' ca.id as categoryId, ca.name as categoryName, ca.description as categoryDescription ';
            $query .= ' FROM '.$this->tableName.' as t ';
            $query .= ' INNER JOIN ticketTypes as tt ON t.idTicketType = tt.id ';
            $query .= ' INNER JOIN calendars as c ON t.idCalendar = c.id';
            $query .= ' INNER JOIN venues as v ON c.idVenue = v.id ';
            $query .= ' INNER JOIN events as e ON c.idEvent = e.id ';
            $query .= ' INNER JOIN cities as ci ON v.idCity = ci.id ';
            $query .= ' INNER JOIN categories as ca ON e.idCategory = ca.id ';
            $query .= ' WHERE t.idCalendar = :idCalendar AND t.erased = 0; ';
            $parameters['idCalendar'] = $idCalendar;
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            $ticket = new Ticket();
            foreach ($resultSet as $row) {
                    
                $ticketType = new TicketType();
                $calendar = new Calendar();
                $event = new Event();
                $venue = new Venue();
                $city = new City();
                $category = new Category();
                $ticketType->setId($row['ticketTypeId']);
                $ticketType->setName($row['ticketTypeName']);
                $ticketType->setDescription($row['ticketTypeDescription']);
                $city->setId($row['cityId']);
                $city->setName($row['cityName']);
                $category->setId($row['categoryId']);
                $category->setName($row['categoryName']);
                $category->setDescription($row['categoryDescription']);
                $venue->setId($row['venueId']);
                $venue->setName($row['venueName']);
                $venue->setDescription($row['venueDescription']);
                $venue->setAddress($row['venueAddress']);
                $venue->setCity($city);
                $event->setId($row['eventId']);
                $event->setName($row['eventName']);
                $event->setDescription($row['eventDescription']);
                $event->setCategory($category);
                $calendar->setId($row['calendarId']);
                $calendar->setDate($row['calendarDate']);
                $calendar->setEvent($event);
                $calendar->setVenue($venue);
                $ticket->setId($row['ticketId']);
                $ticket->setTicketType($ticketType);
                $ticket->setCalendar($calendar);
                $ticket->setPrice($row['ticketPrice']);
                $ticket->setAvailable($row['ticketAvailable']);
                $ticket->setRemain($row['ticketRemain']);
            }
            return $ticket;
        } catch (Exception $e)
        {
            throw $e;
        }
    }
    public function getByEventId($idEvent)
    {
        try
        {
            $ticketsArray = array();
            $query = 'SELECT t.id as ticketId, t.price as ticketPrice, t.available as ticketAvailable, t.remain as ticketRemain, ';
            $query .= ' tt.id as ticketTypeId, tt.name as ticketTypeName, tt.description as ticketTypeDescription, ';
            $query .= ' c.id as calendarId, c.date as calendarDate,';
            $query .= ' v.id as venueId, v.name as venueName, v.address as venueAddress, v.description as venueDescription,';
            $query .= ' ci.id as cityId, ci.name as cityName,';
            $query .= ' e.id as eventId, e.name as eventName, e.description as eventDescription,';
            $query .= ' ca.id as categoryId, ca.name as categoryName, ca.description as categoryDescription ';
            $query .= ' FROM '.$this->tableName.' as t ';
            $query .= ' INNER JOIN ticketTypes as tt ON t.idTicketType = tt.id ';
            $query .= ' INNER JOIN calendars as c ON t.idCalendar = c.id';
            $query .= ' INNER JOIN venues as v ON c.idVenue = v.id ';
            $query .= ' INNER JOIN events as e ON c.idEvent = e.id ';
            $query .= ' INNER JOIN cities as ci ON v.idCity = ci.id ';
            $query .= ' INNER JOIN categories as ca ON e.idCategory = ca.id ';
            $query .= ' WHERE c.idEvent = :idEvent AND c.erased = 0; ';
            $parameters['idEvent'] = $idEvent;
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            $ticket = new Ticket();
            foreach ($resultSet as $row) {
                    
                $ticketType = new TicketType();
                $calendar = new Calendar();
                $event = new Event();
                $venue = new Venue();
                $city = new City();
                $category = new Category();
                $ticketType->setId($row['ticketTypeId']);
                $ticketType->setName($row['ticketTypeName']);
                $ticketType->setDescription($row['ticketTypeDescription']);
                $city->setId($row['cityId']);
                $city->setName($row['cityName']);
                $category->setId($row['categoryId']);
                $category->setName($row['categoryName']);
                $category->setDescription($row['categoryDescription']);
                $venue->setId($row['venueId']);
                $venue->setName($row['venueName']);
                $venue->setDescription($row['venueDescription']);
                $venue->setAddress($row['venueAddress']);
                $venue->setCity($city);
                $event->setId($row['eventId']);
                $event->setName($row['eventName']);
                $event->setDescription($row['eventDescription']);
                $event->setCategory($category);
                $calendar->setId($row['calendarId']);
                $calendar->setDate($row['calendarDate']);
                $calendar->setEvent($event);
                $calendar->setVenue($venue);
                $ticket->setId($row['ticketId']);
                $ticket->setTicketType($ticketType);
                $ticket->setCalendar($calendar);
                $ticket->setPrice($row['ticketPrice']);
                $ticket->setAvailable($row['ticketAvailable']);
                $ticket->setRemain($row['ticketRemain']);
                array_push($ticketsArray, $ticket);
            }
            return $ticketsArray;
        } catch (Exception $e)
        {
            throw $e;
        }
    }
    public function modify($idTicket, $idTicketType, $price, $available)
    {
        try
        {
            $parameters['id'] = $idTicket;
            $parameters['idTicketType'] = $idTicketType;
            $parameters['price'] = $price;
            $parameters['available'] = $available;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET idTicketType=:idTicketType, price=:price, available=:available";
            $query .= " WHERE id = :id;";
            $this->connection = Connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    public function delete($id)
    {
        try
        {
            $query = "DELETE FROM ".$this->tableName." WHERE id = :id; ";
            
            $parameters["id"] = $id;

            $this->connection = connection::GetInstance();

            $this->connection->ExecuteNonQuery($query, $parameters);   
        }
        catch(Exception $ex)
        {
            throw $ex;
        } 
    }
    public function logicDelete($id)
    {
        try
        {
            $parameters['id'] = $id;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET erased=1 ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }

}
?>