<?php
namespace dao;
use model\Purchase as Purchase;
class PurchaseDAO implements IPurchaseDAO
{
    private $purchasesArray = array();

    public function getAll()
    {
        return $this->purchasesArray;
    }
    public function add(Purchase $purchase)
    {
        array_push($this->purchasesArray,$purchase);
    }
    public function getById($id)
    {
        $purchase = null;
        foreach ($this->purchasesArray as $purchaseCompare) {
            if ($purchaseCompare->getId() == $id) {
                $purchase = $purchaseCompare;
            }
        }
    return $purchase;
    }
    public function delete($id)
    {
        # code...
    }
}
?>