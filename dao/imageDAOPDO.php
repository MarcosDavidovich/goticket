<?php
namespace dao;
use model\image as image;

class imageDAOPDO implements iImageDAO
{
    private $connection = null;
    private $tableName = 'images';
    
    public function getAll()
    {
        try
        {
            $imagesArray = array();
            $query = ' SELECT i.id as imageId, i.name as imageName FROM '.$this->tableName.' as i; ';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $image = Image();
                $image->setId($row['imageId']);
                $image->setName($row['imageName']);
                array_push($imagesArray, $image);
            }
            return $imagesArray;
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    public function getAllActives()
    {
        try
        {
            $imagesArray = array();
            $query = ' SELECT i.id as imageId, i.name as imageName FROM '.$this->tableName.' as i ';
            $query = ' WHERE i.erased = 0; ';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $image = Image();
                $image->setId($row['imageId']);
                $image->setName($row['imageName']);
                array_push($imagesArray, $image);
            }
            return $imagesArray;
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }

    public function add(image $image)
    {
        try
        {
            $query = 'INSERT INTO '.$this->tableName.' (name) VALUES(:imageName) ;';
            $parameters['imageName'] = $image->getName();
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }

    public function getById($id)
    {
        try
        {
            $query = ' SELECT i.id as imageId, i.name as imageName FROM '.$this->tableName.' as i ';
            $query .= ' WHERE imageId = :id; ';
            $parameters['id'] = $id;
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            foreach ($resultSet as $row) {
                $image = Image();
                $image->setId($row['imageId']);
                $image->setName($row['imageName']);
            }
            return $image;
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    public function getByName($name)
    {
        try
        {
            $query = ' SELECT i.id as imageId, i.name as imageName FROM '.$this->tableName.' as i ';
            $query .= ' WHERE name = :name; ';
            $parameters['name'] = $name;
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            $image = new Image();
            foreach ($resultSet as $row) {
                $image->setId($row['imageId']);
                $image->setName($row['imageName']);
            }
            return $image;
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }

    public function delete($id)
    {
        try
        {
            $query = "DELETE FROM ".$this->tableName." WHERE id = :id; ";
            $parameters["id"] = $id;
            $this->connection = connection::GetInstance();
            $this->connection->ExecuteNonQuery($query, $parameters);   
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    public function logicDelete($id)
    {
        try
        {
            $parameters['id'] = $id;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET erased=1 ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }

}
?>