<?php
namespace dao;
use model\Purchase as Purchase;
interface IPurchaseDAO
{
    public function getAll();
    public function add(Purchase $purchase);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);

}
?>