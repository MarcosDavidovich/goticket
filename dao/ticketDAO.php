<?php
namespace dao;
use model\ticket as ticket;

class ticketDAO implements iTicketDAO
{
    private $ticketsArray  = array();
    
    public function __construct(){
        if(!isset($_SESSION["Tickets"])){   // consulta si no esta creado en session
            $_SESSION["Tickets"] = array();   // crea array en session
        }
        $this->ticketsArray = &$_SESSION["Tickets"];    // vincula el local con el de session
    }

    public function getAll()
    {
        return $this->ticketsArray;
    }
    public function add(Ticket $ticket)
    {
        $ticket->setId( rand(0,999));
        array_push($this->ticketsArray, $ticket);
    }
    public function getById($id)
    {
        $ticket = null;
        foreach ($this->ticketsArray as $ticketCompare) {
            if ($ticketCompare->getId() == $id) {
                $ticket = $ticketCompare;
            }
        }
    return $ticket;
    }
    public function delete($id)
    {
        # code...
    }
}
?>