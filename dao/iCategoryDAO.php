<?php
namespace dao;
use model\Category as Category;
interface ICategoryDAO
{
    public function getAll();
    public function add(Category $category);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);

}
?>