<?php
namespace dao;
use model\ticketType as TicketType; 
use \PDO as PDO;
class TicketTypeDAOPDO implements iTicketTypeDAO
{
    private $connection = null;
    private $tableName = 'ticketTypes';
    public function getAll()
    {
        try
        {
            $ticketTypeArray = array();
            $query = 'SELECT * FROM '.$this->tableName.';';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $ticketType = new TicketType();
                $ticketType->setID($row['id']);//the value of the row must be equal to the column name
                $ticketType->setName($row['name']);
                array_push($ticketTypeArray, $ticketType);
            }
            return $ticketTypeArray;
        }catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function getAllActives()
    {
        try
        {
            $ticketTypeArray = array();
            $query = 'SELECT * FROM '.$this->tableName.' as tt ';
            $query .= ' WHERE tt.erased = 0; ';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
            foreach ($resultSet as $row) {
                $ticketType = new TicketType();
                $ticketType->setID($row['id']);//the value of the row must be equal to the column name
                $ticketType->setName($row['name']);
                array_push($ticketTypeArray, $ticketType);
            }
            return $ticketTypeArray;
        }catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function add(TicketType $ticketType)
    {
        try
        {
            $query = 'INSERT INTO '.$this->tableName.' (name) VALUES (:name);';
            $parameters['name'] = $ticketType->getName();
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();// return the last id inserted
        }catch (Exception $e)
        {
            throw $e;
        }
    }
    public function getById($id)
    {
        try{
            $query = 'SELECT * FROM '.$this->tableName.' WHERE id = :id';
            $parameters['id'] = $id; 
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            foreach ($resultSet as $row) {
                $ticketType = new TicketType(); 
                $ticketType->setId($row['id']);
                $ticketType->setName($row['name']);
            }
            return $ticketType;
        }catch (Exception $e)
        {
            throw $e;
        }
    }
    public function modify($idTicketType,$name)
    {
        try
        {
            $parameters['id'] = $idTicketType;
            $parameters['name'] = $name;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET name=:name ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    public function delete($id)
    {
        try
        {
            $query = "DELETE FROM ".$this->tableName." WHERE id = :id";
            
            $parameters["id"] = $id;

            $this->connection = connection::GetInstance();

            $this->connection->ExecuteNonQuery($query, $parameters);   
        }
        catch(Exception $ex)
        {
            throw $ex;
        }            
    }
    public function logicDelete($id)
    {
        try
        {
            $parameters['id'] = $id;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET erased=1 ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    
}


?>