<?php
namespace dao;
use model\category as category;
class categoryDAO implements iCategoryDAO
{
    private $categoriesArray = array();

    public function __construct(){
        if(!isset($_SESSION["Categories"])){   // consulta si no esta creado en session
            $_SESSION["Categories"] = array();   // crea array en session
        }
        $this->categoriesArray = &$_SESSION["Categories"];    // vincula el local con el de session
    }



    public function getAll()
    {
        return $this->categoriesArray;
    }
    public function add(Category $category)
    {
        $category->setId( rand(0,999));
        array_push($this->categoriesArray, $category);
    }
    public function getById($id)
    {
        $category = null;
        foreach ($this->categoriesArray as $categoryCompare) {
            if ($categoryCompare->getId() == $id) {
                $category = $categoryCompare;
            }
        }
    return $category;
    }
}
?>