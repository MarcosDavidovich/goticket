<?php
namespace dao;
use model\PurchasedTicket as PurchasedTicket;
interface IPurchasedTicketDAO
{
    public function getAll();
    public function add(PurchasedTicket $purchasedTicket);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);

}
?>