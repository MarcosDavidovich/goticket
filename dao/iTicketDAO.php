<?php
namespace dao;
use model\Ticket as Ticket;
interface ITicketDAO
{
    public function getAll();
    public function add(Ticket $ticket);
    public function getById($id);
    public function delete($id); 
    public function logicDelete($id);

    
}
?>