<?php
namespace dao;
use model\event as event;

class eventDAO implements iEventDAO
{
    private $eventsArray = array();
    
    public function __construct()
    {
       if(!isset($_SESSION["Events"])){   // consulta si no esta creado en session
                $_SESSION["Events"] = array();   // crea array en session
            }
            $this->eventsArray = &$_SESSION["Events"];    // vincula el local con el de session
    }
    
    public function getAll()
    {
        return $this->eventsArray;
    }
    public function add(Event $event)
    {
        $event->setId(rand(0,999));
        array_push($this->eventsArray, $event);
        return $event->getId();   // agregado retorna ID
    }
    public function getById($id)
    {
        $event = null;
        foreach ($this->eventsArray as $eventCompare) {
            if ($eventCompare->getId() == $id) {
                $event = $eventCompare;
            }
        }
    return $event;
    }
    public function delete($id)
    {
        
    }
}
?>