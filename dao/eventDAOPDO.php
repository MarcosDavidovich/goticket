<?php
namespace dao;
use model\event as Event;
use model\category as Category;
class eventDAOPDO implements iEventDAO
{
        private $connection = null;
        private $tableName = 'events';
        private $imagesTable = 'images';

        public function getAll()
        {
            try{
            $eventsArray = array();
            $query = 'SELECT e.id as eventId, c.id as categoryId, c.name as categoryName, c.description as categoryDescription,';
            $query .=' e.name as eventName, e.description as eventDescription,';
            $query .= ' ifnull(ic.name, "ticketgocover.jpg") as coverImg, ifnull(isq.name, "ticketgosq.jpg") as sqImg';
            $query .= ' FROM '.$this->tableName.' as e ';
            $query .= ' INNER JOIN categories as c ON e.idCategory = c.id';
            $query .= ' LEFT JOIN images as ic ON e.idCoverImg = ic.id';
            $query .= ' LEFT JOIN images as isq ON e.idSqImg = isq.id ORDER BY e.id;';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
                foreach ($resultSet as $row) {
                    $event = new Event();
                    $category = new Category();
                    $event->setId($row['eventId']);
                    $event->setName($row['eventName']);
                    $event->setDescription($row['eventDescription']);
                    $event->setCoverImg($row['coverImg']);
                    $event->setSqImg($row['sqImg']);
                    
                    $category->setId($row['categoryId']);
                    $category->setName($row['categoryName']);
                    $category->setDescription($row['categoryDescription']);
                    
                    $event->setCategory($category);
                    
                    array_push($eventsArray, $event);
                }
                return $eventsArray;
            } catch (Exception $e) 
            {
                throw $e;
            }
        }
        public function getAllActives()
        {
            try{
            $eventsArray = array();
            $query = 'SELECT e.id as eventId, c.id as categoryId, c.name as categoryName, c.description as categoryDescription,';
            $query .=' e.name as eventName, e.description as eventDescription,';
            $query .= ' ifnull(ic.name, "ticketgocover.jpg") as coverImg, ifnull(isq.name, "ticketgosq.jpg") as sqImg';
            $query .= ' FROM '.$this->tableName.' as e ';
            $query .= ' INNER JOIN categories as c ON e.idCategory = c.id';
            $query .= ' LEFT JOIN images as ic ON e.idCoverImg = ic.id';
            $query .= ' LEFT JOIN images as isq ON e.idSqImg = isq.id ';
            $query .= ' WHERE e.erased = 0;';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
                foreach ($resultSet as $row) {
                    $event = new Event();
                    $category = new Category();
                    $event->setId($row['eventId']);
                    $event->setName($row['eventName']);
                    $event->setDescription($row['eventDescription']);
                    $event->setCoverImg($row['coverImg']);
                    $event->setSqImg($row['sqImg']);
                    
                    $category->setId($row['categoryId']);
                    $category->setName($row['categoryName']);
                    $category->setDescription($row['categoryDescription']);
                    
                    $event->setCategory($category);
                    
                    array_push($eventsArray, $event);
                }
                return $eventsArray;
            } catch (Exception $e) 
            {
                throw $e;
            }
        }

        public function getByName($name)
        {
            try{
            $eventsArray = array();
            $query = 'SELECT e.id as eventId, c.id as categoryId, c.name as categoryName, c.description as categoryDescription,';
            $query .=' e.name as eventName, e.description as eventDescription,';
            $query .= ' ifnull(ic.name, "ticketgocover.jpg") as coverImg, ifnull(isq.name, "ticketgosq.jpg") as sqImg';
            $query .= ' FROM '.$this->tableName.' as e ';
            $query .= ' INNER JOIN categories as c ON e.idCategory = c.id';
            $query .= ' LEFT JOIN images as ic ON e.idCoverImg = ic.id';
            $query .= ' LEFT JOIN images as isq ON e.idSqImg = isq.id ';
            $query .= ' WHERE lower(e.name) like "%'.$name.'%" AND e.erased = 0;';
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query);
                foreach ($resultSet as $row) {
                    $event = new Event();
                    $category = new Category();
                    $event->setId($row['eventId']);
                    $event->setName($row['eventName']);
                    $event->setDescription($row['eventDescription']);
                    $event->setCoverImg($row['coverImg']);
                    $event->setSqImg($row['sqImg']);
                    
                    $category->setId($row['categoryId']);
                    $category->setName($row['categoryName']);
                    $category->setDescription($row['categoryDescription']);
                    
                    $event->setCategory($category);
                    
                    array_push($eventsArray, $event);
                }
                return $eventsArray;
            } catch (Exception $e) 
            {
                throw $e;
            }
        }
        public function add(Event $event)
        {
            try
            {
            $query = 'INSERT INTO '.$this->tableName.' (name,description,idCategory, idCoverImg, idSqImg ) VALUES (:name,:description,:idCategory, :coverImg, :sqImg);';
            $parameters['name'] = $event->getName();
            $parameters['description'] = $event->getDescription();
            $parameters['idCategory'] = $event->getCategory()->getId();
            $parameters['coverImg'] = $event->getCoverImg();
            $parameters['sqImg'] = $event->getSqImg();
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();
            } catch (Exception $e)
            {
                throw $e;  
            }
        }

        public function getById($id)
        {
            try
            {
            $query =  'SELECT c.id as categoryId, c.name as categoryName, c.description as categoryDescription, ';
            $query .= ' e.id as eventId, e.name as eventName, e.description as eventDescription, ';
            $query .= ' ifnull(ic.name, "ticketgocover.jpg") as coverImg, ifnull(isq.name, "ticketgosq.jpg") as sqImg';
            $query .= ' FROM '.$this->tableName.' as e ';
            $query .= ' INNER JOIN categories as c ON e.idCategory = c.id';
            $query .= ' LEFT JOIN images as ic ON e.idCoverImg = ic.id';
            $query .= ' LEFT JOIN images as isq ON e.idSqImg = isq.id';
            $query .= ' WHERE e.id = :id and e.erased = 0; '; 
            $parameters['id'] = $id;
            $this->connection = connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            $event = new Event();
            $category = new Category();
            foreach ($resultSet as $row) {
                $event->setId($row['eventId']);
                $event->setName($row['eventName']);
                $event->setDescription($row['eventDescription']);
                $event->setCoverImg($row['coverImg']);
                $event->setSqImg($row['sqImg']);

                $category->setId($row['categoryId']);
                $category->setName($row['categoryName']);
                $category->setDescription($row['categoryDescription']);
                
                $event->setCategory($category);
            }
            return $event;
            } catch (Exception $e)
            {
                throw $e;
            }
        }
        
        public function modify($idEvent, $name, $description, $idCategory, $coverId, $sqId)
        {
            try
            {
            $parameters['name'] = $name;
            $parameters['description'] = $description;
            $parameters['idCategory'] = $idCategory;
            $parameters['coverImg'] = $coverId;
            $parameters['sqImg'] = $sqId;
            $parameters['id'] = $idEvent;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET name=:name, description = :description, idCategory = :idCategory, idCoverImg =:coverImg, idSqImg = :sqImg ";
            $query .= " WHERE id = :id;";
            
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();
            } catch (Exception $e)
            {
                throw $e;  
            }
        }

        public function deleteCoverImage($idEvent)
        {
            try
            {
            $parameters['id'] = $idEvent;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET idCoverImg = NULL ";
            $query .= " WHERE id = :id;";
        
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();
            } catch (Exception $e)
            {
                throw $e;  
            }
        }
        public function deleteSqImage($idEvent)
        {
            try
            {
            $parameters['id'] = $idEvent;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET idSqImg = NULL ";
            $query .= " WHERE id = :id;";
        
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();
            } catch (Exception $e)
            {
                throw $e;  
            }
        }




        public function delete($id)
        {
            try
            {
                $query = "DELETE FROM ".$this->tableName." WHERE id = :id";
                
                $parameters["id"] = $id;
    
                $this->connection = connection::GetInstance();
    
                $this->connection->ExecuteNonQuery($query, $parameters);   
            }
            catch(Exception $ex)
            {
                throw $ex;
            }
        }
        public function logicDelete($id)
        {
            try
            {
                $parameters['id'] = $id;
                $query = " UPDATE ".$this->tableName;
                $query .= " SET erased=1 ";
                $query .= " WHERE id = :id;";
                $this->connection = connection::getInstance();
                $this->connection->executeNonQuery($query, $parameters);
            } catch (Exception $e) 
            {
                throw $e; 
            }
        }
        
}
?>