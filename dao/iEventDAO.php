<?php
namespace dao;
use model\Event as Event;
interface IEventDAO
{
    public function getAll();
    public function add(Event $event);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);

}
?>