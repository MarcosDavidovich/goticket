<?php
namespace dao;
use model\TicketType as TicketType;
interface ITicketTypeDAO
{
    public function getAll();
    public function add(TicketType $ticketType);
    public function getById($id);
    public function delete($id);
    public function logicDelete($id);

}
?>