<?php
namespace dao;
use model\artistxCalendar as artistxCalendar;
class artistxCalendarDAOPDO implements iArtistxCalendarDAO
{
    private $connection = null;
    private $tableName = 'artistxCalendar';
    public function getAll()
    {
        try
        {
        $artistxCalendarArray = array();
        $query = 'SELECT axc.idArtist as artistId, axc.idCalendar as calendarId';
        $query .= ' FROM '.$this->tableName.' as axc;';
        $this->connection = Connection::getInstance();
        $resultSet = $this->connection->execute($query);
        foreach ($resultSet as $row) {
            $artistxCalendar = new ArtistxCalendar();
            $artistxCalendar->setIdArtist($row['artistId']);
            $artistxCalendar->setIdCalendar($row['calendarId']);
            array_push($artistxCalendarArray, $artistxCalendar);
        }
        return $artistxCalendarArray;
        } catch (Exception $e)
        {
            throw $e;
        }
    }
    public function getAllActives()
    {
        try
        {
        $artistxCalendarArray = array();
        $query = 'SELECT axc.idArtist as artistId, axc.idCalendar as calendarId';
        $query .= ' FROM '.$this->tableName.' as axc';
        $query .= ' WHERE axc.erased=0;';
        $this->connection = Connection::getInstance();
        $resultSet = $this->connection->execute($query);
        foreach ($resultSet as $row) {
            $artistxCalendar = new ArtistxCalendar();
            $artistxCalendar->setIdArtist($row['artistId']);
            $artistxCalendar->setIdCalendar($row['calendarId']);
            array_push($artistxCalendarArray, $artistxCalendar);
        }
        return $artistxCalendarArray;
        } catch (Exception $e)
        {
            throw $e;
        }
    }

    public function add(ArtistxCalendar $artistxCalendar)
    {
        try
        {
            $query = 'INSERT INTO '.$this->tableName.' (idCalendar,idArtist) VALUES (:idCalendar,:idArtist);';
            $parameters['idCalendar'] = $artistxCalendar->getIdCalendar();
            $parameters['idArtist'] = $artistxCalendar->getIdArtist();
            $this->connection = Connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
            return $this->connection->getLastInsertId();
        } catch (Exception $e) 
        {
            throw $e;
        }
    }

    public function getById($id)
    {
        try
        {
            $query = 'SELECT axc.idArtist as artistId, axc.idCalendar as calendarId';
            $query .= ' FROM '.$this->tableName.' as axc ';
            $query .= ' WHERE axc.id = :id;';
            $parameters['id'] = $id;
            $this->connection = Connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            foreach ($resultSet as $row) {
                $artistxCalendar = new ArtistxCalendar();
                $artistxCalendar->setIdArtist($row['artistId']);
                $artistxCalendar->setIdCalendar($row['calendarId']);
            }
            return $artistxCalendar;
        } catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function getByCalendarId($id)
    {
        try
        {
            $query = 'SELECT axc.idArtist as artistId, axc.idCalendar as calendarId';
            $query .= ' FROM '.$this->tableName.' as axc ';
            $query .= ' WHERE axc.idCalendar = :id;';
            $parameters['id'] = $id;
            $this->connection = Connection::getInstance();
            $resultSet = $this->connection->execute($query, $parameters);
            foreach ($resultSet as $row) {
                $artistxCalendar = new ArtistxCalendar();
                $artistxCalendar->setIdArtist($row['artistId']);
                $artistxCalendar->setIdCalendar($row['calendarId']);
            }
            return $artistxCalendar;
        } catch (Exception $e) 
        {
            throw $e;
        }
    }

    public function delete($id)
    {
        try
        {
            $query = "DELETE FROM ".$this->tableName." WHERE id = :id; "; 
            $parameters["id"] = $id;
            $this->connection = connection::GetInstance();
            $this->connection->ExecuteNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e;
        }
    }
    public function logicDelete($id)
    {
        try
        {
            $parameters['id'] = $id;
            $query = " UPDATE ".$this->tableName;
            $query .= " SET erased=1 ";
            $query .= " WHERE id = :id;";
            $this->connection = connection::getInstance();
            $this->connection->executeNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e; 
        }
    }
    
    public function deleteByIdCalendar($idCalendar)
    {
        try
        {
            $query = "DELETE FROM ".$this->tableName." WHERE idCalendar = :idCalendar; "; 
            $parameters["idCalendar"] = $idCalendar;
            $this->connection = connection::GetInstance();
            $this->connection->ExecuteNonQuery($query, $parameters);
        } catch (Exception $e) 
        {
            throw $e;
        }
    }

}
?>