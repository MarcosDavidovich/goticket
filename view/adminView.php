<?php



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
  <link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">
  
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >
    <div class="row align-items-center bg-info p-3 mb-4 px-4">
      <div class="col text-white  ">
        <h4><i class="fas fa-info-circle"></i> Usuarios </h4>
      </div>      
      <div class="col-3">
        <!-- <a href="<?php //echo FRONT_ROOT?>calendar/calendarView" class="btn btn-warning" role="button" aria-disabled="true"><i class="fas fa-undo-alt"></i> VOLVER A CALENDARIOS</a> -->
      </div>
    </div>     

    <div class="container mt-3">
      <?php if (""!=$message) { 
                      ?><div class="alert alert-success" role="alert">
                      <?php echo $message; ?>
                    </div><?php
                      }?>

      <hr>

      <h4> <i class="far fa-list-alt"></i>  Listado de Usuarios</h4>

      <div class="row justify-content-center mt-5 mb-5">
        <table class="table table-sm table-hover col-sm-8">
          <thead class="thead-light">
            <tr class="">
              <th scope="col" >#</th>
              <th scope="col" >Nombre</th>
              <th scope="col" >Email</th>
              <th scope="col" >Tipo </th>
              <th scope="col" ></th>
            </tr>
          </thead>
          <tbody>
              <?php
              if(empty($userList)){
                  echo "<tr><td colspan='3'>LISTA VACIA</td></tr>";
              } else {
                  foreach ($userList as $user) {
                    $userType = ($user->getUserType() == 0) ? 'Admin' : 'User' ;
                    ?>
                      <tr>
                        <th scope="row"><?php echo $user->getId()?></th>
                        <td> <?php echo $user->getUserName() ?> </td>
                        <td> <?php echo $user->getEmail() ?> </td>
                        <td> <?php echo $userType ?> </td>
                        <td class="row">
                          <div class="p-0 col-5">
                            <form action="<?php echo FRONT_ROOT ?>user/editUser" method="POST">
                              <input type="hidden" name="idUser" value="<?php echo $user->getId()?>">
                              <input type="hidden" name="email" value="<?php echo $user->getEmail()?>">
                              <input type="hidden" name="userName" value="<?php echo $user->getUserName()?>">
                              <input type="hidden" name="userType" value="<?php echo $user->getUserType()?>">
                              <button type="submit" class="btn btn-warning col-9"><i class="fas fa-pencil-alt"></i></button>
                            </form>
                          </div>
                          <div class="p-0  col-5">
                            <form action="<?php echo FRONT_ROOT ?>user/deleteUser" method="POST">
                              <input type="hidden" name="id" value="<?php echo $user->getId()?>">
                              <button type="submit" class="btn btn-danger col-9" ><i class="far fa-trash-alt"></i></button>
                            </form>
                          </div>
                        </td>
                      </tr>
              <?php }} ?>
              </tbody>
        </table>
      </div>
    </div>

      <!-- Cierre del main container -->
  </div> 