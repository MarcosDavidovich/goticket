<?php



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo CSS_PATH ?>ticketgo.css">
  <link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<style>
.card-img{
  height:150px;
}




</style>




</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >

    <div id="eventcont" class="pb-5 bg-dark">
      <img class ="img-fluid" src="<?php echo IMG_PATH."events/"?>ticketgocover.jpg" alt="">
      <div class="negativemgt container rounded bg-light pt-3">
        <div class="row"></div>
          <h2>Resultados de busqueda para "<?php  echo $keyword  ?>"</h2>
          <div class="row">

            <!-- MAIN CONTENT -->
            <div class="col-9"> 
                    <!-- START CARDS  -->

                    <div class="container cards justify-content-center mt-5">
                      <div class="row">
                        <?php 
                        if (empty($eventList)){?>
                          <div class="col-12 mt-3 mb-3 text-center border border-danger rounded">
                            <h3 class="p-3">  <i class="far fa-dizzy"></i>  No hay Resultados para "<?php echo $keyword ?>"</h3>
                          </div>                       
                        
                        <?php }else {
                          foreach($eventList as $event) { ?>
                          <div class="col-4 mb-3">
                          <a href=" <?php  echo FRONT_ROOT  ?>event/eventView/<?php echo $event->getId()?>" class="none">  
                            <div class="card">
                            <div class="card-img" style="
                            background: url(<?php echo IMG_PATH."events/".$event->getSqImg();?>) no-repeat center center;
                            background-size: cover; ">
                            </div>
                            <div class="card-body">
                                  <h5 class="card-title"> <?php  echo $event->getName()  ?></h5>
                                  <a href=" <?php  echo FRONT_ROOT  ?>event/eventView/<?php echo $event->getId()?> " class="btn btn-outline-success btn-block"><i class="fas fa-cart-plus"></i> COMPRAR TICKETS</a>
                                </div>
                            </div>
                            </a>
                          </div>
                          <?php }}?>
                      </div>
                    </div>

                  <!-- END CARDS  -->  




            </div>
            <!-- END MAIN CONTENT -->
            <!-- SIDEBAR! -->
            <div class="col">
              <h5>SIDEBAR</h5>
            </div>
            <!-- END SIDEBAR -->
          </div>
      
      </div>
    </div>

    <!-- Cierre del main container -->
  </div> 




