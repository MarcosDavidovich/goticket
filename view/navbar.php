<nav id="nav" class="navbar navbar-expand-lg navbar-dark  sticky-top">
  
  <a class="navbar-brand" href="<?php echo FRONT_ROOT?>home"> 
  <img style="max-width:200px;" src="<?php echo IMG_PATH?>logo.png"/>  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">

      <li class="nav-item">
        <a class="nav-link " href="<?php echo FRONT_ROOT ?>" >
          <i class="fas fa-home"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="<?php echo FRONT_ROOT ?>artist/listArtistView" >
          ARTISTAS
        </a>
      </li>
          <li class="nav-item mr-2 ">
            <a class="nav-link" href="<?php echo FRONT_ROOT ?>event/listEventView" > EVENTOS </a>
          </li>
            <!-- BUSCADOR -->
       <li class="pr-3 mr-4 border-right border-secondary">
        <form action="<?php echo FRONT_ROOT ?>home/search" method="POST">
          <div class="input-group">
            <input class="form-control" type="text" name="keyword"  autocomplete="off" placeholder="Buscar Evento...">
            <div class="input-group-append">
              <button class="input-group-text bg-dark" id="basic-text1"><i class="fa fa-search text-white" aria-hidden="true"></i></button>
            </div>
          </div>
        </form>
      </li>
      <!-- Fin BUSCADOR -->
 
      
    <?php  
      if( isset($_SESSION["userType"]) ) { ?>
        <?php
        if($_SESSION["userType"]=="admin"){ ?>
            <!-- Menu ADMIN -->
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              ADMINISTRAR
            </a>
            <div class="dropdown-menu dropdown-menu-center mr-2" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item  border-bottom" href="<?php echo FRONT_ROOT?>home/dashboardView"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
              <a class="dropdown-item" href="<?php echo FRONT_ROOT?>event/createEventView">Eventos</a>
              <a class="dropdown-item" href="<?php echo FRONT_ROOT?>artist/artistView">Artistas</a>
              <a class="dropdown-item" href="<?php echo FRONT_ROOT?>category/categoryView">Categorias</a>
              <a class="dropdown-item" href="<?php echo FRONT_ROOT?>city/cityView">Ciudades</a>
              <a class="dropdown-item" href="<?php echo FRONT_ROOT?>venue/venueView">Lugares</a>
              <a class="dropdown-item" href="<?php echo FRONT_ROOT?>ticketType/ticketTypeView">Tipos de Ticket</a>
              <a class="dropdown-item" href="<?php echo FRONT_ROOT?>user/adminView">Usuarios</a>

            </div>
          </li>
          <?php  if (isset($event) && $event!=null) { ?>
            <form action="<?php echo FRONT_ROOT ?>calendar/editCalendar" method="post" >
              <input type="hidden" name="eventId" value="<?php echo $event->getId() ?>">
              <button type="submit" class="btn btn-warning mr-1"><i class="fas fa-pencil-alt"></i> EDITAR EVENTO</button>
            </form>
          <!-- FIN MENU ADMIN -->
          <?php } }?>
          <a href="<?php echo FRONT_ROOT?>user/userView" class="btn btn-outline-light mr-2"><i class="fas fa-user"></i></a>
        <?php 
      }// cierra esle isset 
        ?>

        <?php if (isset($_SESSION["purchase"]) && !empty($_SESSION["purchase"]->getLines())     ){ ?>    
            <a href="<?php echo FRONT_ROOT?>purchase/purchaseView" class="btn btn-outline-light mx-2">
                <i class="fas fa-shopping-cart"></i>  
                <!-- TODO contar cuantos tickets hay en la compra -->
                <span class="badge badge-pill badge-light">
                  <?php  
                    echo $_SESSION["purchase"]->getTicketAmount();
                  ?>
                </span> 
            </a>
          <?php } ?>

      <!-- LOGIN / LOGOUT -->
      <?php  
      if( isset($_SESSION["userType"]) ) { ?>         
        <a href="<?php echo FRONT_ROOT?>home/logout" class="mx-2 btn btn-outline-light">LOGOUT</a>
      <?php } else { // cierra if user issset ?>
        <a href="<?php echo FRONT_ROOT?>user/loginView" class="mx-2 btn btn-outline-light">LOGIN</a>
      <?php  } ?>
      <!-- FIN LOGIN / LOGOUT -->
    </ul>
  </div>
</nav>