<?php



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
<div class="container-fluid p-0 m-0" id="main-content" >
        <div class="row align-items-center bg-info p-3 mb-4 px-4">
        <div class="col text-white  ">
            <h4><i class="fas fa-info-circle"></i> GoTicket > Artistas </h4>
        </div>      
        <div class="col-3">
            <a href="<?php echo FRONT_ROOT?>artist/artistView" class="btn btn-warning" role="button" aria-disabled="true"><i class="fas fa-undo-alt"></i> VOLVER A ARTISTAS</a>
        </div>
        </div>     

    <div class="container mt-3">
      <h3><i class="fas fa-edit"></i>Modificacion de artista</h3>
      <form action="<?php echo FRONT_ROOT ?>artist/editArtist" method="POST">
        <div class="row justify-content-center mt-5 mb-5">
              <div class="form-row col-sm-9">
              <input type="hidden" name="idArtist" value="<?php echo $artist->getId()?>">
                  <div class="col">
                      <input type="text" name="name" class="form-control" value="<?php echo $artist->getName(); ?>"id="" placeholder="Nombre del Artista" maxlength="30" required>
                  </div>
                  <div class="col">
                      <input type="text" name="description" class="form-control" id="" value="<?php echo $artist->getDescription(); ?>" maxlength="50" placeholder="Descripcion">
                  </div>
                  <div class="col">
                      <button type="submit" class="btn btn-outline-success btn-block"><i class="fas fa-pen-alt"></i>  EDITAR</button>
                  </div>
              </div>
        </div>
      </form>


      

      

      <!-- Cierre del main container -->
  </div> 