<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT ?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


<style>


</style>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
  <div class="container-fluid p-0 m-0" id="main-content" >
    <div id="eventcont" class="pb-5">
      <img class ="img-fluid" src="<?php echo IMG_PATH."events/ticketgocover.jpg"?>" alt="">
      <div class="negativemgt container rounded  p-4">
        <div class="row">
        </div>
        <h1 class="event-tit"><i class="fas fa-chevron-circle-right"></i>  EVENTOS</h1>
        <div class="row">
          <div class="col-9"> 
              <form action=" <?php  echo FRONT_ROOT  ?>event/listEventView" method="POST">
                <input type="hidden" name="$filtertype" value="1">
                <div class="form-row my-4">
                  <div class="col-9">
                    <?php if(empty($categoryList)) { ?>
                    <h5>No hay categorias para mostrar!</h5>
                    <?php }else {?>
                      <select class="catselect form-control" name="filterparams[]" multiple="multiple">
                      <?php foreach ($categoryList as $category) { ?>
                        <option <?php
                          if( !empty($filterCategories) && in_array($category->getId(), $filterCategories) )
                          {
                            echo "selected";
                          }
                        ?> value="<?php echo $category->getId() ?>"><?php echo $category->getName() ?></option>
                      <?php } ?>
                      </select>
                    <?php }?>
                  </div>
                  <div class="col-3">
                    <button type="submit" class="btn btn-outline-success btn-block">
                      <?php if ($filtertype==1) { ?>      
                        <i class="fas fa-sync-alt"></i> ACTUALIZAR
                      <?php } else { ?>
                        <i class="fas fa-filter"></i>  FILTRAR
                      <?php } ?>
                    </button>
                  </div>
                </div>
              </form>
              <form action=" <?php  echo FRONT_ROOT  ?>event/listEventView" method="POST">
                <input type="hidden" name="$filtertype" value="2">
                <div class="form-row my-4">
                  <div class="col-3">
                    <input type="date" class="form-control" name="filterparams[]" 
                    <?php
                      if ($filtertype==2){ ?>
                        value="<?php echo $filterParams[0];?>"
                      <?php
                      }
                    ?>
                                        id="">
                  </div>
                  <div class="col-3">
                    <input type="date" class="form-control" name="filterparams[]" 
                    <?php
                      if ($filtertype==2){ ?> 
                        value="<?php echo $filterParams[1]; ?>"
                      <?php
                      }
                    ?>
                    id="">
                  </div>
                  <div class="col-3">
                    <button type="submit" class="btn btn-outline-success btn-block">
                      <?php if ($filtertype=="2") { 
                        ?>                            
                        <i class="fas fa-sync-alt"></i> ACTUALIZAR
                      <?php } else { ?>
                        <i class="fas fa-filter"></i>  FILTRAR
                      <?php } ?>
                    </button>
                  </div>
                </div>
              </form>



            <div class="row"> 
              <?php 
                $display=false;
                foreach($eventList as $event) { 
                  if(!empty($filterCategories)) {
                    if(in_array($event->getCategory()->getId(), $filterCategories) ) {
                      $display=true;
                      ?>
                      <div class="col-4 mb-3">
                        <a href=" <?php  echo FRONT_ROOT  ?>event/eventView/<?php echo $event->getId()?>" class="none"> 
                          <div class="card card-dark">
                            <div class="card-img" style="
                            background: url(<?php echo IMG_PATH."events/".$event->getSqImg();?>) no-repeat center center;
                            background-size: cover; ">
                            </div>
                              <div class="card-body">
                                <h5 class="card-title"> <?php  echo $event->getName()  ?></h5>
                                <!-- <p class="card-text"><?php  //echo $event->getDescription()  ?></p> -->
                                <a href=" <?php  echo FRONT_ROOT  ?>event/eventView/<?php echo $event->getId()?> " class="btn btn-outline-success btn-block mt-4"><i class="fas fa-cart-plus"></i> COMPRAR TICKETS</a>
                              </div>
                          </div>
                        </a>
                      </div>
                    <?php } }else{ 
                      $display=true;
                      ?>
                      <div class="col-4 mb-3">
                        <a href=" <?php  echo FRONT_ROOT  ?>event/eventView/<?php echo $event->getId()?>" class="none"> 
                          <div class="card card-dark">
                            <div class="card-img" style="
                            background: url(<?php echo IMG_PATH."events/".$event->getSqImg();?>) no-repeat center center;
                            background-size: cover; ">
                            </div>
                              <div class="card-body">
                                <h5 class="card-title"> <?php  echo $event->getName()  ?></h5>
                                <!-- <p class="card-text"><?php  //echo $event->getDescription()  ?></p> -->
                                <a href=" <?php  echo FRONT_ROOT  ?>event/eventView/<?php echo $event->getId()?> " class="btn btn-outline-success btn-block mt-4"><i class="fas fa-cart-plus"></i> COMPRAR TICKETS</a>
                              </div>
                          </div>
                        </a>
                      </div>
                <?php }};
                
                if (!$display){
                ?>
                    <div class="col m-4 border border-danger rounded text-center">
                      <h3 class="p-3">  <i class="far fa-dizzy"></i>  No hay eventos para mostrar!</h3>
                    </div>                
                <?php } ?>
              
              </div>        
          
          
          
          </div>
          <!-- CLOSE MAIN CONT -->
          <!-- Start sidebar -->
          <div class="col"> 
            <h3>SIDEBAR</h3>
          </div>
          <!-- END sidebar -->

        </div>      
      </div>
    </div>

  </div>
  <script>
  $(document).ready(function() {
    $('.catselect').select2({
      placeholder: 'Filtrar por categorias',
      width: '100%'
    });
  });
  </script>