<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo CSS_PATH ?>ticketgo.css">
	<link rel="stylesheet" href="<?php echo CSS_PATH ?>index.css">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >
          <div class="row align-items-center bg-info p-3 mb-4 ">
            <div class="col text-white  ">
              <h3><i class="fas fa-info-circle"></i> Evento > <?php echo $event->getName()?></h3>
            </div>      
            <div class="col-3">
              <a href="<?php echo FRONT_ROOT?>calendar/calendarView" class="btn btn-warning" role="button" aria-disabled="true"><i class="fas fa-undo-alt"></i> VOLVER A CALENDARIOS</a>
            </div>
          </div>

        <div class="container mt-3">

          <h2><i class="far fa-edit"></i> Editar Evento <?php echo $event->getName() ?></h2>


          <form id="event" action="<?php echo FRONT_ROOT?>event/modifyEvent" enctype="multipart/form-data" method="POST">
          <div class="row justify-content-center mt-5 mb-5 ">
              <div class="form-row col-sm-10 ">
                <div class="col-3">
                  <input form="event" type="hidden" name="idEvent" value="<?php echo $eventId ?>">
                  <input form="event" type="text" name="name"  class="form-control" id="" value="<?php echo $event->getName()?>" required> 
                </div>
                <div class="col">
                  <input form="event" type="text" name="description" class="form-control" id="" 
                  <?php if($event->getDescription()==""){
                    echo 'placeholder="Ingrese descripcion del evento..." ';
                  } else{
                    echo "value='".$event->getDescription()."'";
                  } ?>
                  >
                </div>
                <?php if(empty($categoryList)) { ?>
                  <div class="col-2">
                    <a href="<?php echo FRONT_ROOT?>category/categoryView" class="btn btn-warning" role="button" aria-disabled="true">CREAR CATEGORIAS</a> 
                  </div>
                  <?php }else {?>
                  <div class="col-2">
                    <select form="event"  name="category" class="form-control" >
                      <?php foreach ($categoryList as $category) { ?>
                      <option value="<?php echo $category->getId() ?>" 
                        <?php
                          if ($category->getId() == $event->getCategory()->getId()){
                              echo "selected";
                           } ?>
                      ><?php echo $category->getName(); ?>
                      </option>
                      <?php }?>
                    </select>
                  </div>
                <?php }?>
              </div>
              <div class="form-row col-sm-10 mt-4">
              
                  <div class="col form-group border rounded p-3 mr-2">
                      <div>
                        <img class ="img-fluid" src="<?php echo IMG_PATH."events/".$event->getCoverImg();?>" alt="">
                      </div>
                      <label for="cover_img" class="bg-info text-white p-2"><i class="far fa-image"></i> Modificar Imagen de portada</label>
                      <input form="event" type="file" class="form-control-file" name="coverImg" id="">
                      <small id="helpblock" class="form-text text-muted">
                        Archivo png o jpg de 1300px x 400px.
                      </small>
                      <?php if($event->getCoverImg()!= "ticketgocover.jpg") {?>
                       <a href="<?php echo FRONT_ROOT?>event/deleteEventImg/<?php echo $eventId ?>/1" class="btn btn-outline-danger btn-block mt-2"><i class="fas fa-times"></i> ELIMINAR</a>
                      <?php  }  ?>
                  </div>
                  <div class="col form-group border rounded p-3">
                      <div>
                        <!-- <img class ="" src="http://placehold.it/300x200" alt=""> -->
                        <img class ="img-fluid" src="<?php echo IMG_PATH."events/".$event->getSqImg();?>" alt="">
                      </div>
                      <label for="sq_img" class="bg-info text-white p-2"><i class="far fa-image"></i> Modificar Imagen de perfil</label>
                      <input form="event" type="file" class="form-control-file" name="sqImg" id="">
                      <small id="helpblock" class="form-text text-muted">
                      Archivo png o jpg de 500px x 500px.                      
                      </small>
                      <?php 
                      if($event->getSqImg()!= "ticketgosq.jpg") {?>
                        <a href="<?php echo FRONT_ROOT?>event/deleteEventImg/<?php echo $eventId ?>/2" class="btn btn-outline-danger btn-block mt-2"><i class="fas fa-times"></i> ELIMINAR</a>
                      <?php  }  ?>
                  </div>                           

              </div>
              <div class="form-row col-sm-10 mt-3">
                  <div class="col-8">
                  </div>                           
                  <div class="col-4">
                    <button type="submit" form="event"  class="btn btn-outline-success btn-block"><i class="fas fa-pen-alt"></i> EDITAR</button>
                  </div>                           
              </div>
        </div>
        </form>

        <hr>
   
      <!-- Cierre del main container -->
  </div> 
                
                  
