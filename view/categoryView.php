<?php



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >
    
    <div class="row align-items-center bg-info p-3 mb-4 px-4">
      <div class="col text-white  ">
        <h4><i class="fas fa-info-circle"></i> GoTicket > Categorias </h4>
      </div>      
      <div class="col-3">
        <!-- <a href="<?php //echo FRONT_ROOT?>calendar/calendarView" class="btn btn-warning" role="button" aria-disabled="true"><i class="fas fa-undo-alt"></i> VOLVER A CALENDARIOS</a> -->
      </div>
    </div>   

    <div class="container mt-3">
    <?php if (isset($message)) {
                     if (""!=$message) { 
                      ?><div class="alert alert-success" role="alert">
                      <?php echo $message; ?>
                    </div><?php
                      }}?>
      <?php if (null != $_SESSION['editCategory']) {?>
      <h3><i class="fas fa-edit"></i> Modificar Categoria #<?php echo $category->getId()?></h3>
      <form action="<?php echo FRONT_ROOT?>category/modifyCategory" method="POST">
        <input type="hidden" name="idCategory" value="<?php echo $category->getId()?>">
        <div class="row justify-content-center mt-5 mb-5">
              <div class="form-row col-sm-9">
                  <div class="col">
                    <input type="text" name="name" maxlength="30" class="form-control" id="" value="<?php echo $category->getName();?>" required>
                  </div> 
                  <div class="col-4">
                    <button type="submit" class="btn btn-outline-success"><i class="fas fa-pen-alt"></i>  EDITAR</button>
                  </div>
              </div>
        </div>
      </form>

      <?php
      }else {?>
      <h3><i class="fas fa-tags"></i> Alta de Categoria</h3>
      
      <form action="<?php echo FRONT_ROOT?>category/createCategory" method="POST"> 
        <div class="row justify-content-center mt-5 mb-5">
              <div class="form-row col-sm-9">
                  <div class="col">
                    <input type="text" name="name" class="form-control" id="" maxlength="30" placeholder="Categoria" required>
                  </div>
                  <div class="col-4">
                      <button type="submit" class="btn btn-outline-success btn-block"><i class="fas fa-plus"></i>  AGREGAR</button>
                  </div>
              </div>
        </div>
      </form>

      <hr>
      <?php }?>
  
      <h4> <i class="far fa-list-alt"></i>  Listado de Categorias </h4>
      <div class="row justify-content-center mt-5 mb-5">
  
        <table class="table table-sm table-hover col-sm-5">
          <thead class="thead-light">
            <tr class="">
              <th scope="col" >#</th>
              <th scope="col" >Categoría</th>
              <th scope="col" ></th>
              <th scope="col" ></th>
            </tr>
          </thead>
          <tbody>
            
            <?php
              if(empty($categoryList)){
                  echo "<tr><td colspan='3'>LISTA VACIA</td></tr>";
              } else {
                    foreach ($categoryList as $category) {?>
                      <tr>
                        <th scope="row"><?php echo $category->getId()?></th>
                        <td> <?php echo $category->getName() ?> </td>
                        <td class="row">
                            <div class="p-0 col-6">
                              <form action="<?php echo FRONT_ROOT ?>category/editCategory" method="POST">
                                <input type="hidden" name="idCategory" value="<?php echo $category->getId()?>">
                                <input type="hidden" name="name" value="<?php echo $category->getName()?>">
                                <button type="submit" class="btn btn-warning mr-1"><i class="fas fa-pencil-alt"></i></button>
                              </form>
                            </div>
                            <div class="p-0  col-6">
                              <form action="<?php echo FRONT_ROOT ?>category/deleteCategory" method="POST">
                                <input type="hidden" name="id" value="<?php echo $category->getId()?>">
                                <button type="submit" class="btn btn-danger mr-3" ><i class="far fa-trash-alt"></i></button>
                              </form>
                            </div>
                          </td>
                      </tr>
            <?php }} ?>
          </tbody>
        </table>
      </div>
    </div>

    <!-- Cierre del main container -->
  </div> 