<?php
include("lib/qrcode/qrlib.php");
// capas podriamos meterlo en las constantes del config
// es IMPORTANTISIMO que el directorio tenga permisos
$tempDir ="lib/tmp/";




?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT ?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
  <link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">
  	<link rel="stylesheet" href="<?php echo CSS_PATH ?>ekko-lightbox.css">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >


    <div id="usercont" class="pb-3">
      <img class ="img-fluid" src="<?php echo IMG_PATH."events/"?>ticketgocover.jpg" alt="">
      <div class="shadow-lg negativemgt container rounded bg-light p-4 ">
        <h1><i class="far fa-address-card"></i> Mis Datos</h1>
          <div class="row">
            <!-- MAIN CONTENT -->
            <div class="row col-8 justify-content-center align-self-start">  
              <div class="data row col-12">
                <div class="col-md-3">
                <img src="<?php echo IMG_PATH?>profile.jpg" class="img-fluid rounded-circle shadow">
                </div>
                <div class="col">
                  <h2><?php  echo $user->getUserName()?></h2>
                  Email: <?php  echo $user->getEmail()  ?><br>
                  <div class="btn-group btn-group-sm mt-5" role="group">
                  <form action="<?php echo FRONT_ROOT?>user/changePasswordView" method="post">
                                  <input type="hidden" name="idUser" value=" <?php  echo $user->getId()  ?> ">
                                  <button type="submit" href="" class="btn btn-block rounded btn-success  mt-3"> Cambiar Contraseña</button>
                                </form>
                  </div>
                  <hr>
                </div>
              </div>
              <div class="purchases row col-12 mt-4">
                <h2><i class="fas fa-shopping-cart"></i>  Mis Tickets</h2>
                <div class="row col-12 justify-content-center align-self start">
                  <!-- inicia listado tickets -->
                  <?php
                    if(empty($purchasesArray))
                    {?>
                      <div class="mt-5 mb-5 border border-danger rounded ">
                        <h3 class="p-3">  <i class="far fa-sad-tear"></i> Aún no has comprado Tickets!</h3>
                      </div>
                    <?php
                    }

                    foreach($purchasesArray as $purchase)
                    { ?>
                      <div class="col-12 bg-dark text-white py-3 my-2 rounded">
                      <h5><i class="far fa-calendar-alt"></i> <?php echo $purchase->getDate();?></h5>
                      </div>
                      <?php 
                      foreach($purchasedTicketArray as $pticket)
                      {
                        if($pticket->getIdPurchase() == $purchase->getId())
                        { 
                          //seteo para ordenar un poco el codigo
                          $ticket = $this->ticketDAO->getById($pticket->getIdTicket());
                          $calendar = $ticket->getCalendar();
                          $calendar->setArtists($this->artistDAO->getArtists($calendar->getId()));

                          $venue = $calendar->getVenue();
                          $event = $calendar->getEvent();

                          //Seteo del QR
                          $filename=  rand(01,99).".png";
                          $qrContent= "GO TICKET! Ticketid: ".$pticket->getQr();
                          QRcode::png($qrContent, $tempDir.$filename, QR_ECLEVEL_L, 9); 
                          ?>
                          
                          <!-- START CARD -->
                          <div class="row border border-secondary rounded col-11 mt-2 mb-2 p-0 ">
                            <div class="col-md-3 p-0 m-0  bg-white">
                              <a href="<?php echo FRONT_ROOT.$tempDir.$filename?>" data-toggle="lightbox">
                                <img class="p-0 img-fluid align-self-center"  src="<?php echo FRONT_ROOT.$tempDir.$filename?>" alt="Qr Code" style="width:100%"/>
                              </a>
                            </div>
                            <div class="col-md-6 m-0">
                              <h2><?php echo $event->getName() ?></h2>
                              <div class="row data">
                                <div class="col">
                                  <span><i class="far fa-calendar-check"></i>  <?php echo $calendar->getDate() ?> </span><br>
                                </div>
                                <div class="col">
                                  <span data-toggle="popover" data-html="true" data-trigger="hover"  
                                    data-placement="auto"
                                    title="<i class='fas fa-info-circle'></i> <?php echo $venue->getName()?>" 
                                    data-content=" <?php echo $venue->getAddress()."</br>".$venue->getCity()->getName() ?>   " > 
                                    <i class="fas fa-map-marker-alt"></i>  <?php  echo $venue->getName()  ?>
                                  </span>
                                </div>
                              </div>
                              <p>Artistas: <?php 
                              
                              foreach($calendar->getArtists() as $artist)
                              {
                                echo $artist->getName()." · ";
                              }
                              ?></p>
                              <p><?php  echo $ticket->getTicketType()->getName()  ?> <br>Valor: $<?php  echo $ticket->getPrice()  ?></p>
                            </div>
                              <div class="col-md-3 d-flex flex-column align-items-stretch">
                                <form action="" method="" class="form-inline m-auto">
                                  <div class="form-group">
                                    Nº  <b><?php  echo $pticket->getId()  ?></b>    
                                  </div>
                                </form>
                                <h3 class="text-center m-auto"> <?php  echo $ticket->getTicketType()->getName()  ?>  </h3>
                                <form action="<?php echo FRONT_ROOT?>" method="post">
                                  <input type="hidden" name="lineId" value=" <?php  //echo $line->getId()  ?> ">
                                  <button type="submit" href="" class="btn btn-block rounded btn-danger mb-3">IMPRIMIR (?)</button>
                                </form>
                              </div>
                            </div>
                            <!-- END CARD -->     

                        
                        
                        
                        <?php }
                      }
                      
                    }                  
                  ?>
                         
                
                  <!-- fin listado tickets -->
                  </div>
              </div>
            
            </div>
            <!-- END MAIN CONTENT -->
            <!-- SIDEBAR! -->
            <div class="col-4 align-self-start">
              
            </div>
            <!-- END SIDEBAR -->
          </div>
      
      </div>
    </div>



    <!-- Cierre del main container -->
  </div> 

<script>
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
</script>

