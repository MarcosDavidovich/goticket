<?php



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >
  <div class="row row align-items-center bg-info p-3 px-4 ">
      <div class="col text-white">
        <h4><i class="fas fa-info-circle"></i>  Evento 
        > <?php echo $cal->getEvent()->getName()?> 
        > <?php echo $cal->getDate()." @ ".$cal->getVenue()->getName() ?> 
      </h4>
      </div>
      <div class="col-3">
        <a href="<?php echo FRONT_ROOT?>ticket/createTicketView" class="btn btn-warning" role="button" aria-disabled="true"><i class="fas fa-undo-alt"></i> VOLVER A TICKETS</a>
      </div>
    </div>

    <div class="container mt-3">
    <h3><i class="far fa-plus-square"></i> Modificacion de Tickets</h3>
    

<form action="<?php echo FRONT_ROOT ?>/ticket/modifyTicket" method="POST">
      <div class="row justify-content-center mt-4 mb-5">
        <div class="form-row col-10">
        <h3>Ticket #<?php echo $idTicket?></h3>
        </div>
          
        <div class="form-row col-10 mt-4">
        
          <div class="col">
          <?php if(empty($ticketTypeList)) {?>

            <a href="<?php echo FRONT_ROOT?>ticketType/ticketTypeView" class="btn btn-warning" role="button" aria-disabled="true">CREAR TIPO</a>
            <?php }else {?>
                <input type="hidden" name="idTicket" value="<?php echo $ticket->getId()?>">
                <input type="hidden" name="idTicketType" value="<?php echo $ticket->getTicketType()->getId()?>">
              <select name="ticketType" class="form-control" required>
                <?php foreach ($ticketTypeList as $ticketType) { ?>
                  <option <?php if($ticket->getTicketType()->getId() == $ticketType->getId()) { echo 'selected="true"';} ?>value="<?php echo $ticketType->getId() ?>"><?php echo $ticketType->getName() ?></option>
                <?php }?>
              </select>  
          <?php }?>
          </div>
          <div class="col">
            <input type="number" min= "1" name="price" class="form-control" id=""  value="<?php echo $price?>" required>
          </div>
          <div class="col">
            <input type="number" min= "1" name="available" class="form-control" id=""  value="<?php echo $available?>" required>
          </div>
          <div class="col">
            <button type="submit" class="col btn btn-outline-success"><i class="fas fa-pen-alt"></i> EDITAR</button>
          </div>
        </div>
      </div>          
    </form>
    </div>
    </div>
    </body>