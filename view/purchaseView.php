<?php
if (isset($_SESSION["purchase"]) && empty($_SESSION["purchase"]->getLines())) 
{
  header("location:".FRONT_ROOT."");
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
  <link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >

    <div id="purchasecont" class="pb-3">
      <img class ="img-fluid" src="<?php echo IMG_PATH."events/"?>ticketgocover.jpg" alt="">
      <div class="shadow-lg negativemgt container rounded bg-light pt-3 ">
        <h1 class="event-tit"><i class="fas fa-shopping-cart"></i>  Carrito</h1>
          <div class="row">
            <!-- MAIN CONTENT -->
            <div class="row col-8 justify-content-center align-self-start">  
              <?php 
                foreach($activePurchase->getLines() as $line ){
                  $ticket = $this->ticketDAO->getById($line->getIdTicket());
                  $calendar= $this->fillCalendar($ticket->getCalendar());
                  $venue= $calendar->getVenue();
                  $event = $calendar->getEvent();
                  
                  ?>
                  <!-- START CARD -->
                  <div class="row border border-secondary rounded col-11 mt-2 mb-2 p-0 ">
                    <div class="col-md-3 p-0 m-0" style="background: url('<?php echo IMG_PATH."events/".$event->getSqImg();?>') center center;  ">
                    </div>
                  <div class="col-md-6 m-0">
                  <a href=" <?php  echo FRONT_ROOT  ?>event/eventView/<?php echo $event->getId()?>" class="none">  
                    <h2><?php echo $event->getName()?></h2>
                  </a>
                    <div class="data">
                        <span><i class="far fa-calendar-check"></i> <b><?php echo $calendar->getDate() ?> </span></b><br>
                        <span data-toggle="popover" data-html="true" data-trigger="hover"  
                              data-placement="auto"
                              title="<i class='fas fa-info-circle'></i> <?php echo $venue->getName()?>" 
                              data-content=" <?php echo $venue->getAddress()."</br>".$venue->getCity()->getName() ?>   " > 
                          <i class="fas fa-map-marker-alt"></i>  <b><?php  echo $venue->getName()  ?></b>
                        </span>
                    </div>
                    <p>Artistas: <?php foreach($calendar->getArtists() as $artist)
                    {
                      echo $artist->getName().", ";
                    }
                    ?></p>
                    <p><i class="fas fa-ticket-alt"></i> <b><?php  echo $ticket->getTicketType()->getName()  ?></b> Precio: $<?php  echo $ticket->getPrice()  ?></p>
                  </div>
                    <div class="col-md-3 d-flex flex-column align-items-stretch">
                      <form action="" method="" class="form-inline m-auto">
                        <div class="form-group">
                          Cantidad:  <b><?php  echo $line->getQuantity()  ?></b>    
                          <!-- <input style="max-width: 30%;" type="number" name="quantity" class="ml-2 form-control form-control-sm" placeholder="1"> -->
                        </div>
                      </form>
                      <h3 class="text-center m-auto">$ <?php  echo $line->getPrice()  ?> </h3>
                      <form action="<?php echo FRONT_ROOT?>purchase/deleteLine" method="post">
                        <input type="hidden" name="lineId" value=" <?php  echo $line->getId()  ?> ">
                        <button type="submit" href="" class="btn btn-block rounded btn-danger mb-3">REMOVER</button>
                      </form>
                    </div>
                  </div>
                  <!-- END CARD -->
              <?php  }  ?>
            
            </div>
            <!-- END MAIN CONTENT -->
            <!-- SIDEBAR! -->
            <div class="col-4 align-self-start">
              <div class="row border border-dark rounded mt-2 mb-2 mr-3 p-2 ">
                <h4 class="text-center"><i class="far fa-credit-card bg-warning p-2"></i> Detalles de compra</h4>
                <hr>
                <?php
                  foreach($activePurchase->getLines() as $line ){
                    $ticket = $this->ticketDAO->getById($line->getIdTicket());
                    $calendar = $ticket->getCalendar();
                    $event = $calendar->getEvent();
                ?>
                  <div class="pline">
                    <h3><i class="far fa-star"></i> <?php  echo $event->getName()  ?></h3>
                    <div class="row border rounded p-2 ml-4 mb-3">
                      <div class="col-1">
                        <i class="far fa-calendar-check"></i> <br>
                        <i class="fas fa-map-marker-alt"></i> <br>
                        <i class="fas fa-map-marked-alt"></i> <br>
                        <i class="fas fa-globe-americas"></i>
                      </div>
                      <div class="col">
                        <?php echo $calendar->getDate() ?> <br>
                      <?php  echo $calendar->getVenue()->getName() ?><br>
                      <?php  echo $calendar->getVenue()->getAddress() ?> <br>
                      <?php  echo $calendar->getVenue()->getCity()->getName() ?>
                      </div>
                    

                    </div>
                    <h3 style="text-align:center;"><i class="fas fa-ticket-alt"></i> <?php  echo $line->getQuantity()  ?>   x   <?php  echo $ticket->getTicketType()->getName()  ?></h3>
                    Subtotal: $<?php  echo $ticket->getPrice()*$line->getQuantity()  ?><br>
                    <hr>
                  </div>
                  <?php } ?>

                <div class="total">
                <?php              
                ?>
                  <span>Total:</span>
                  <span><b>$<?php  
                  $activePurchase->calculateTotal();
                  echo $activePurchase->getTotal();
                  ?></b></span>
                </div>
              
                <?php if(isset($_SESSION['activeUser'])) {?>
                  <a href="<?php echo FRONT_ROOT?>purchase/addPurchase" class="btn btn-block rounded btn-success  mt-3"><i class="far fa-handshake"></i> CHECKOUT</a>
                  <?php  } else{  ?>
                    <a href="<?php echo FRONT_ROOT?>user/loginView" class="btn btn-block rounded btn-warning  mt-3">LOGIN</a>
                  <?php  }  ?>
              </div>
            </div>
            <!-- END SIDEBAR -->
          </div>
      
      </div>
    </div>

    <!-- Cierre del main container -->
  </div> 



