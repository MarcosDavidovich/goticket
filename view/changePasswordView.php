<?php


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
  <link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">  
  
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >
    <div class="row align-items-center bg-info p-3 mb-4 px-4 ">
      <div class="col text-white  ">
        <h4><i class="fas fa-info-circle"></i> Cambio de Contraseña</h4>
      </div>      
      <div class="col-3">
      </div>
    </div>

    <div class="container mt-3">
    <?php if (" "!=$message) { ?>
      <div class="alert alert-success" role="alert">
          <?php echo $message; ?>
      </div><?php
     }?>        
      <div class="row justify-content-center mt-5 mb-5 col-12">
        <form  class="col-6 "action="<?php echo FRONT_ROOT?>user/changePassword" method="POST">
            <input type="hidden" name="idUser" value="<?php echo $user->getId()?>">
            <div class="form-row my-2">
              <div class="col">
                <input type="password" class="form-control" name="newPassword" maxlength="30" placeholder="Nueva contraseña"required>
              </div>
            </div>
            <div class="form-row my-2">
              <div class="col">
                <input type="password" class="form-control" name="newPassword2" maxlength="30" placeholder="Repita Nueva contraseña" required>
              </div>
            </div>
            <div class="form-row my-2">
              <div class="col">
                <input type="password" class="form-control" name="oldPassword" maxlength="30" placeholder="Contraseña anterior" required>
              </div>
            </div>
            <div class="form-row my-2">
              <div class="col">
                <button type="submit" class="btn btn-outline-success btn-block"><i class="fas fa-pen-alt"></i>  EDITAR</button>
              </div>
            </div>
        </form>
      </div>
 
    </div>
        <!-- Cierre del main container -->
  </div>
