<?php



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT ?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


<style>


</style>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="maincont" >

    <!-- START SLIDER -->
    <div id="slider" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#slider" data-slide-to="0" class="active"></li>
        <li data-target="#slider" data-slide-to="1"></li>
        <li data-target="#slider" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="<?php echo IMG_PATH?>slide1.jpg" alt="First slide">
          <div class="carousel-caption d-none d-md-block">
            <h5>GO TICKET!</h5>
            <p>El Lugar donde tus sueños se hacen realidad</p>
          </div>
        </div>
        <div class="carousel-item ">
          <img class="d-block w-100" src="<?php echo IMG_PATH?>slide2.jpg" alt="First slide">
          <div class="carousel-caption d-none d-md-block">
            <h5>GO TICKET!</h5>
            <p>El Lugar donde tus sueños se hacen realidad</p>
          </div>
        </div>
        <div class="carousel-item ">
          <img class="d-block w-100" src="<?php echo IMG_PATH?>slide3.jpg" alt="First slide">
          <div class="carousel-caption d-none d-md-block">
            <h5>GO TICKET!</h5>
            <p>El Lugar donde tus sueños se hacen realidad</p>
          </div>
        </div>
      
      </div>
      <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <!-- END SLIDER  -->


    <!-- START CARDS  -->

    <div class="container justify-content-center mt-5">
      <div class="row justify-content-center my-4">
        <div class="col text-center">
          <span class="neon">ÚLTIMOS</span>
          <span class="neon">EVENTOS</span>
        </div>
      </div>
    
      <div class="row">
        
        <?php foreach($eventList as $event) { ?>
        <div class="col-3 mb-3">
        <a href=" <?php  echo FRONT_ROOT  ?>event/eventView/<?php echo $event->getId()?>" class="none"> 
          <div class="card card-light">
            <div class="card-img" style="
            background: url(<?php echo IMG_PATH."events/".$event->getSqImg();?>) no-repeat center center;
            background-size: cover; ">
            </div>
              <div class="card-body">
                <h5 class="card-title"> <?php  echo $event->getName()  ?></h5>
                <!-- <p class="card-text"><?php  //echo $event->getDescription()  ?></p> -->
                <a href=" <?php  echo FRONT_ROOT  ?>event/eventView/<?php echo $event->getId()?> " class="btn btn-outline-success btn-block mt-4"><i class="fas fa-cart-plus"></i> COMPRAR TICKETS</a>
              </div>
          </div>
        </a>

        </div>
        <?php };?>
      </div>

   


    </div>



 
  
  <!-- END CARDS  -->


  <!-- Modal close -->
  <div class="modal fade" id="modalx" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalx">Cerrando sesion...</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Hasta la proxima!
        </div>
        <div class="modal-footer">
          <a href="<?php echo FRONT_ROOT?>home/index" class="btn btn-danger">Adios!</a>
        </div>
      </div>
    </div>
  </div>

</div>

<script>
  
  function myAlert(){
    console.log( "ready!" );
    $('#modalx').modal('show');
  }
</script>

