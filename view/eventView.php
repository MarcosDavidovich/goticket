<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
  
  <link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
  <link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">
  
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


<style>

</style>
</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >          
         <div id="eventcont" class="">
         <div class="cover-img" style="
            background: url(<?php echo IMG_PATH."events/".$event->getCoverImg();?>) no-repeat center center;
            background-size: cover; ">
            </div>
            <div class="negativemgt container rounded bg-light pt-3">
              <div class="row"></div>
              
                <h1 class="event-tit"><i class="fas fa-chevron-circle-right"></i>  <?php  echo $event->getName()  ?></h1>
                <div class="row">

                  <!-- MAIN CONTENT -->
                  <div class="col-9"> 
                    <div class="eventDescription mt-3 mb-3">
                      <?php  echo $event->getDescription()  ?>
                    </div>
                    <hr>
                    <h4><i class="fas fa-ticket-alt"></i>  TICKETS</h4>
                    <?php if (isset($message)) {
                     if (""!=$message) { 
                      ?><div class="alert alert-success" role="alert">
                      <?php echo $message; ?>
                    </div><?php
                      }}?>
                      
                    <div class="row eventCalendars mt-3 justify-content-center">
                      <?php  
                      if(empty($calendarList))
                      { ?>
                        <div class="mt-5 mb-5 border border-danger rounded ">
                        <h3 class="p-3">  <i class="far fa-dizzy"></i>  No hay Calendarios para este evento!</h3>
                        </div>
                      
                      <?php }
                      foreach($calendarList as $calendar){  
                        if($calendar->getEvent()->getId() == $event->getId()){ ?>
                          <div class="border border-secondary rounded col-11 mt-2 mb-2 ">
                            <div class="row bg-dark text-light pt-3 pb-3 align-items-center">
                              <div class="col-lg-6 col-sm-12 pr-0"> 
                                <h6>
                                  <i class="fas fa-chevron-right"></i> <?php foreach($calendar->getArtists() as $artist){ echo $artist->getName()." · "; } ?>
                                </h6>
                              </div>
                            
                              <div class="col-lg-2 col-sm-12 p-0"> <h5><i class="far fa-calendar-alt"></i> <?php  echo $calendar->getDate()  ?> </h5></div>

                              <div class="col-lg-4 col-sm-12 pr-0"> 
                                <h5 class="">
                                  <i class="fas fa-map-marker-alt" ></i>
                                  <span data-toggle="popover" data-html="true" data-trigger="hover"  
                                    data-placement="auto"
                                    title="<i class='fas fa-info-circle'></i> <?php echo $calendar->getVenue()->getName()?>" 
                                    data-content=" <?php echo $calendar->getVenue()->getAddress()."</br>".$calendar->getVenue()->getCity()->getName() ?>   " > 
                                  <?php  echo $calendar->getVenue()->getName()  ?>
                                  </span>
                                </h5>
                              </div>
                            </div>
                            <?php
                            $tcount= false;
                            foreach ($ticketList as $ticket) { 
                              $ticket->setRemain($ticket->getAvailable() - $this->purchasedTicketDAO->getUsedTicketsByIdCalendarAndIdTicket($calendar->getId(), $ticket->getId())) ;

                                if($ticket->getCalendar()->getId() == $calendar->getId() && $ticket->getRemain() > 0) {
                                  $tcount = true;
                                  ?>
                                  <!-- START TICKET -->
                                  <div class="row pt-3 pb-3 align-items-center">
                                    <div class="col-2"> <?php  echo $ticket->getTicketType()->getName();  ?> </div>
                                    <div class="col-3 text-center">$<?php  echo $ticket->getPrice();  ?></div>
                                    <div class="col-3">DISPONIBLES <?php echo $ticket->getRemain()?></div>
                                    <div class="col-4">
                                      <form action=" <?php echo FRONT_ROOT?>purchase/addTicket " method="POST" class="">
                                        <div class="form-row">
                                          <div class="col-4">
                                            <input type="hidden" name="ticketId" value="<?php echo $ticket->getId() ?>">
                                            <input type="number" min="1" class="form-control" name="quantity" value='1'>
                                          </div>
                                          <div class="col-8">
                                            <button type="submit"  class="button btn-outline-success form-control"><i class="fas fa-cart-plus"></i> AGREGAR</button>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                  <!-- END TICKET -->
                              <?php
                                }  
                              }
                              if(!$tcount)
                              { ?>
                                <div class="mt-3 mb-3 text-center border border-danger rounded">
                                <h5 class="p-3">  <i class="far fa-dizzy"></i>  No hay Tickets para esta Fecha!</h5>
                                </div>
                              <?php } ?>
                            
                          </div>
                        <?php }
                      }?>
                    </div>
                  </div>
                  <!-- END MAIN CONTENT -->
                  <!-- SIDEBAR! -->
                  <div class="col">
                    <h5>SIDEBAR</h5>
                  </div>
                  <!-- END SIDEBAR -->
                </div>
            
            </div>
          </div>
    <!-- Cierre del main container -->
  </div>




