<?php



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
<link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css"> 
 
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >
    <?php if(empty($eventList)) { ?> 
      <a href="<?php echo FRONT_ROOT?>event/createEventView" class="btn btn-warning" role="button" aria-disabled="true">CREAR EVENTO</a>
    <?php }else {?>
      <div class="row align-items-center bg-info p-3 mb-4 px-4">
        <div class="col text-white  ">
          <h4><i class="fas fa-info-circle"></i> Evento > <?php echo $localEvent->getName()?></h4>
        </div>
        <div class="col-2">
          <form action="<?php echo FRONT_ROOT ?>event/editEvent" method="post" >
            <input type="hidden" name="eventId" value="<?php echo $eventId ?>">
            <button type="submit" class="btn btn-warning mr-1"><i class="fas fa-pencil-alt"></i> EDITAR EVENTO</button>
          </form>
        </div>
        <div class="col-2">
        <?php if(count($filteredCalendars)>0) {?>
          <a href="<?php echo FRONT_ROOT?>ticket/createTicketView" class="btn btn-warning" role="button" aria-disabled="true"> <i class="fas fa-ticket-alt"></i> CREAR TICKETS</a>
        <?php } ?>
        </div>
        <div class="col-3">
          <a href="<?php echo FRONT_ROOT?>event/createEventView" class="btn btn-warning" role="button" aria-disabled="true"><i class="fas fa-undo-alt"></i> VOLVER A EVENTOS</a>
        </div>
      </div>
    
    <?php }?>
    <div class="container mt-3">
    <h3> <i class="far fa-calendar-plus"></i> Crear Calendario</h3>

    <form action="<?php echo FRONT_ROOT?>calendar/createCalendar" method="POST">
      <input type="hidden" name="eventId" value="<?php echo $localEvent->getId() ?>">
      <div class="row justify-content-center mt-3 mb-5">
        <div class="form-row col-sm-9">
          <div class="col">
            <input type="date" name="date" class="form-control" placeholder="Fecha" required>
          </div>
          <div class="col">
            <?php if(empty($venuesList)) { ?>
              <a href="<?php echo FRONT_ROOT?>venue/venueView" class="btn btn-warning" role="button" aria-disabled="true">CREAR LUGARES</a>
            <?php }else {?>
                <select name="venue"class="form-control" required>
                    <option value="" disabled selected>Seleccione Lugar</option>
                    <?php foreach ($venuesList as $venue) { ?>
                    <option value="<?php echo $venue->getId() ?>"><?php echo $venue->getName() ?></option>
                    <?php }?>
                </select>
            <?php }?>
          </div>
        </div>
        <div class="form-row col-sm-9 my-3">
          <div class="col-sm-2">
            <h4>Artistas </h4>
          </div>
          <div class="col-sm-10">
              <?php if(empty($artistsList)) { ?>
                <a href="<?php echo FRONT_ROOT?>artist/artistView" class="btn btn-warning" role="button" aria-disabled="true">CREAR ARTISTAS</a>
                <?php }else {?>
                  <select class="js-example-basic-multiple" name="artistArray[]" multiple="multiple" required>
                  <?php foreach ($artistsList as $artist) { ?>
                    <option value="<?php echo $artist->getId() ?>"><?php echo $artist->getName() ?></option>
                  <?php } ?>
                  </select>
              <?php }?>
          </div>
        </div>
        <div class="form-row col-sm-9">
          <div class="col-sm"></div>
        <div class="col-sm-4 align-self-right">
          <button type="submit" class="btn btn-outline-success btn-block"><i class="fas fa-plus"></i>  AGREGAR</button>
        </div>
      
        </div>
      </div>
    </form>
                    
    <?php if ("" !=$message) {
      ?><div class="alert alert-success" role="alert">
      <?php echo $message; ?>
    </div><?php
    }?>

    <hr>

    <h4><i class="far fa-list-alt"></i> Calendarios para "<?php echo $localEvent->getName()?>" </h4>

    <?php
        if(empty($filteredCalendars)){ ?>
          <div class="row justify-content-center mt-5 mb-5">
            <div class="mt-5 mb-5 border border-danger rounded ">
              <h3 class="p-3">  <i class="far fa-sad-tear"></i> No hay calendarios para este evento</h3>
            </div>
          </div>
        <?php } else {
            foreach ($filteredCalendars as $calendar) {
              if($calendar->getEvent()->getId() == $localEvent->getId() ){
              ?>
                <div class="row justify-content-center mt-5 mb-5">
                  <div class="col-10 p-0 justify-content-center border border-dark rounded">
                    <table class="table col-12">
                      <thead class="thead-light">
                        <tr class="">
                          <th scope="col" >#</th>
                          <th scope="col" >Fecha</th>
                          <th scope="col" >Lugar</th>
                          <th scope="col" >Artistas</th>
                          <th scope="col" ></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row"><?php echo $calendar->getId()?></th>
                          <td><?php echo $calendar->getDate()?></td>
                          <td><?php echo $calendar->getVenue()->getName()?></td>
                          <td>
                            <?php 
                            foreach ($calendar->getArtists() as $artist) {
                              echo $artist->getName() . "<br>";
                            } ?>
                          </td>
                          <td class="row p-0 m-0 align-middle">
                            <div class="p-0 col-5 mr-1 mt-2">
                              <form action="<?php echo FRONT_ROOT ?>calendar/updateCalendar" method="POST">
                                <input type="hidden" name="calendarId" value="<?php echo $calendar->getId()?>">
                                <input type="hidden" name="eventId" value="<?php echo $localEvent->getId()?>">
                                <button type="submit" class="btn btn-warning  form-control mr-1"><i class="fas fa-pencil-alt"></i></button>
                              </form>
                            </div>
                            <div class="p-0  col-6 mt-2">
                              <form action="<?php echo FRONT_ROOT ?>calendar/deleteCalendar" method="POST">
                                <input type="hidden" name="id" value="<?php echo $calendar->getId()?>">
                                <button type="submit" class="btn btn-danger form-control" ><i class="far fa-trash-alt"></i></button>
                              </form>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="5" align="center"><b>Tickets:</b></td>
                        </tr>
                        <tr> <td colspan="5" align="center">
                          <?php 
                          
                            if(!empty($filteredTickets)) { ?>
                              <table class="table table-sm table-hover col-sm-10">
                                <thead class="thead-light">
                                  <tr class="">
                                    <th scope="col">#</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col">Cantidad</th>
                                    <th scope="col">Disponibles</th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  foreach ($filteredTickets as $ticket) {
                                    $ticket->setRemain($ticket->getAvailable() - $this->purchasedTicketDAO->getUsedTicketsByIdCalendarAndIdTicket($calendar->getId(), $ticket->getId())) ;

                                    $calAux = $ticket->getCalendar();          
                                    if($calAux->getDate() == $calendar->getDate() && $calAux->getVenue()->getName() == $calendar->getVenue()->getName()) { ?>
                                      <tr>
                                        <th scope="row"><?php echo $ticket->getId()?></th>
                                        <td><?php echo $ticket->getTicketType()->getName()?></td>
                                        <td>$<?php echo $ticket->getPrice()?> </td>
                                        <td><?php echo $ticket->getAvailable()?> </td>
                                        <td><?php echo $ticket->getRemain()?></td>
                                        <td>
                                        <form action="<?php echo FRONT_ROOT ?>ticket/editTicket" method="POST">
                                            <input type="hidden" name="idTicket" value="<?php echo $ticket->getId()?>">
                                            <input type="hidden" name="idCalendar" value="<?php echo $ticket->getCalendar()->getId()?>">
                                            <input type="hidden" name="idTicketType" value="<?php echo $ticket->getTicketType()->getId()?>">
                                            <input type="hidden" name="price" value="<?php echo $ticket->getPrice()?>"> 
                                            <input type="hidden" name="available" value="<?php echo $ticket->getAvailable()?>">  
                                            <input type="hidden" name="remain" value="<?php echo $ticket->getRemain()?>">  
                                          <button type="submit" class="btn btn-warning col-8"><i class="fas fa-pencil-alt"></i></button>    
                                          </form>
                                        </td>
                                        <td>
                                        <form action="<?php echo FRONT_ROOT ?>ticket/deleteTicket" method="POST">
                                          <input type="hidden" name="id" value="<?php echo $ticket->getId()?>">
                                          <button type="submit" class="btn btn-danger col-9" ><i class="far fa-trash-alt"></i></button>
                                        </form>
                                        </td>
                                      </tr><?php
                                    } 
                                  } ?>
                                </tbody>
                              </table> <?php 
                            }else{ 
                              echo "<tr><td colspan='5' align='center'><b>No Hay Tickets para esta fecha de calendario</b></td></tr>";
                            }
                            ?>
                        </td> </tr>
                      </tbody>
                    </table>          
                  </div>
                </div>

        <?php } else {
                    echo "<h5>No hay calendarios creados...</h5>";
                  } 
            }
        } ?>
    </div>
  
  
  
  
  
      <!-- Cierre del main container -->
  </div>
  <script>
  $(document).ready(function() {
    $('.js-example-basic-multiple').select2({
      placeholder: 'Seleccione Artistas',
      width: '100%'
    });
  });
  </script>