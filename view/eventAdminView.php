<?php



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >
  <div class="row align-items-center bg-info p-3 mb-4 px-4">
      <div class="col text-white  ">
        <h4><i class="fas fa-info-circle"></i> GoTicket > Eventos </h4>
      </div>      
      <div class="col-3">
        <!-- <a href="<?php //echo FRONT_ROOT?>calendar/calendarView" class="btn btn-warning" role="button" aria-disabled="true"><i class="fas fa-undo-alt"></i> VOLVER A CALENDARIOS</a> -->
      </div>
    </div>  

        <div class="container mt-3">

          <h2><i class="far fa-plus-square"></i> Nuevo Evento</h2>

                      <?php if (""!=$message) { 
                      ?><div class="alert alert-success" role="alert">
                      <?php echo $message; ?>
                      </div><?php
                      }?>
          <form action="<?php echo FRONT_ROOT?>event/createEvent" enctype="multipart/form-data" method="POST">
          <div class="row justify-content-center mt-5 mb-5 ">
              <div class="form-row col-sm-10 ">
                <div class="col-3">
                  <input type="text" name="name"  class="form-control" id="" placeholder="Nombre del Evento"  maxlength= "30" autocomplete="off" required> 
                </div>
                <div class="col">
                  <input type="text" name="description" class="form-control" id=""  autocomplete="off"  maxlength= "60" placeholder="Descripción" required> 
                </div>
                <?php if(empty($categoryList)) { ?>
                  <div class="col-2">
                    <a href="<?php echo FRONT_ROOT?>category/categoryView" class="btn btn-warning" role="button" aria-disabled="true">CREAR CATEGORIAS</a> 
                  </div>
                  <?php }else {?>
                  <div class="col-2">
                    <select name="category" class="form-control" required>
                      <option value="" disabled selected>Categoria</option>
                      <?php foreach ($categoryList as $category) { ?>
                      <option value="<?php echo $category->getId() ?>"><?php echo $category->getName(); ?></option>
                      <?php }?>
                    </select>
                  </div>
                <?php }?>
              </div>
              <div class="form-row col-sm-10 mt-4">
              
                  <div class="col form-group border rounded p-3 mr-2">
                      <label for="cover_img" class="bg-info text-white p-2"><i class="far fa-image"></i> Imagen de portada</label>
                      <input type="file" class="form-control-file" name="coverImg" id="">
                      <small id="helpblock" class="form-text text-muted">
                        Archivo png o jpg de 1300px x 400px.
                      </small>
                  </div>
                  <div class="col form-group border rounded p-3">
                      <label for="sq_img" class="bg-info text-white p-2"><i class="far fa-image"></i> Imagen de perfil</label>
                      <input type="file" class="form-control-file" name="sqImg" id="">
                      <small id="helpblock" class="form-text text-muted">
                      Archivo png o jpg de 500px x 500px.                      
                      </small>
                  </div>                           

              </div>
              <div class="form-row col-sm-10 mt-3">
                  <div class="col-8">
                  </div>                           
                  <div class="col-4">
                    <button type="submit" class="btn btn-outline-success btn-block"><i class="fas fa-plus"></i> CREAR</button>
                  </div>                           
              </div>
        </div>
        </form>

        <hr>

        <h2><i class="far fa-list-alt"></i> Listado de Eventos</h2>
        <?php
          if(empty($eventList)){
              echo "<h5>No hay eventos creados</h5>";
          } else {
              foreach ($eventList as $event) {?>
                <div class="row justify-content-center mt-5 mb-5">
                  <div class="row rounded col-10 p-3 bg-secondary text-white">
                    <div class="col-sm-2">
                      #<?php echo $event->getId()?>
                    </div>
                    <div class="col-sm-5">
                    <?php echo $event->getName()?>
                    </div>
                    <div class="col-sm-2">
                      Publicado
                    </div>
                    <div class="col-sm-3 row">
                      <form action="<?php echo FRONT_ROOT ?>calendar/editCalendar" method="post" >
                        <input type="hidden" name="eventId" value="<?php echo $event->getId() ?>">
                        <button type="submit" class="btn btn-warning mr-1"><i class="far fa-calendar-plus"></i></button>
                      </form>
                      <form action="<?php echo FRONT_ROOT ?>event/deleteEvent" method="POST">
                        <input type="hidden" name="id" value="<?php echo $event->getId()?>">
                        <button type="submit" class="btn btn-danger form-control" ><i class="far fa-trash-alt"></i></button>
                      </form>
                    </div>
                  </div>
                  <div class="row justify-content-center rounded col-10 p-3 bg-light">
                    
                    <?php
                      $calCount= $this->calendarCount($event->getId());
                      if($calCount>0)
                      { ?>
                        <div class="col-sm-6 text-center">
                        Calendarios (<?php echo $calCount ?>)
                        </div>
                        <div class="col-sm-6 text-center">
                        <?php
                        $ticketCount = $this->ticketCount($event->getId());
                        ?>
                        Tickets(<?php echo $ticketCount ?>)
                        </div>
                      <?php } else { ?>
                        <form action="<?php echo FRONT_ROOT ?>calendar/editCalendar" method="post" >
                          <input type="hidden" name="eventId" value="<?php echo $event->getId() ?>">
                          <button type="submit" class="btn btn-warning mr-1"><i class="far fa-calendar-plus"></i> CREAR CALENDARIOS</button>
                        </form>
                      <?php
                      }
                      ?>
                  </div>
                </div>
              <?php }}?>
      </div>
   
   
      <!-- Cierre del main container -->
  </div> 
                
                  
