<?php


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
  <link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >
    <div class="row row align-items-center bg-info p-3 px-4 ">
      <div class="col text-white">
        <h4><i class="fas fa-info-circle"></i>  Evento > <?php echo $localEvent->getName()?></h4>
      </div>
      <div class="col-3">
        <a href="<?php echo FRONT_ROOT?>calendar/calendarView" class="btn btn-warning" role="button" aria-disabled="true"><i class="fas fa-undo-alt"></i> VOLVER A CALENDARIOS</a>
      </div>
    </div>

    <div class="container mt-3">

    <h3><i class="far fa-plus-square"></i> Alta de Tickets</h3>

    <form id="createTicketForm" action="<?php echo FRONT_ROOT ?>ticket/createTickets" method="POST">
      
      <div class="row justify-content-center mt-4 mb-5">

        <div class="form-row col-10">
         <?php if(empty($calendarList)) {?>
          <a href="<?php echo FRONT_ROOT?>calendar/calendarView" class="btn btn-warning" role="button" aria-disabled="true">CREAR CALENDARIO</a>
          <?php }else {?>
            <b>Evento y Calendario: </b>
            <select id="select-calendar"class="calendarselector" name="artistArray[]" multiple="multiple" required>
            <?php foreach ($calendarList as $calendar) { 
              if($calendar->getEvent()->getId() == $localEvent->getId()){
              ?>
                <option value="<?php echo $calendar->getId() ?>"><?php echo $calendar->getEvent()->getName()." - ".$calendar->getDate(). " @ ".$calendar->getVenue()->getName() ?></option>
            <?php }} }?>
           </select>
        </div>

        
        <div class="form-row col-10 mt-4">
          <div class="col">
          <?php if(empty($ticketTypeList)) {?>

            <a href="<?php echo FRONT_ROOT?>ticketType/ticketTypeView" class="btn btn-warning" role="button" aria-disabled="true">CREAR TIPO</a>
            <?php }else {?>
              <select id="select-type" name="ticketType" class="form-control" required>
                <option value="" disabled selected>Tipo de Ticket</option>
                <?php foreach ($ticketTypeList as $ticketType) { ?>
                  <option value="<?php echo $ticketType->getId() ?>"><?php echo $ticketType->getName() ?></option>
                <?php }?>
              </select>  
          <?php }?>
          </div>
          <div class="col">
            <input type="number" name="price" min= "1" class="form-control" id=""  placeholder="Precio" required>
          </div>
          <div class="col">
            <input type="number" name="available" min= "1" class="form-control" id=""  placeholder="Cantidad Disponible" required>
          </div>
          <div class="col">
            <button type="submit" class="col btn btn-outline-success">AGREGAR</button>
          </div>
        </div>
      </div>          
    </form>
    <?php if($message!= "") {?>
    <div class="alert alert-success" role="alert">
      <?php echo $message; ?>
    </div>
    <?php }?>
    <hr>

    <?php 
        
        $ticketTypeList = $this->ticketTypeDAO->getAllActives();
    ?>
    <h4><i class="far fa-list-alt"></i> Listado de Tickets para "<?php echo $localEvent->getName() ?>"</h4>

    <table class="table table-sm table-hover">
      <thead class="thead-light">
        <tr class="">
          <th scope="col">#</th>
          <th scope="col">Fecha</th>
          <th scope="col">Lugar</th>
          <th scope="col">Tipo</th>
          <th scope="col">Precio</th>
          <th scope="col">Cantidad</th>
          <th scope="col">Disponibles</th>
          <th scope="col"></th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        
        <?php
          if(empty($ticketList)){
              echo "<td colspan='8'>LISTA VACIA</td>";
          } else {
              foreach ($ticketList as $ticket) {
                $calAux = $ticket->getCalendar();
                $ticket->setRemain($ticket->getAvailable() - $this->purchasedTicketDAO->getUsedTicketsByIdCalendarAndIdTicket($calAux->getId(), $ticket->getId())) ;
                if($calAux->getEvent()->getId() == $localEvent->getId() ){?>
                  <tr>
                    <th scope="row"><?php echo $ticket->getId()?></th>
                    <td><?php echo $calAux->getDate()?> </td>
                    <td><?php echo $calAux->getVenue()->getName() ?> </td>
                    <td><?php echo $ticket->getTicketType()->getName()?></td>
                    <td>$<?php echo $ticket->getPrice()?> </td>
                    <td><?php echo $ticket->getAvailable()?> </td>
                    <td><?php echo $ticket->getRemain()?></td>
                    <td>
                                        <form action="<?php echo FRONT_ROOT ?>ticket/editTicket" method="POST">
                                            <input type="hidden" name="idTicket" value="<?php echo $ticket->getId()?>">
                                            <input type="hidden" name="idCalendar" value="<?php echo $ticket->getCalendar()->getId()?>">
                                            <input type="hidden" name="idTicketType" value="<?php echo $ticket->getTicketType()->getId()?>">
                                            <input type="hidden" name="price" value="<?php echo $ticket->getPrice()?>"> 
                                            <input type="hidden" name="available" value="<?php echo $ticket->getAvailable()?>">  
                                          <button type="submit" class="btn btn-warning col-8"><i class="fas fa-pencil-alt"></i></button>    
                                          </form>
                                        </td>
                                        <td>
                                        <form action="<?php echo FRONT_ROOT ?>ticket/deleteTicket" method="POST">
                                          <input type="hidden" name="id" value="<?php echo $ticket->getId()?>">
                                          <button type="submit" class="btn btn-danger col-9" ><i class="far fa-trash-alt"></i></button>
                                        </form>
                                        </td>
                  </tr>
        <?php }}} ?>
      </tbody>
    </table>
    </div>
    <!-- Cierre del main container -->
  </div>   
  <script>

  $(document).ready(function() {
    $('.calendarselector').select2({
      placeholder: 'Seleccione uno o más Calendarios',
      width: '100%'
    });
  });
  </script>