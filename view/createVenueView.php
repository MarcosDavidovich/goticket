<?php




?>
<h1>Welcome</h1>
<h3>Alta de Lugares</h3>
<form action="<?php echo FRONT_ROOT ?>venue/createVenue" method="POST">
  <input type="text" name="name" id="" maxlength="30" placeholder="Nombre del lugar" required>
  <input type="text" name="description" id="" maxlength="30" placeholder="Descripcion">
  <input type="text" name="address" id="" maxlength="30" placeholder="Direccion" required>
  <?php if(empty($cityList)) {?>
    <a href="<?php echo FRONT_ROOT?>city/createCityView" class="btn btn-warning" role="button" aria-disabled="true">CREAR LUGARES</a>
    <?php
  }else {?>
    <select name="city">
      <?php foreach ($cityList as $city) { ?>
        <option value="<?php echo $city->getId(); ?>"><?php echo $city->getName(); ?></option>
      <?php }?>
    </select>
    <button type="submit" class="btn btn-outline-success">AGREGAR</button>
  <?php }?>
</form>
<?php if($message!= " ") {?>
<div class="alert alert-success" role="alert">
  <?php echo $message; ?>
</div>
<?php }?>

