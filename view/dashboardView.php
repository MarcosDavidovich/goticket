<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >
    <div class="row align-items-center bg-info p-3 mb-4 px-4">
      <div class="col text-white  ">
        <h4><i class="fas fa-info-circle"></i> Administrador > Dashboard </h4>
      </div>      
      <div class="col">
        <div class="btn-group" role="group" aria-label="Basic example">
          <a href="<?php echo FRONT_ROOT?>event/createEventView" class="btn btn-warning">Eventos</a>
          <a href="<?php echo FRONT_ROOT?>artist/artistView" class="btn btn-warning">Artistas</a>
          <a href="<?php echo FRONT_ROOT?>category/categoryView" class="btn btn-warning">Categorias</a>
          <a href="<?php echo FRONT_ROOT?>city/cityView" class="btn btn-warning">Lugares</a>
          <a href="<?php echo FRONT_ROOT?>venue/venueView" class="btn btn-warning">Ciudades</a>
          <a href="<?php echo FRONT_ROOT?>ticketType/ticketTypeView" class="btn btn-warning">Tipos de Ticket</a>
          <a href="<?php echo FRONT_ROOT?>user/adminView" class="btn btn-warning">Usuarios</a>
        </div> 
      </div>
    </div>  

    <div class="container mt-3">

        <h2><i class="fas fa-hand-holding-usd"></i>  Ventas</h2>
        
        <div class="row">
          <div class="col border-right border-secondary p-4">
            <h3><i class="fas fa-piggy-bank"></i>  Diario</h3>
              <div class="pl-3 mt-4">
                <h5> Tickets Vendidos <span style="color:darkred;"> <?php echo $daily["cantidad"]?></span> </h5>
                <h5> Total recaudado <span style="color:darkred;"> $<?php echo $daily["total"]?> </span></h5>
              </div>
          </div>
          <div class="col border-right border-secondary p-4">
            <h3><i class="fas fa-piggy-bank"></i>  Semanal</h3>
            <div class="pl-3 mt-4">
              <h5> Tickets Vendidos <span style="color:darkred;"> <?php echo $weekly["cantidad"]?> </span> </h5>
              <h5> Total recaudado <span style="color:darkred;"> $<?php echo $weekly["total"]?> </span></h5>
            </div>
          </div>
          <div class="col border-secondary p-4">
            <h3><i class="fas fa-piggy-bank"></i>  Mensual</h3>
            <div class="pl-3 mt-4">
              <h5> Tickets Vendidos <span style="color:darkred;"> <?php echo $monthly["cantidad"]?> </span> </h5>
              <h5> Total recaudado <span style="color:darkred;"> $<?php echo $monthly["total"]?> </span></h5>
            </div>
          </div>
        </div>

        <div class="row border rounded p-4 mt-4">
          <div class="col">
          <form action="<?php echo FRONT_ROOT?>home/dashboardView" method="POST">
            <h3>Consulta de Ventas</h3>
            <div class="form-row mb-3">
              <div class="col">
                <input type="date" name="date1" class="form-control" 
                  <?php
                      if ($range){ ?>
                        value="<?php echo $date1;?>"
                      <?php
                      }
                    ?>
                    id="" required>
              </div>
              <div class="col">
                <input type="date" name="date2" class="form-control" 
                  <?php
                      if ($range){ ?>
                        value="<?php echo $date2;?>"
                      <?php
                      }
                    ?>
                    id="" required>
              </div>
            </div>
            <div class="form-row">
              <div class="col">
                <button type="submit" class="btn btn-outline-success btn-block">
                      <?php if ($range) { 
                        ?>                            
                        <i class="fas fa-sync-alt"></i> ACTUALIZAR
                      <?php } else { ?>
                        <i class="fas fa-filter"></i>  CONSULTAR
                      <?php } ?>
                </button>              
              </div>
            </div>
          
          </form>
          </div>
          <div class="col">
            <?php if($range) { ?>
              <h4><i class="fas fa-piggy-bank"></i>  Consulta del <?php echo $date1?> al <?php echo $date2?> </h4>
              <div class="pl-5 mt-4">
                <h5> Tickets Vendidos <span style="color:darkred;"> <?php echo $rta["cantidad"]?> </span> </h5>
                <h5> Total recaudado <span style="color:darkred;"> $<?php echo $rta["total"]?> </span></h5>
              </div>
            <?php }else{ ?>
              <h4><i class="fas fa-piggy-bank"></i>  Ingrese una consulta... </h4>
            <?php } ?>
            
          </div>
        </div>





    </div>
   
   
      <!-- Cierre del main container -->
  </div> 
                
                  
