<?php


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
  <link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">  
  
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >
    <div class="row align-items-center bg-info p-3 mb-4 px-4 ">
      <div class="col text-white  ">
        <h4><i class="fas fa-info-circle"></i> Usuario > <?php echo $user->getId()?></h4>
      </div>      
      <div class="col-3">
      </div>
    </div>

    <div class="container mt-3">
   


    <form action="<?php echo FRONT_ROOT?>user/modifyUser" method="POST">
      <input type="hidden" name="idUser" value="<?php echo $user->getId()?>">

      <div class="row justify-content-center mt-5 mb-5">
        <div class="form-row col-sm-9">
          <div class="col"><label for="email">Email</label>
          <input type="text" name="email" maxlength="60" value="<?php echo $user->getEmail()?>" required>
          </div>
          <div class="col"><label for="UserName">UserName</label>
          <input type="text" name="userName" maxlength="30" value="<?php echo $user->getUserName()?>" required>
          </div>

          <div class="col">
            
                <select name="userType"class="form-control" required>
                    <option value="" disabled selected>Tipo de Usuario</option>
                    <option value="0" >Admin</option>
                    <option value="1" >Usuario</option>
                   
                </select>
          </div>
        </div>
        <div class="form-row col-sm-9 my-3">
          
          <div class="col-sm-2">
          </div>
          <div class="col-sm-3">
          </div>
          <div class="col-sm-10">
                
          </div>
        </div>
        <div class="form-row col-sm-9">
          <div class="col-sm"></div>
        <div class="col-sm-4 align-self-right">
          <button type="submit" class="btn btn-outline-success btn-block"><i class="fas fa-pen-alt"></i>  EDITAR</button>
        </div>
      
        </div>
      </div>
    </form>












    <hr>

    </div>
        <!-- Cierre del main container -->
  </div>
