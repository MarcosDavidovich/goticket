<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT ?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


<style>


</style>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
  <div class="container-fluid p-0 m-0" id="main-content" >
    <div id="eventcont" class="pb-5">
      <img class ="img-fluid" src="<?php echo IMG_PATH."events/ticketgocover.jpg"?>" alt="">
      <div class="negativemgt container rounded  pt-3">
        <div class="row">
        </div>
        <h1 class="event-tit"><i class="fas fa-chevron-circle-right"></i>  ARTISTAS</h1>
        <div class="row">
          <div class="row col-9 justify-content-center"> 
             <div class="col-12">
             
             <form action=" <?php  echo FRONT_ROOT  ?>artist/listArtistView" method="POST">
                <div class="form-row col-12 my-4">
                  <div class="col-9">
                      <input type="text" name="keyword" class="form-control" id="" autocomplete="off" 
                      <?php if($keyword!="") { ?>
                          value="<?php echo $keyword ?>"
                      <?php } ?>
                      placeholder="Ingrese Busqueda... ">
                  </div>
                  <div class="col-3">
                    <button type="submit" class="btn btn-outline-success btn-block">
                      <?php if ( $keyword!=""   ) { ?>      
                        <i class="fas fa-sync-alt"></i> ACTUALIZAR
                      <?php } else { ?>
                        <i class="fas fa-filter"></i>  BUSCAR
                      <?php } ?>
                    </button>
                  </div>
                </div>
              </form>
             </div>
             <!-- START MAIN CONT -->
             
                  
              <?php if(!empty($artistList)) {
                    foreach($artistList as $artist){
                ?>
                    <div class="py-4 my-3 col-9 row border-bottom ">
                      <div class="col">
                        <h2><?php echo $artist->getName()?></h2>
                        <p><?php echo $artist->getDescription()?></p>
                      </div>
                      <div class="col-3">
                      <a href="<?php  echo FRONT_ROOT  ?>event/listEventByArtistView/<?php echo $artist->getId() ?>" class="btn btn-warning">
                      VER EVENTOS
                      </a>
                      </div>
                    </div>
                <?php } } else { ?>
                  <h2> <i class="far fa-dizzy"></i> SIN RESULTADOS...</h2>
                <?php } ?>
           
              
          
          
          
          </div>
          <!-- CLOSE MAIN CONT -->
          <!-- Start sidebar -->
          <div class="col"> 
            <h3>SIDEBAR</h3>
          </div>
          <!-- END sidebar -->

        </div>      
      </div>
    </div>

  </div>
