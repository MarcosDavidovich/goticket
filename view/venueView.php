<?php



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<?php
	include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >
    <div class="row align-items-center bg-info p-3 mb-4 px-4">
      <div class="col text-white  ">
        <h4><i class="fas fa-info-circle"></i> GoTicket > Lugares </h4>
      </div>      
      <div class="col-3">
        <!-- <a href="<?php //echo FRONT_ROOT?>calendar/calendarView" class="btn btn-warning" role="button" aria-disabled="true"><i class="fas fa-undo-alt"></i> VOLVER A CALENDARIOS</a> -->
      </div>
    </div>   

    <div class="container mt-3">
    <?php if (" "!=$message) { 
                      ?><div class="alert alert-success" role="alert">
                      <?php echo $message; ?>
                    </div><?php
                      }?>
    <?php if (null != $_SESSION['editVenue']) {?>
        <h3><i class="fas fa-edit"></i> Modificar Lugar: <?php echo $venue->getName()?></h3>
        <form action="<?php echo FRONT_ROOT ?>venue/modifyVenue" method="POST">
        <input type="hidden" name="idVenue" value="<?php echo $venue->getId()?>">
      <div class="row justify-content-center mt-5 mb-5">
          <div class="form-row col-sm-11">
                <div class="col">
                  <input type="text" name="name" maxlength="30" class="form-control"id="" value="<?php echo $venue->getName()?>" required>
                </div>
                <div class="col">
                  <input type="text" name="description" maxlength="30" class="form-control"id="" value="<?php echo $venue->getDescription()?>">
                </div>
                <div class="col">
                  <input type="text" name="address" maxlength="30" class="form-control"id="" value="<?php echo $venue->getAddress()?>" required>
                </div>
          </div>
          <div class="form-row col-sm-11 mt-3">
                <div class="col">
                  <?php if(empty($cityList)) {?>
                    <a href="<?php echo FRONT_ROOT?>city/cityView" class="btn btn-warning" role="button" aria-disabled="true">CREAR CIUDADES</a>
                    </div>
                    <?php
                  }else {?>
                    <select name="city" class="form-control">
                  <?php foreach ($cityList as $city) { ?>
                        <option <?php if($venue->getCity()->getId() == $city->getId()) { echo 'selected="true"';} ?>value="<?php echo $city->getId() ?>"><?php echo $city->getName() ?></option>
                  <?php }?>
                  <option selected="true" value="<?php echo $venue->getCity()->getId() ?>"><?php echo $venue->getCity()->getName() ?></option>

                      </select>
                </div>
                <div class="col">
                  <button type="submit" class="btn btn-outline-success btn-block"><i class="fas fa-pen-alt"></i> EDITAR</button>
                </div>
              <?php }?>
          </div>
      </div>
    </form> 
    <?php
    }else {?>
      <h3><i class="fas fa-globe-americasfas fa-map-marker-alt"></i> Alta de Lugares</h3>
    <form action="<?php echo FRONT_ROOT ?>venue/createVenue" method="POST">
      <div class="row justify-content-center mt-5 mb-5">
          <div class="form-row col-sm-11">
            <div class="col">
              <input type="text" name="name" maxlength="30" class="form-control"id="" placeholder="Nombre del lugar" required>
            </div>
            <div class="col">
              <input type="text" name="description" maxlength="30" class="form-control"id="" placeholder="Descripcion">
            </div>
            <div class="col">
              <input type="text" name="address" maxlength="30" class="form-control"id="" placeholder="Direccion" required>
            </div>
          </div>

          <div class="form-row col-sm-11 mt-3">
                <div class="col">
                  <?php if(empty($cityList)) {?>
                    <a href="<?php echo FRONT_ROOT?>city/cityView" class="btn btn-warning" role="button" aria-disabled="true">CREAR CIUDADES</a>
                    </div>
                    <?php
                  }else {?>
                    <select name="city" class="form-control" required>
                    <option value="" disabled selected>Seleccionar Ciudad</option>
                  <?php foreach ($cityList as $city) { ?>
                        <option value="<?php echo $city->getId() ?>"><?php echo $city->getName() ?></option>
                  <?php }?>
                      </select>
                </div>
                <div class="col">
                  <button type="submit" class="btn btn-outline-success btn-block"><i class="fas fa-plus"></i> AGREGAR</button>
                </div>
              <?php }?>
          </div>
      </div>
    </form>

    <?php }?>

    

    <hr>

      <h4> <i class="far fa-list-alt"></i>  Listado de Lugares</h4>
        
    <div class="row justify-content-center mt-5 mb-5">
      <table class="table table-sm table-hover col-sm-11">
        <thead class="thead-light">
          <tr class="">
            <th scope="col" >#</th>
            <th scope="col" >Nombre</th>
            <th scope="col" >Descripcion</th>
            <th scope="col" >Direccion</th>
            <th scope="col" >Ciudad</th>
            <th scope="col" ></th>
            <th scope="col" ></th>
          </tr>
        </thead>
        <tbody>
        <?php
            if(empty($venueList)){
              echo "<tr><td colspan='3'>LISTA VACIA</td></tr>";
            } else {
                foreach ($venueList as $venue) {?>
                    <tr>
                      <th scope="row"><?php echo $venue->getId()?> </th>
                      <td> <?php echo $venue->getName()?></td>
                      <td> <?php echo $venue->getDescription()?></td>
                      <td> <?php echo $venue->getAddress()?></td>
                      <td> <?php echo $venue->getCity()->getName()?></td>
                      <td><div class="col">
                            <form action="<?php echo FRONT_ROOT ?>venue/editVenue" method="POST">
                                <input type="hidden" name="idVenue" value="<?php echo $venue->getId()?>">
                                <input type="hidden" name="name" value="<?php echo $venue->getName()?>">
                                <input type="hidden" name="decription" value="<?php echo $venue->getDescription()?>">
                                <input type="hidden" name="address" value="<?php echo $venue->getAddress()?>">
                                <input type="hidden" name="idCity" value="<?php echo $venue->getCity()->getId()?>">
                                <button type="submit" class="btn btn-warning form-control"><i class="fas fa-pencil-alt"></i></button>
                              </form>
                    </div>
                      </td>
                      <td>
                      <div class="col">
                              <form action="<?php echo FRONT_ROOT ?>venue/deleteVenue" method="POST">
                                <input type="hidden" name="id" value="<?php echo $venue->getId()?>">
                                <button type="submit" class="btn btn-danger form-control" ><i class="far fa-trash-alt"></i></button>
                              </form>
                      </div>
                      </td>  
                    </tr>
            <?php }} ?>
            </tbody>
      </table>
    </div>
    </div>


    <!-- Cierre del main container -->
  </div> 