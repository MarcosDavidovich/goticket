<?php



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Go Ticket!</title>
	
	<link rel="shortcut icon" href="<?php echo FRONT_ROOT?>favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>ticketgo.css">
  <link rel="stylesheet" href="<?php echo FRONT_ROOT."view/CSS/" ?>index.css">
  
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<style>
#logincont{
  background: url(<?php echo IMG_PATH."login-back.jpg"?>) center center;
  min-height: 80vh;
}

</style>
<body>
<?php
  include_once(VIEWS_PATH."navbar.php");
	?>
	<div class="container-fluid p-0 m-0" id="main-content" >

    <div id="logincont" class="pt-5 pb-5">
      <div class="container">
        <div class="row justify-content-center login-form ">
          <div class="col-5 border p-3 rounded bg-light " id="loginform">
              <ul class="nav nav-pills nav-fill mb-3">
                <li class="nav-item">
                  <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="true"><i class="fas fa-unlock-alt"></i>  LOGIN</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" id="sign-tab" data-toggle="tab" href="#sign" role="tab" aria-controls="sign" aria-selected="true"><i class="fas fa-user-check"></i>  SIGN UP</a>
                </li>
              </ul>

            
            <div class="tab-content" id="myTabContent">

              <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">  
                <h2>LOGIN</h2>
                <p>Por favor, ingrese Usuario/Email y Contraseña</p>
                <form id="Login" action="<?php echo FRONT_ROOT?>user/login" method="POST">
                  <div class="form-group">
                    <input type="text" class="form-control" name="user" placeholder="Usuario o Email" autocomplete="off" required>
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control" name="pass" placeholder="Contraseña" autocomplete="off" required>
                  </div>
                  <?php  if($message!=""){ ?>
                    <div class="alert alert-danger" role="alert">
                      <?php   echo $message; ?>
                    </div>
                  <?php  }  ?>
                  <button type="submit" class="btn btn-success btn-block"><i class="fas fa-unlock-alt"></i> LOGIN</button>
                </form>
              </div>

              <div class="tab-pane fade" id="sign" role="tabpanel" aria-labelledby="sign-tab">
              
                <h2 class="">SIGN UP</h2>
                <p>Por favor, ingrese Email, Usuario y Contraseña</p>
                <form id="Sign" action="<?php echo FRONT_ROOT?>user/signUp" method="POST">
                  <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Email" required>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" name="user" placeholder="Usuario" required>
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control" name="pass" placeholder="Contraseña" required>
                  </div>
                  <button type="submit" class="btn btn-success btn-block"><i class="fas fa-user-check"></i> SIGN UP</button>
                </form>  
              
              </div>



            </div>
          </div>
        </div>
      </div>
    </div>

      <!-- Button trigger modal -->
     <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
      Launch demo modal
     </button> -->

      <!-- Modal Success -->
      <div class="modal fade" id="successMod" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalx">Bienvenido!</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Bienvenido a GoTicket!
            </div>
            <div class="modal-footer">
              <?php if(isset($_SESSION['purchase'])) { ?>
                <a href="<?php echo FRONT_ROOT?>purchase/purchaseView" class="btn btn-warning">Ir al Carrito</a>
              <?php  }  ?>
              <?php if($_SESSION["userType"] == "admin") { ?>
                <a href="<?php echo FRONT_ROOT?>home/dashboardView" id="x2"class="btn btn-warning">Ir al Dashboard</a>
              <?php  } else{  ?>
                <a href="<?php echo FRONT_ROOT?>user/userView" id="x2" class="btn btn-info">Ir a mi Perfil</a>
              <?php  }  ?>
              <a href="<?php echo FRONT_ROOT?>home/index" class="btn btn-success">Ir al Inicio</a>
            </div>
          </div>
        </div>
     
    <!-- Cierre del main container -->
  </div> 
    <script>
      
      function successMod(){
        $('#successMod').modal('show');
        }
      
    </script>
