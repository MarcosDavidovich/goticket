

/* truncate table venues;
truncate table calendars;
truncate table events;
truncate table artists;
truncate table cities;
truncate table artistxCalendar;
truncate table categories;
truncate table ticketTypes;
truncate table tickets;
truncate table purchasedTickets; */

/* 
INSERT INTO categories (name,description) VALUES ('Rock','Rock bands and artists');
INSERT INTO categories (name,description) VALUES ('Funk','Funk bands and artists');
INSERT INTO categories (name,description) VALUES ('Pop','Pop bands and artists');
INSERT INTO categories (name,description) VALUES ('Match','Sports Match');
INSERT INTO categories (name,description) VALUES ('Electronica','Music festival');
INSERT INTO categories (name,description) VALUES ('Festival','Music festival');

INSERT INTO cities (name) VALUES ('Mar del plata, Buenos Aires');
INSERT INTO cities (name) VALUES ('Rosario, Santa Fé');
INSERT INTO cities (name) VALUES ('Buenos Aires');
INSERT INTO cities (name) VALUES ('San Luis capital');
INSERT INTO cities (name) VALUES ('Cordoba capital');

INSERT INTO artists  (name,description) VALUES ('Pink Floyd','Rock Band');
INSERT INTO artists  (name,description) VALUES ('Red Hot Chili Peppers','Funk Band');
INSERT INTO artists  (name,description) VALUES ('AC/DC','Heavy Rock Band');
INSERT INTO artists  (name,description) VALUES ('La Nueva Luna','Cumbia Band');
INSERT INTO artists  (name,description) VALUES ('Nicole Moudaber','Crazy techno dj from europe');
INSERT INTO artists  (name,description) VALUES ('Cacho Castaña','Amador de mujeres con vasta experiencia');
INSERT INTO artists  (name,description) VALUES ('Adam Beyer','Metele techno a tu dia mamita');
INSERT INTO artists  (name,description) VALUES ('Christian Smith','Punchi Pachi y musica de tachos');
INSERT INTO artists  (name,description) VALUES ('Seba Soler','Dj amateur originario de las costas pamperas');

INSERT INTO venues (name,address,idCity,description) VALUES ('Minela','JuanBJusto',1,'Estadio Mundialista');
INSERT INTO venues (name,address,idCity,description) VALUES ('Bombonera','La boca',3,'Estadio de futbol');
INSERT INTO venues (name,address,idCity,description) VALUES ('Teatriz','Diagonal Pueyrredon 2222',1,'Teatro totalmente renovado');
INSERT INTO venues (name,address,idCity,description) VALUES ('Radio City','San Luis 1234',1,'El Clasico teatro marplatense');
INSERT INTO venues (name,address,idCity,description) VALUES ('Luna Park','Costanera 2345',3,'El mounstruo de los recitales');
INSERT INTO venues (name,address,idCity,description) VALUES ('Hipodromo de Palermo','Palermo av. 2345',3,'Recitales al aire libre');
INSERT INTO venues (name,address,idCity,description) VALUES ('Club Hipico de San Isidro','San Isidro av. 2345',3,'Hogar del Loolapartuza');

INSERT INTO events (name,description,idCategory) VALUES ('Lollapalooza','El festival con mas trayectoria del rock',1);
INSERT INTO events (name,description,idCategory) VALUES ('Awakenings ARG','Electronica del mas alla, por 1º vez en Argentina',2);
INSERT INTO events (name,description,idCategory) VALUES ('Festival Popular del Candombe','Sarassa y mucha Fruta en muchos lugares',3);
INSERT INTO events (name,description,idCategory) VALUES ('LooolaPartuza','La cumbia gedienta tiene su lugar!',4);


INSERT INTO ticketTypes (name, description) VALUES ('VIP','Very Important Person');
INSERT INTO ticketTypes (name, description) VALUES ('Palco','Palco general');
INSERT INTO ticketTypes (name, description) VALUES ('Platea','Platea general');
INSERT INTO ticketTypes (name, description) VALUES ('Campo','campo general');



INSERT INTO users (email,userName,password,userType) VALUES ('marcos@marcos.com','marcos','marcos',0);
INSERT INTO users (email,userName,password,userType) VALUES ('victor@victor.com','victor','victor',0);
INSERT INTO users (email,userName,password,userType) VALUES ('test@test.com','test','test',1);
insert into users (email,username,password,userType) VALUES ('a@a.com', 'admin', '123', 0);
insert into users (email,username,password,userType) VALUES ('user@test.com','user','123',1);
  */



/*
INSERT INTO calendars (date,idVenue,idEvent) VALUES (now(),1,1);
INSERT INTO calendars (date,idVenue,idEvent) VALUES (now(),2,2);
INSERT INTO calendars (date,idVenue,idEvent) VALUES (now(),1,3);
INSERT INTO calendars (date,idVenue,idEvent) VALUES (now(),1,5);


INSERT INTO artistxcalendar (idCalendar,idArtist) VALUES (1,1);
INSERT INTO artistxcalendar (idCalendar,idArtist) VALUES (1,2);
INSERT INTO artistxcalendar (idCalendar,idArtist) VALUES (2,1);
INSERT INTO artistxcalendar (idCalendar,idArtist) VALUES (1,3);
INSERT INTO artistxcalendar (idCalendar,idArtist) VALUES (1,3);
INSERT INTO artistxcalendar (idCalendar,idArtist) VALUES (5,6);
INSERT INTO artistxcalendar (idCalendar,idArtist) VALUES (6,5);


INSERT INTO tickets (idTicketType,idCalendar,price,available,remain) VALUES (1,1,900,1000,1000);
INSERT INTO tickets (idTicketType,idCalendar,price,available,remain) VALUES (2,2,200,2000,2000);
INSERT INTO tickets (idTicketType,idCalendar,price,available,remain) VALUES (2,1,500,500,500);
INSERT INTO tickets (idTicketType,idCalendar,price,available,remain) VALUES (1,3,400,1000,1000);


INSERT INTO purchasedTickets (id,idTicket,idPurchase,qr) VALUES (1,1,1,'qrprueba.com');
INSERT INTO purchasedTickets (id,idTicket,idPurchase,qr) VALUES (2,1,2,'asdassd.com');
INSERT INTO purchasedTickets (id,idTicket,idPurchase,qr) VALUES (3,2,1,'test.com');
INSERT INTO purchasedTickets (id,idTicket,idPurchase,qr) VALUES (4,2,2,'modmasod.com');
*/
