use ticket2;


-- acomode erased default y cambie el orden para que corra limpito.

 CREATE TABLE artists (id int AUTO_INCREMENT,
                     name varchar(30),
                     description varchar(50),
                     erased int default 0,
                     primary key (id));

 CREATE TABLE categories(id int AUTO_INCREMENT,
                         name varchar(30),
                         description varchar(30),
                         erased int default 0,
                         primary key(id));

 CREATE TABLE cities(id int AUTO_INCREMENT,
                     name varchar(30),
                     erased int default 0,
                     primary key(id));

 CREATE TABLE ticketTypes(id int AUTO_INCREMENT,
                         name varchar(30),
                         description varchar(50),
                         erased int default 0,
                         primary key (id) );
 

 CREATE TABLE venues (id int AUTO_INCREMENT,
                     name varchar(30),
                     address varchar(30),
                     idCity int,
                     description varchar(30),
                     erased int default 0,
                     primary key(id),
                     foreign key venues(idCity) references cities(id)); 

 
 CREATE TABLE images (id int AUTO_INCREMENT,
                     name varchar(60),
                     erased int default 0,
                     primary key (id));


 CREATE TABLE events (id int AUTO_INCREMENT,
                     name varchar(30),
                     description varchar(60),
                     idCategory int,
                     idCoverImg int,
                     idSqImg int,
                     erased int default 0,
                     primary key(id),
                     foreign key (idCategory) references categories(id), 
                     foreign key (idCoverImg) references images(id),
                     foreign key (idSqImg) references images(id)); 
 

 CREATE TABLE calendars (id int AUTO_INCREMENT,
                         date date,
                         idVenue int,
                         idEvent int,
                         erased int default 0,
                         primary key(id),
                         foreign key (idVenue) references venues(id),
                         foreign key (idEvent) references events(id));    

 CREATE TABLE artistxCalendar(idCalendar int, 
                             idArtist int,
                             erased int default 0,
                             primary key (idCalendar,idArtist),
                             foreign key (idCalendar) references calendars(id),
                             foreign key (idArtist) references artists(id));




CREATE TABLE tickets(id int AUTO_INCREMENT,
                     idTicketType int,
                     idCalendar int,
                     price float,
                     available int,
                     remain int,
                     erased int default 0,
                     primary key(id),
                     foreign key (idTicketType) references ticketTypes(id),
                     foreign key (idCalendar) references calendars(id) );                            


 CREATE TABLE purchases (id int AUTO_INCREMENT,
                         userId int,
                         date date,
                         total float,
                         discount int,
                         erased int default 0,
                         primary key(id));

 CREATE TABLE purchaseLines (id int AUTO_INCREMENT,
                             idPurchase int,
                             idTicket int,
                             quantity int,
                             price float,
                             erased int default 0,
                             primary key(id),
                             foreign key (idPurchase) references purchases(id),
                             foreign key (idTicket) references tickets(id));


 CREATE TABLE purchasedTickets(id int AUTO_INCREMENT,
                                 idTicket int,
                                 idPurchase int,
                                 price float,
                                 qr varchar(500),
                                 erased int default 0,
                                 primary key (id),
                                 foreign key (idPurchase) references purchases(id),
                                 foreign key (idTicket) references tickets(id));

 CREATE TABLE users (id int AUTO_INCREMENT,
                     email varchar(60),
                     userName varchar(30),
                     password varchar(30),
                     userType int NOT NULL,
                     erased int default 0,
                     primary key (id));

