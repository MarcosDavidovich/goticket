<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	
	require_once "config/autoload.php";
	
	require_once "config/request.php";
	require_once "config/router.php";
	require_once "config/config.php";
	//require_once "Daos/SingletonDao.php";
	/**
	 * Alias
	 */
	use Config\Autoload as Autoload;
	use Config\Router 	as Router;
	use Config\Request 	as Request;
	//use Daos\SingletonDao as SingletonDao;
	
	Autoload::start();
	
	session_start();

	Router::direccionar(new Request()); 
	include_once(VIEWS_PATH."footer.php");
	?>

	<script>

	$(function () {
		$('[data-toggle="popover"]').popover();
	})

	</script>
</body>
</html>
